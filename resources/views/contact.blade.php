@php
    $detect = new Mobile_Detect;   
@endphp

@extends('layouts.main')

@section('title', 'Contact')

@section('meta_description', 'Contact page')

@section('content')

    <section class="container">
        <div class="columns">
            <div class="column col-md-12 col-8 col-mx-auto">
                <header>
                    <h1>{{ __('Contact') }}</h1>
                </header> 

                @include('inc.errors')

                <div class="contact_form">
                    <form method="POST" action="{{ route('submit-form') }}">
                        @csrf

                        <div class="form-group @error('name') has-error @enderror">
                            <label for="name" class="form-label">{{ __('Your Name') }}</label>
                            <input id="name" type="text" class="form-input" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="{{ __('Your Name') }}" autofocus>

                            @error('name')
                                <p class="form-input-hint">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                            
                        </div> 
                        <div class="form-group @error('email') has-error @enderror">
                            <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}">

                            @error('email')
                                <p class="form-input-hint">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label class="form-label" for="message-body">Message</label>
                            <textarea class="form-input" name="message-body" id="message-body" cols="30" rows="10">
                                {{ old('message-body') ?? '' }}
                            </textarea>
                            @error('message-body')
                                <p class="form-input-hint">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                        </div>
                        <div class="form-group @error('g-recaptcha-response') has-error @enderror recapcha @if($detect->isMobile() && !$detect->isTablet()) recapcha--mobile @endif">
                            <div class="g-recaptcha" data-sitekey="6LceYd4UAAAAAB7C-FYyELkIkmRcGbrFSFnUAs8H" data-theme="dark" 
                                @if($detect->isMobile() && !$detect->isTablet())
                                    data-size="compact"
                                @endif>
                            </div>
                            @error('g-recaptcha-response')
                                <p class="form-input-hint">
                                    <strong>{{ $message }}</strong>
                                </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">
                                {{ __('Send') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush