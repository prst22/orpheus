@php
    $detect = new Mobile_Detect;   
@endphp
<div class="card mt">
    <div class="card-body">
        @if($errors->has('commentable_type'))
            <div class="toast_outer">    
                <div class="toast toast-error my-10">
                    {{ $errors->get('commentable_type') }}
                </div>
            </div>
        @endif
        @if($errors->has('commentable_id'))
            <div class="toast_outer">    
                <div class="toast toast-error my-10">
                    {{ $errors->get('commentable_id') }}
                </div>
            </div>
        @endif
        <form method="POST" action="{{ route('comments.store') }}">
            @csrf
            @honeypot
            <input type="hidden" name="commentable_type" value="\{{ get_class($model) }}" />
            <input type="hidden" name="commentable_id" value="{{ $model->getKey() }}" />

            {{-- Guest commenting --}}
            @if(isset($guest_commenting) and $guest_commenting == true)

                <div class="form-group">
                    <label class="form-label" for="guest_name">Enter your name here:</label>
                    <input type="text" class="form-input @if($errors->has('guest_name')) is-invalid @endif" name="guest_name" id="guest_name" value="{{ old('guest_name') ?? '' }}"/>
                    @error('guest_name')
                        <p class="form-input-hint">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label class="form-label" for="guest_email">Enter your email here:</label>
                    <input type="email" class="form-input @if($errors->has('guest_email')) is-invalid @endif" name="guest_email" id="guest_email" value="{{ old('guest_email') ?? '' }}"/>
                    @error('guest_email')
                        <p class="form-input-hint">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                
            @endif

            <div class="form-group">
                <label class="form-label" for="comment_message">Enter your message here:</label>
                <textarea class="form-input" name="message" rows="5" id="comment_message" required>
                    {{ old('message') ?? '' }}
                </textarea>
                @if($errors->has('message'))
                    <p class="form-input-hint">
                        <strong>{{ __('Message is required.') }}</strong>
                    </p>
                @endif
            </div>
            @if(isset($guest_commenting) and $guest_commenting == true)
                <div class="form-group @error('g-recaptcha-response') has-error @enderror recapcha @if($detect->isMobile() && !$detect->isTablet()) recapcha--mobile @endif">
                    <div class="g-recaptcha" data-sitekey="6LceYd4UAAAAAB7C-FYyELkIkmRcGbrFSFnUAs8H" data-theme="dark" 
                        @if($detect->isMobile() && !$detect->isTablet())
                            data-size="compact"
                        @endif>
                    </div>
                    @error('g-recaptcha-response')
                        <p class="form-input-hint">
                            <strong>{{ $message }}</strong>
                        </p>
                    @enderror
                </div>
                <script src='https://www.google.com/recaptcha/api.js' async defer></script>
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary text-uppercase">Submit</button>
            </div>
            @if (!Auth::guard('web')->check() && !Auth::guard('admin')->check())
                <div class="form-group"> 
                    <div class="divider text-center" data-content="OR"></div>
                </div>
                <div class="form-group"> 
                    @include('inc.login_buttons.login')
                </div>
            @endif  
        </form>
    </div>
</div>
<br />