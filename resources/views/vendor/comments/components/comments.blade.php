@php
    if (isset($approved) and $approved == true) {
        $comments = $model->approvedComments;
    } else {
        $comments = $model->comments;
    }
@endphp

<h3>{{ __('Comments:') }}</h3>

@if(Auth::guard('web')->check() || Auth::guard('admin')->check())
    @include('comments::_form')
@elseif(config('comments.guest_commenting') == true)
    @include('comments::_form', [
        'guest_commenting' => true
    ])
@else
    <div class="card">
        <div class="card-header">
            <div class="card-title h4">
                Authentication required
            </div>
        </div>
        <div class="card-body">
            <p>You must log in to post a comment.</p>
            <a href="{{ route('login') }}" class="btn btn-link mb-2">
                <svg class="icon">
                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#login"></use>
                </svg> 
                <b>Login</b>
            </a>
            @if (!Auth::guard('web')->check() || !Auth::guard('admin')->check())
                <div class="form-group"> 
                    <div class="divider text-center" data-content="OR"></div>
                </div>
                <div class="form-group"> 
                    @include('inc.login_buttons.login')
                </div>
            @endif  
        </div>
    </div>
@endauth

@if($comments->count() < 1)
    <div>There are no comments yet.</div>
@endif

<ul class="list mb-0">
  
    @php
        $comments = $comments->sortByDesc('created_at');

        if (isset($perPage)) {
            $page = request()->query('page', 1) - 1;

            $parentComments = $comments->where('child_id', '');

            $slicedParentComments = $parentComments->slice($page * $perPage, $perPage);

            $m = Config::get('comments.model'); // This has to be done like this, otherwise it will complain.
            $modelKeyName = (new $m)->getKeyName(); // This defaults to 'id' if not changed.

            $slicedParentCommentsIds = $slicedParentComments->pluck($modelKeyName)->toArray();

            // Remove parent Comments from comments.
            $comments = $comments->where('child_id', '!=', '');
                
            $grouped_comments = new \Illuminate\Pagination\LengthAwarePaginator(
                $slicedParentComments->merge($comments)->groupBy('child_id'),
                $parentComments->count(),
                $perPage
            );
            $grouped_comments->withPath(\Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath());
           
        } else {
            $grouped_comments = $comments->groupBy('child_id');
        }
    @endphp
    @foreach($grouped_comments as $comment_id => $comments)
        {{-- Process parent nodes --}}
        @if($comment_id == '')
            @foreach($comments as $comment)
                @include('comments::_comment', [
                    'comment' => $comment,
                    'grouped_comments' => $grouped_comments
                ])
            @endforeach
        @endif
    @endforeach
</ul>

@isset ($perPage)
    {{ $grouped_comments->fragment('comments')->links() }}
@endisset


