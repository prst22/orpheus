@inject('markdown', 'Parsedown')
@php($markdown->setSafeMode(true))

@if(isset($reply) && $reply === true)
    <div id="comment-{{ $comment->getKey() }}">
@else
    <li id="comment-{{ $comment->getKey() }}">
@endif
    <div>
        <h5 class="mt-0 mb-2 author_name @if($comment->commenter_type === 'App\Admin') text-bold text-primary @endif">
            @if($comment->commenter_type === 'App\Admin') Admin @endif
            {{ $comment->commenter->name ?? $comment->guest_name }} 
            <small><i> {{ $comment->created_at->diffForHumans() }} </i></small>
        </h5>
        <div style="white-space: pre-wrap; word-break: break-word;">{!! $markdown->line($comment->comment) !!}</div>

        <div class="mt-2">
            @can('reply-to-comment', $comment)
                <button class="btn btn-sm text-uppercase modal_reply_trigger">Reply</button>
            @endcan
            @can('edit-comment', $comment)
                <button class="btn btn-sm text-uppercase modal_edit_trigger">Edit</button>
            @endcan
            @can('delete-comment', $comment)
                <a href="{{ route('comments.destroy', $comment->getKey()) }}" onclick="event.preventDefault();document.getElementById('comment_delete_form_{{ $comment->getKey() }}').submit();" class="btn btn-sm text-uppercase">Delete</a>
                <form id="comment_delete_form_{{ $comment->getKey() }}" action="{{ route('comments.destroy', $comment->getKey()) }}" method="POST" style="display: none;">
                    @method('DELETE')
                    @csrf
                </form>
            @endcan
        </div>

        @can('edit-comment', $comment)
            <div class="modal edit_comment_modal">
                <a href="#close" class="modal-overlay" aria-label="Close"></a>
                <div class="modal-container">
                    <div class="modal-header">
                        <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                        <div class="modal-title h5">{{ __('Edit Comment') }}</div>
                    </div>
                    <div class="modal-body">
                        <div class="content">
                            <form method="POST" action="{{ route('comments.update', $comment->getKey()) }}">
                                @method('PUT')
                                @csrf

                                <div class="form-group">
                                    <label class="form-label" for="comment_message">Update your message here:</label>
                                    <textarea class="form-input" name="message" rows="10" id="comment_message" required>{{ $comment->comment }}</textarea>
                                </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">Update</button>
                                    <a class="btn btn-sm text-uppercase" href="#close">Cancel</a> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>        
        @endcan

        @can('reply-to-comment', $comment)
            <div class="modal reply_comment_modal">
                <a href="#closereply" class="modal-overlay" aria-label="Close"></a>
                <div class="modal-container">
                    <div class="modal-header">
                        <a href="#closereply" class="btn btn-clear float-right" aria-label="Close"></a>
                        <div class="modal-title h5">{{ __('Reply to Comment') }}</div>
                    </div>
                    <div class="modal-body">
                        <div class="content">
                            <form method="POST" action="{{ route('comments.reply', $comment->getKey()) }}">
                                @csrf

                                <div class="form-group">
                                    <label for="message">Enter your message here:</label>
                                    <textarea class="form-input" name="message" rows="10" required placeholder="Message"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm text-uppercase">Reply</button>
                                    <a class="btn btn-sm text-uppercase" href="#closereply">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @endcan

        <br />{{-- Margin bottom --}}

        {{-- Recursion for children --}}
        @if($grouped_comments->has($comment->getKey()))
            @foreach($grouped_comments[$comment->getKey()] as $child)
                @include('comments::_comment', [
                    'comment' => $child,
                    'reply' => false,
                    'grouped_comments' => $grouped_comments
                ])
            @endforeach
        @endif

    </div>
@if(isset($reply) && $reply === true)
  </div>
@else
  </li>
@endif