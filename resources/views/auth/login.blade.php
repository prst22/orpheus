@extends('layouts.main')

@section('title', 'Login')

@section('meta_description', 'Login page')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto login">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title h4">
                            <div class="header_logo">
                                <a href="{{ url('/') }}" class="header_logo__link">
                                    <img src="{{ URL::asset('assets') }}/logo.svg" alt="home" class="header_logo__img">    
                                </a>
                            </div>
                            <span class="login_icon">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#login"></use>
                                </svg> 
                            </span>
                            <h1 class="h4 d-inline">{{ __('Login') }}</h1>
                        </div>
                    </div>    
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf

                            <div class="form-group @error('email') has-error @enderror">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}" autofocus>

                                @error('email')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                                
                            </div>

                            <div class="form-group @error('password') has-error @enderror">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-input" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">

                                @error('password')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                                
                            </div>

                            <div class="form-group">
                                <label class="form-switch">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <i class="form-icon"></i> {{ __('Remember Me') }}
                                </label>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg mb-2" id="submit">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link mb-2" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                            @if (!Auth::guard('web')->check() && !Auth::guard('admin')->check())
                                <div class="form-group"> 
                                    <div class="divider text-center" data-content="OR"></div>
                                </div>
                                <div class="form-group"> 
                                    @include('inc.login_buttons.login')
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush