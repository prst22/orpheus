@extends('layouts.main')

@section('title', 'Verify Your Email Address')

@section('meta_description', 'Verify email page')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto login">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h1 class="h4">{{ __('Verify Your Email Address') }}</h1>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="toast_outer">    
                                <div class="toast toast-success my-2">
                                    <button class="btn btn-clear float-right"></button>
                                    {{ __('A fresh verification link has been sent to your email address. You might need to check email spam folder to find it.') }}
                                </div>
                            </div>
                        @endif

                        <p>
                            {{ __('Before proceeding, please check your email for a verification link. You might need to check email spam folder to find it.') }}
                            <small class="text-gray">
                                <i>({{ __('Sending an email can take up to 1 hour') }})</i>
                            </small>
                        </p>
                        
                        <p>{{ __('If you did not receive the email') }},</p>
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf
                            <button type="submit" class="btn btn-link btn-sm" id="submit">{{ __('click here to request another') }}</button>
                        </form>

                        <div class="text-right">
                            <a href="{{ url()->previous() }}" class="btn btn-lg mt mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                                </svg> 
                                Back
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush