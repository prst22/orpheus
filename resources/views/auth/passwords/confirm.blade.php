@extends('layouts.main')

@section('title', 'Confirm Password')

@section('meta_description', 'Confirm password page')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h1 class="h4">{{ __('Confirm Password') }}</h1>
                    </div>

                    <div class="card-body">
                        {{ __('Please confirm your password before continuing.') }}

                        <form method="POST" action="{{ route('password.confirm') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf

                            <div class="form-group">
                                <label for="password" class="form-label">{{ __('Password') }}</label>

                                <input id="password" type="password" class="form-input @error('password') has-error @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                                    
                            </div>

                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-primary mb-2 mr-2" id="submit">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link mb-2" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                
                            </div>
                        </form>
                        <div class="text-right">
                            <a href="{{ url()->previous() }}" class="btn btn-lg mt mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                                </svg> 
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush