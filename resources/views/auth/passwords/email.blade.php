@extends('layouts.main')

@section('title', 'Reset Password')

@section('meta_description', 'Reset password page')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h1 class="h4">{{ __('Reset Password') }}</h1>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="toast_outer">    
                                <div class="toast toast-success my-2">
                                    <button class="btn btn-clear float-right"></button>
                                    {{ session('status') }}
                                </div>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf

                            <div class="form-group @error('email') has-error @enderror">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Your E-mail">

                                @error('email')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary mb-2" id="submit">
                                    <svg class="icon">
                                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#resend"></use>
                                    </svg> {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </form>
                        <div class="text-right">
                            <a href="{{ url()->previous() }}" class="btn btn-lg mt mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                                </svg> 
                                Back
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush
