@extends('layouts.main')

@section('title', 'Reset Password')

@section('meta_description', 'Reset password page')

@section('content')
    <section class="container">
        <div class="row justify-content-center">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto login">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h1 class="h4">{{ __('Reset Password') }}</h1>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('password.update') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group @error('email') has-error @enderror">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>

                                <input id="email" type="email" class="form-input" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" placeholder="Your E-mail">

                                @error('email')
                                    <span class="form-input-hint" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                
                            </div>

                            <div class="form-group @error('password') has-error @enderror">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-input" name="password" required autocomplete="new-password" placeholder="New Password" autofocus>

                                @error('password')
                                    <span class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                             
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-input" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm New Password">
                            </div>

                            <div class="form-group">
                            
                                <button type="submit" class="btn btn-primary mb-2" id="submit">
                                    {{ __('Reset Password') }}
                                </button>
                                
                            </div>
                        </form>
                        <div class="text-right">
                            <a href="{{ url()->previous() }}" class="btn btn-lg mt mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                                </svg> 
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush