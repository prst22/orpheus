@php
    $detect = new Mobile_Detect;   
@endphp

@extends('layouts.main')

@section('title', 'Register')

@section('meta_description', 'Register page')

@section('content')
    <section class="container">
        <div class="columns">
            <div class="column col-xs-12 col-sm-11 col-md-8 col-lg-7 col-5 col-mx-auto login">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title h4">
                            <div class="header_logo">
                                <a href="{{ url('/') }}" class="header_logo__link">
                                    <img src="{{ URL::asset('assets') }}/logo.svg" alt="home" class="header_logo__img">
                                </a>
                            </div>
                            <span class="login_icon">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#register"></use>
                                </svg>
                            </span>
                            <h1 class="h4 d-inline">{{ __('Register') }}</h1>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}" onSubmit="document.getElementById('submit').disabled=true;">
                            @csrf

                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name" class="form-label">{{ __('Name') }}</label>
                                <input id="name" type="text" class="form-input" name="name" value="{{ old('name') }}" placeholder="{{ __('Name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group @error('email') has-error @enderror">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required autocomplete="email">

                                @error('email')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group @error('password') has-error @enderror">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-input"  name="password" placeholder="{{ __('Password') }}" required autocomplete="new-password">

                                @error('password')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-input" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required autocomplete="new-password">
                            </div>
                            <div class="form-group @error('g-recaptcha-response') has-error @enderror recapcha @if($detect->isMobile() && !$detect->isTablet()) recapcha--mobile @endif">
                                <div class="g-recaptcha" data-sitekey="6LceYd4UAAAAAB7C-FYyELkIkmRcGbrFSFnUAs8H" data-theme="dark"
                                    @if($detect->isMobile() && !$detect->isTablet())
                                        data-size="compact"
                                    @endif></div>
                                @error('g-recaptcha-response')
                                    <p class="form-input-hint">
                                        <strong>{{ $message }}</strong>
                                    </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg" id="submit">
                                    {{ __('Register') }}
                                </button>
                            </div>
                            @if (!Auth::guard('web')->check() && !Auth::guard('admin')->check())
                                <div class="form-group"> 
                                    <div class="divider text-center" data-content="OR"></div>
                                </div>
                                <div class="form-group"> 
                                    @include('inc.login_buttons.login')
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@push('footer_scripts')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush