@extends('layouts.main')

@if(Route::currentRouteName() === 'posts.category' || Route::currentRouteName() === 'posts.category.sortby')
    @section('title', isset($title) ? "Category {$title}" : 'Articles, videos, streams, jackpot tracker')
@else
    @section('title', isset($title) ? ucfirst($title) : 'Articles, videos, streams, jackpot tracker')
@endif
@section('meta_description', isset($title) ? ucfirst($title) : 'Independent resource about gambling. Articles, videos, streams, online casino reviews, track progressive slot jackpots across tens of different online casinos.')

@include('inc.keywords')

@push('header_styles')
    <style>
        .mediabox-img.ls-blur-up-is-loading,
        .mediabox-img.lazyload:not([src]){
            visibility: hidden;
        }
        .ls-blur-up-img,
        .mediabox-img{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            object-fit: cover;
        }
        .ls-blur-up-img{
            filter: blur(10px);
            opacity: 1;
            transition: opacity 700ms, filter 1000ms;
        }
        .ls-blur-up-img.ls-inview.ls-original-loaded{
            opacity: 0;
            filter: blur(5px);
        }
    </style>
@endpush

@if(isset($posts[0]->thumbnail) && $posts[0]->thumbnail !== null)
    @push('preload_images')
        <link rel="preload" href="{{ $posts[0]->thumbnail['l']['sm']['url'] }}" as="image" type="image/webp">
        <link rel="preload" href="{{ $posts[0]->thumbnail['l']['md']['url'] }}" as="image" type="image/webp"> 
        <link rel="preload" href="{{ $posts[0]->thumbnail['l']['lg']['url'] }}" as="image" type="image/webp">
    @endpush
@endif

@section('content') 
    <div class="column col-md-12 col-8 posts_cnt">
        
        @include('inc.sorting_bar')
        
        @if(Route::currentRouteName() === 'posts.category' || Route::currentRouteName() === 'posts.category.sortby') 
            <header class="category_header">
                <h1>Category: {{ isset($category_name) ? $category_name : '' }}</h1>
            </header>
        @endif 

        @if(count($posts) > 0)  
            @each('inc.article_thumb', $posts, 'post') 
        @else
            <h3 class="posts_cnt__item text-center">{{ __('No articles found') }}</h3>
        @endif

    </div>        
@endsection

@section('sidebar')
    
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            @if($all_categories)
                <li class="divider" data-content="CATEGORY"></li>
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                </li>
                @foreach ($all_categories as $key => $category_link)
                    <li class="menu-item">{!! $category_link !!}</li>
                @endforeach
            @endif
        </ul>
    </div>

@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <aside class="menu">
            @if($all_categories)
                <ul class="category_btn" tabindex="0">
                    <li class="divider" data-content="CATEGORY">
                        <span id="category_icon">
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#chevron-down"></use>
                            </svg> 
                        </span>
                    </li>
                </ul>
                <ul class="menu__inner">
                    <li class="menu-item">
                        <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                    </li>

                    @foreach ($all_categories as $key => $category_link)
                        <li class="menu-item">{!! $category_link !!}</li>
                    @endforeach
                </ul>
            @endif
        </aside>
    </div>

@endsection

@section('pagination')

    @if(count($posts) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $posts->onEachSide(2)->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@if (Route::currentRouteName() === 'home' && (request('page') === null || request('page') === '1'))
    @push('footer_scripts')
        <script type="application/ld+json">
            {"@context":"https://schema.org","@type":"Organization","name":"GamblingCentral","url":"{{ URL::to('/') }}","logo":"{{ URL::asset('assets') }}/logo.svg","sameAs": ["https://gcentralin.tumblr.com/"],"potentialAction": [{"@type": "SearchAction","target": {"@type": "EntryPoint","urlTemplate": "{{ URL::to('/search') }}?s={search_term_string}"},"query-input": "required name=search_term_string"}]}
        </script>   
    @endpush
@endif

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush