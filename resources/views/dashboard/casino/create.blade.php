
@extends('layouts.main_dashboard')

@section('title', 'Add new casino')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 

    <section class="column col-8 col-md-12 col-mx-auto add_casino_form">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Create Casino</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
        
        @include('inc.errors')

        <div class="create_casino">
            <form action="{{ route('casinos.store') }}" method="POST" enctype="multipart/form-data"> 
                @method('POST')
                <div class="form-group">
                    <div class="form_heading_cnt form_heading_cnt--create_post">
                        <label class="form-checkbox form-inline">
                            <input type="checkbox" name="tracked" value="1">
                            <i class="form-icon"></i> Trace Jackpots
                        </label>
                    </div> 
                </div>
                <div class="form-group @error('name') has-error @enderror">
                    <label class="form-label" for="name">Casino Name</label>
                    <input class="form-input" type="text" name="name" value="{{ old('name') ?? '' }}" required>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Netent casino data</h4>

                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_netent_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Yggdrasil casino data</h4>

                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_yggdrasil_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Red Tiger casino data</h4>

                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_redtiger_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Push Gaming casino data</h4>

                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_pushgaming_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Quick Spin casino data</h4>

                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_quickspin_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <div class="column col-12">
                            <label class="form-label" for="url">Casino Url</label>
                            <input class="form-input" type="text" name="url" value="{{ old('url') ?? '' }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">  
                    <label class="form-label" for="logo">Logo</label>
                    <input class="form-input" type="file" id="logo" name="logo">
                </div>
                <div class="form-group">
                    <label class="form-label" for="banner">Banner</label>
                    <input class="form-input" type="file" id="banner" name="banner">
                </div>
                <div class="form-group">
                    <label class="form-label" for="rating">Casino Rating</label>
                    <input class="form-input" type="number" id="rating" name="rating" min="0" max="10" step="0.1" value="{{ old('rating') ?? '' }}">
                </div>

                @include('dashboard.casino.inc.casino_info_form_group')

                <div class="form-group">
                    <label class="form-label" for="description">Casino Description</label>
                    <textarea class="form-input" id="description" name="description" rows="5">
                        {{ old('description') ?? '' }}
                    </textarea>
                </div>
                @csrf
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Add casino" class="btn btn-lg" id="submit">
                </div>
            </form>
            
        </div> 
    </section> 

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush