<div class="form-group">          
    <div class="columns">
        <h4 class="column col-12">Info</h4>
        <div class="column col-12">
            <label class="form-label" for="games">Casino Games</label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="slots" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('slots') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Slots
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="roulette" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('roulette') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Roulette
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="blackjack" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('blackjack') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Blackjack
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="craps" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('craps') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Craps
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="sports-betting" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('sports-betting') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Sports betting
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="video-poker" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('video-poker') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Video poker
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="bingo" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('bingo') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Bingo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="keno" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('keno') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Keno
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="baccarat" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('baccarat') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Baccarat
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="jackpot-games" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('jackpot-games') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Jackpot games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="live-games" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('live-games') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Live games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="poker" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('poker') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Poker
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="scratch-cards" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('scratch-cards') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Scratch cards
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="games[]" value="esports-betting" 
                    @isset($casino->info['games']) 
                        {{ collect($casino->info['games'])->contains('esports-betting') ? 'checked="checked"' : '' }} 
                    @endisset>
                <i class="form-icon"></i> Esports betting
            </label>
        </div>
        {{-- providers --}}
        <div class="column col-12">
            <label class="form-label" for="providers">Providers</label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="1x2-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("1x2-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> 1x2 gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="2by2-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("2by2-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> 2by2 gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="4theplayer" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("4theplayer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> 4theplayer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="888-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("888-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> 888 gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="adoptit-publishing" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("adoptit-publishing") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Adoptit publishing
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="ainsworth" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("ainsworth") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ainsworth
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="alchemy-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("alchemy-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Alchemy gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="all41-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("all41-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> All41 studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="amanet" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("amanet") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Amanet
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="amatic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("amatic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Amatic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="apollo-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("apollo-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Apollo games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="ash-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("ash-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ash gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="asia-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("asia-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Asia gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="aspect-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("aspect-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Aspect games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="asylum-labs" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("asylum-labs") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Asylum labs
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="aurify" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("aurify") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Aurify
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="aurum-signature-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("aurum-signature-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Aurum signature studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="authentic-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("authentic-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Authentic gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="avatarux" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("avatarux") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Avatarux
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bally" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bally") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bally
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bally-wulff" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bally-wulff") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bally wulff
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bang-bang" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bang-bang") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bang bang
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="barcrest" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("barcrest") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Barcrest
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bb-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bb-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bb games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="belatra" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("belatra") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Belatra
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bet-digital" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bet-digital") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bet digital
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="betdigital" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("betdigital") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Betdigital
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="betgames" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("betgames") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Betgames
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="betixon" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("betixon") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Betixon
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="betradar" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("betradar") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Betradar
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="betsoft-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("betsoft-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Betsoft gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bf-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bf-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> BF Games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bgaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bgaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bgaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="big-time-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("big-time-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Big time gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="blablablastudios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("blablablastudios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Blablablastudios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="blueprint-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("blueprint-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Blueprint gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="booming-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("booming-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Booming games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="booongo" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("booongo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Booongo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="buck-stakes-entertainment" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("buck-stakes-entertainment") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Buck stakes entertainment
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bulletproof-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bulletproof-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bulletproof gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="boss-media" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("boss-media") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Boss media
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bulletproof-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bulletproof-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bulletproof gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="bwin.party" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("bwin.party") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bwin.party
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="caleta" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("caleta") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Caleta
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="casino-technology" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("casino-technology") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Casino technology
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="cayetano" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("cayetano") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cayetano
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="colossus-bets" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("colossus-bets") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Colossus bets
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="concept-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("concept-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Concept gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="core-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("core-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Core gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="cr-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("cr-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cr games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="crazy-tooth-studio" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("crazy-tooth-studio") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Crazy tooth studio
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="cryptologic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("cryptologic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cryptologic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="d-tech" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("d-tech") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> D-tech
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="dice-lab" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("dice-lab") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Dice lab
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="dreamtech" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("dreamtech") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Dreamtech
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="edict" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("edict") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Edict
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="egt" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("egt") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Egt
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="electracade" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("electracade") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Electracade
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="electric-elephant" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("electric-elephant") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Electric elephant
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="elk-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("elk-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Elk studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="endemol-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("endemol-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Endemol games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="endorphina" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("endorphina") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Endorphina
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="evolution-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("evolution-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Evolution gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="evoplay" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("evoplay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Evoplay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="extreme-live-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("extreme-live-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Extreme live gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="eyecon" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("eyecon") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Eyecon
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="ezugi" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("ezugi") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ezugi
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="fantasma" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("fantasma") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fantasma
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="fazi" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("fazi") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fazi
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="felix-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("felix-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Felix gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="felt-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("felt-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Felt gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="finsoft" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("finsoft") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Finsoft
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="flatdog" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("flatdog") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Flatdog
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="fortune-factory" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("fortune-factory") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fortune factory
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="foxium" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("foxium") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Foxium
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="fuga-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("fuga-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fuga gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="fugaso" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("fugaso") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fugaso
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="g-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("g-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> G games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="game360" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("game360") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Game360
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gameart" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gameart") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gameart
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gameburger-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gameburger-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gameburger studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="games-lab" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("games-lab") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Games lab
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="games-warehouse" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("games-warehouse") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Games warehouse
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gamevy" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gamevy") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gamevy
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gaming-realms" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gaming-realms") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gaming realms
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gaming1" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gaming1") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gaming1
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gamomat" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gamomat") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gamomat
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="ganapati" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("ganapati") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ganapati
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="geco-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("geco-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Geco gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="genesis-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("genesis-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Genesis gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="genii" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("genii") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Genii
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gg-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gg-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gg gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="givme-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("givme-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Givme games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gold-coin-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gold-coin-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gold coin studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="golden-hero" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("golden-hero") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Golden hero
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="golden-race" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("golden-race") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Golden race
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="golden-rock-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("golden-rock-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Golden rock studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="green-jade-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("green-jade-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Green jade games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="green-tube" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("green-tube") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Green tube
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="gts" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("gts") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Gts
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="habanero" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("habanero") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Habanero
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="hacksaw-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("hacksaw-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Hacksaw gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="high-5-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("high-5-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> High 5 games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="hollywoodtv" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("hollywoodtv") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Hollywoodtv
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="igrosoft" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("igrosoft") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Igrosoft
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="igt" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("igt") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Igt
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="inspired-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("inspired-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Inspired gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="instant-win-gaming-(iwg)" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("instant-win-gaming-(iwg)") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Instant win gaming (iwg)
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="iron-dog-studio" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("iron-dog-studio") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Iron dog studio
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="isoftbet" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("isoftbet") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Isoftbet
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="jade-rabbit-studio" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("jade-rabbit-studio") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Jade rabbit studio
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="japan-technicals-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("japan-technicals-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Japan technicals games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="jpm-interactive" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("jpm-interactive") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Jpm interactive
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="just-for-the-win--jftw" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("just-for-the-win--jftw") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Just for the win -jftw
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="kalamba-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("kalamba-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Kalamba games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="kiron-interactive" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("kiron-interactive") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Kiron interactive
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="leander-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("leander-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Leander games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="leap-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("leap-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Leap gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="lightning-box" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("lightning-box") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Lightning box
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="live-5-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("live-5-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Live 5 gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="lucky-streak" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("lucky-streak") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Lucky streak
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="magic-dreams" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("magic-dreams") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Magic dreams
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="magnet-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("magnet-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Magnet gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="mascot-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("mascot-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mascot gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="matchbook" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("matchbook") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Matchbook
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="max-win-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("max-win-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Max win gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="merkur-slots" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("merkur-slots") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Merkur slots
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="meta-games-universal" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("meta-games-universal") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Meta games universal
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="mga" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("mga") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mga
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="microgaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("microgaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Microgaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="mrslotty" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("mrslotty") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mrslotty
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="multi-slot" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("multi-slot") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Multi slot
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="mutuel-play" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("mutuel-play") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mutuel play
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="neogames" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("neogames") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Neogames
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="neon-valley-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("neon-valley-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Neon valley studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="netent" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("netent") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Netent
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="nextgen-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("nextgen-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Nextgen gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="no-limit" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("no-limit") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> No limit
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="nolimit-city" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("nolimit-city") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Nolimit city
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="northern-lights-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("northern-lights-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Northern lights gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="novomatic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("novomatic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Novomatic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="nyx-gaming-group" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("nyx-gaming-group") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Nyx gaming group
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="odobo" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("odobo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Odobo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="old-skool-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("old-skool-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Old skool studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="openbet" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("openbet") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Openbet
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="oryx" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("oryx") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Oryx
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pariplay" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pariplay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pariplay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pearfiction-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pearfiction-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pearfiction studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="peter-&-sons" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("peter-&-sons") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Peter & sons
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pgsoft" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pgsoft") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pgsoft
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pirates-gold-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pirates-gold-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pirates gold studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="plank-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("plank-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Plank gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="platipus" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("platipus") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Platipus
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="play'n-go" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("play'n-go") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Play'n go
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="playson" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("playson") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Playson
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="playtech" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("playtech") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Playtech
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pocketdice" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pocketdice") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pocketdice
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pragmatic-play" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pragmatic-play") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pragmatic play
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="present-creative" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("present-creative") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Present creative
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="print-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("print-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Print studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="probability-jones" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("probability-jones") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Probability jones
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="pulse-8-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("pulse-8-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pulse 8 studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="push-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("push-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Push gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="quickfire" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("quickfire") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Quickfire
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="quickspin" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("quickspin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Quickspin
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="rabcat" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("rabcat") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Rabcat
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="rarestone-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("rarestone-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Rarestone gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="real-dealer-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("real-dealer-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Real dealer studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="realistic-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("realistic-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Realistic games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="red-rake-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("red-rake-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Red rake gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="red-tiger-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("red-tiger-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Red tiger gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="red7" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("red7") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Red7
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="reel-play" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("reel-play") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Reel play
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="reel-time-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("reel-time-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Reel time gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="reelfeel-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("reelfeel-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Reelfeel gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="reflex-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("reflex-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Reflex gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="relax-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("relax-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Relax gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="retro-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("retro-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Retro gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="revolver-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("revolver-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Revolver gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="rocksalt-interactive" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("rocksalt-interactive") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Rocksalt interactive
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="ruby-play" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("ruby-play") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ruby play
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sa-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sa-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sa gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="salsa-technology" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("salsa-technology") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Salsa technology
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="saucify" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("saucify") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Saucify
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sapphire-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sapphire-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sapphire gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="scientific-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("scientific-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Scientific games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="seven-deuce-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("seven-deuce-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Seven deuce gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sg-interactive" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sg-interactive") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sg interactive
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="shuffle-master" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("shuffle-master") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Shuffle master
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="side-city" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("side-city") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Side city
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sigma-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sigma-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sigma gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="skillzzgaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("skillzzgaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Skillzzgaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="skywind-group" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("skywind-group") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Skywind group
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sling-shots-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sling-shots-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sling shots studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="slingo" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("slingo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Slingo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="slot-factory" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("slot-factory") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Slot factory
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="slotvision" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("slotvision") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Slotvision
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="smartsoft-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("smartsoft-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Smartsoft gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="snowborn-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("snowborn-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Snowborn games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spadegaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spadegaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spadegaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spearhead-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spearhead-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spearhead studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spieldev" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spieldev") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spieldev
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spike-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spike-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spike games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spin-play-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spin-play-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spin play games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spinmatic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spinmatic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spinmatic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spinomenal" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spinomenal") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spinomenal
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="spribe" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("spribe") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spribe
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="stakelogic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("stakelogic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Stakelogic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sthlmgaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sthlmgaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sthlmgaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="storm-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("storm-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Storm gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="stormcraft-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("stormcraft-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Stormcraft studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="sunfox-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("sunfox-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sunfox games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="super-spade-club" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("super-spade-club") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Super spade club
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="swintt" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("swintt") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Swintt
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="switch-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("switch-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Switch studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="synot-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("synot-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Synot games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="the-games-company" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("the-games-company") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> The games company
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="thunderkick" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("thunderkick") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Thunderkick
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="tom-horn" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("tom-horn") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Tom horn
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="touchstone-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("touchstone-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Touchstone games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="triple-edge-studios" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("triple-edge-studios") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Triple edge studios
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="triple-profits-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("triple-profits-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Triple profits games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="truelab-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("truelab-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Truelab games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="virtual-generation" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("virtual-generation") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Virtual generation
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="vivo" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("vivo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Vivo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="wagerlogic" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("wagerlogic") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wagerlogic
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="wagermill" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("wagermill") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wagermill
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="wazdan" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("wazdan") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wazdan
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="wild-streak-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("wild-streak-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wild streak gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="williams-interactive" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("williams-interactive") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Williams interactive
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="wms" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("wms") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wms
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="woohoo-games" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("woohoo-games") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Woohoo games
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="yggdrasil-gaming" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("yggdrasil-gaming") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Yggdrasil gaming
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="xplosive-slots" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("xplosive-slots") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Xplosive slots
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="providers[]" value="yoloplay" 
                    @isset($casino->info["providers"]) 
                        {{ collect($casino->info["providers"])->contains("yoloplay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Yoloplay
            </label>
        </div>
        {{-- Payment methods --}}
        <div class="column col-12">
            <label class="form-label" for="payment-methods">Payment methods</label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="24-all-time" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("24-all-time") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> 24 all time
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="ach" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("ach") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ach
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="advcash" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("advcash") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Advcash
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="aktia" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("aktia") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Aktia
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="alandsbanken" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("alandsbanken") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Alandsbanken
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="american-express" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("american-express") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> American express
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="applepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("applepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Applepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="astropay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("astropay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Astropay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="banco-banrisul" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("banco-banrisul") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Banco banrisul
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="banco-do-brasil" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("banco-do-brasil") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Banco do brasil
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="bank-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("bank-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bank transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="bank-wire" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("bank-wire") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bank wire
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="beeline" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("beeline") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Beeline
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="binance-coin" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("binance-coin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Binance Coin (BNB)
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="bitcoin" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("bitcoin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bitcoin
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="bitcoin-cash" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("bitcoin-cash") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bitcoin cash
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="boleto-bancario" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("boleto-bancario") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Boleto bancario
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="bradesco" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("bradesco") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Bradesco
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="busd" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("busd") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> BUSD
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="caixa" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("caixa") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Caixa
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="cashtocode" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("cashtocode") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cashtocode
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="cep-bank" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("cep-bank") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cep bank
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="cheque" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("cheque") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Cheque
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="click2pay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("click2pay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Click2pay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="coinspaid" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("coinspaid") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Coinspaid
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="comepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("comepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Comepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="danske-bank" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("danske-bank") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Danske bank
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="depkasa" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("depkasa") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Depkasa
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="diners-club" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("diners-club") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Diners club
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="discover" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("discover") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Discover
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="dk" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("dk") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Dk
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="dogecoin" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("dogecoin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Dogecoin
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="earthport" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("earthport") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Earthport
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="echecks" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("echecks") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Echecks
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="eco" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("eco") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Eco
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="eco-voucher" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("eco-voucher") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Eco-voucher
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="ecobanq" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("ecobanq") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ecobanq
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="ecopayz" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("ecopayz") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ecopayz
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="elo" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("elo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Elo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="entercash" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("entercash") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Entercash
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="entropay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("entropay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Entropay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="envoy" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("envoy") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Envoy
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="eos" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("eos") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Eos
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="eps" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("eps") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Eps
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="ethereum" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("ethereum") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ethereum
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="europlat" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("europlat") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Europlat
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="euteller" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("euteller") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Euteller
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="fast-bank-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("fast-bank-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Fast bank transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="flexepin" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("flexepin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Flexepin
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="flutterwave" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("flutterwave") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Flutterwave
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="giropay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("giropay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Giropay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="google-pay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("google-pay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Google pay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="handelsbanken" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("handelsbanken") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Handelsbanken
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="hipercard" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("hipercard") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Hipercard
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="hizli-qr" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("hizli-qr") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Hizli qr
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="idebit" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("idebit") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Idebit
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="inpay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("inpay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Inpay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="instadebit" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("instadebit") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Instadebit
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="instant-bank-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("instant-bank-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Instant bank transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="instant-banking" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("instant-banking") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Instant banking
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="instant-payments-by-citadel" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("instant-payments-by-citadel") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Instant payments by citadel
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="interac" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("interac") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Interac
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="interac-e-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("interac-e-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Interac e-transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="isignthis" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("isignthis") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Isignthis
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="itaú" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("itaú") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Itaú
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="iwallet" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("iwallet") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Iwallet
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="jcb" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("jcb") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Jcb
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="jeton" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("jeton") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Jeton
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="jio" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("jio") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Jio
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="kassa-24" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("kassa-24") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Kassa 24
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="klarna" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("klarna") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Klarna
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="kwickgo" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("kwickgo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Kwickgo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="kyivstar" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("kyivstar") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Kyivstar
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="litecoin" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("litecoin") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Litecoin
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="loterica" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("loterica") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Loterica
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="luxonpay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("luxonpay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Luxonpay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="maestro" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("maestro") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Maestro
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mastercard" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mastercard") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mastercard
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="masterpass" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("masterpass") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Masterpass
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="megafon" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("megafon") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Megafon
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mercado-pago" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mercado-pago") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mercado pago
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mifinity" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mifinity") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mifinity
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mobile-payment" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mobile-payment") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mobile payment
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mobilepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mobilepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mobilepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="monero" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("monero") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Monero
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="monetix-wallet" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("monetix-wallet") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Monetix wallet
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="mtc" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("mtc") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Mtc
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="muchbetter" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("muchbetter") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Muchbetter
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="multibanco" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("multibanco") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Multibanco
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="neosurf" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("neosurf") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Neosurf
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="netbanking" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("netbanking") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Netbanking
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="neteller" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("neteller") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Neteller
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="nordea" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("nordea") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Nordea
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="online-bank-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("online-bank-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Online bank transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="omasp" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("omasp") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Omasp
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="op-pohjola" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("op-pohjola") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Op-pohjola
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="oxxo" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("oxxo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Oxxo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pago-efectivo" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pago-efectivo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pago efectivo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pago-express" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pago-express") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pago express
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pagofacil" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pagofacil") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pagofacil
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="papara" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("papara") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Papara
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pay4fun" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pay4fun") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pay4fun
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pay-form" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pay-form") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pay form
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="payberry" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("payberry") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Payberry
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="paynearme" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("paynearme") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Paynearme
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="payeer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("payeer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Payeer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="paypal" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("paypal") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Paypal
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="paysafecard" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("paysafecard") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Paysafecard
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="paytm" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("paytm") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Paytm
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="perfect-money" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("perfect-money") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Perfect money
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="phonepe" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("phonepe") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Phonepe
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="piastrix" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("piastrix") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Piastrix
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pix" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pix") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pix
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="poli" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("poli") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Poli
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="pop-pankki" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("pop-pankki") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Pop pankki
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="postepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("postepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Postepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="prepaid-card" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("prepaid-card") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Prepaid card
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="qiwi" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("qiwi") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Qiwi
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="rapid-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("rapid-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Rapid transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="revolut" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("revolut") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Revolut
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="ripple" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("ripple") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Ripple
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="s-pankki" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("s-pankki") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> S-pankki
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="safety-pay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("safety-pay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Safety pay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="santander" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("santander") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Santander
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="siirto" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("siirto") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Siirto
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="siru" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("siru") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Siru
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="skrill" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("skrill") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Skrill
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="skrill-1-tap" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("skrill-1-tap") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Skrill 1-tap
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="sms-voucher" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("sms-voucher") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sms voucher
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="sofort-banking" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("sofort-banking") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sofort banking
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="sofort-Überweisung" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("sofort-Überweisung") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sofort Überweisung
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="solo" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("solo") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Solo
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="spei" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("spei") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Spei
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="sticpay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("sticpay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Sticpay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="swish" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("swish") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Swish
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="switch" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("switch") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Switch
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="säästöpankki" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("säästöpankki") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Säästöpankki
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="tele2" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("tele2") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Tele2
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="telepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("telepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Telepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="tether" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("tether") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Tether
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="todito-cash" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("todito-cash") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Todito cash
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="transferencia-bancaria-local" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("transferencia-bancaria-local") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Transferencia bancaria local
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="tron" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("tron") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Tron
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="trustly" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("trustly") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Trustly
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="unionpay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("unionpay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Unionpay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="upi" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("upi") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Upi
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="venuspoint" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("venuspoint") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Venuspoint
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="verkkopankki" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("verkkopankki") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Verkkopankki
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="visa" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("visa") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Visa
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="visa-debit" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("visa-debit") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Visa debit
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="visa-electron" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("visa-electron") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Visa electron
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="visa-voucher" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("visa-voucher") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Visa voucher
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="visa-purplepay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("visa-purplepay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Visa purplepay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="walletone" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("walletone") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Walletone
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="webmoney" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("webmoney") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Webmoney
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="wechat-pay" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("wechat-pay") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wechat pay
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="wire-transfer" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("wire-transfer") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Wire-transfer
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="yandex-money" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("yandex-money") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Yandex money
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="zcash" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("zcash") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Zcash
            </label>
            <label class="form-checkbox form-inline">
                <input type="checkbox" name="payment-methods[]" value="zimpler" 
                    @isset($casino->info["payment-methods"]) 
                        {{ collect($casino->info["payment-methods"])->contains("zimpler") ? 'checked="checked"' : "" }} 
                    @endisset>
                <i class="form-icon"></i> Zimpler
            </label>
        </div>
    </div>
</div>