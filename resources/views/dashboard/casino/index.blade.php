@extends('layouts.wide')

@section('title', 'Manage Casinos')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-12 index_casinos">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Casinos</h1>
        </header>

        <div class="card">
            <div class="card-header">
                <div class="card-title h4">Add Casino</div>
            </div>
            <div class="card-footer">
                <a href="{{ route('casinos.create') }}" class="btn btn-lg btn-primary" id="add_slot">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                    </svg> 
                    Add New
                </a>
            </div>
        </div>
        
        <div class="create_casino mt">

            @include('inc.errors')

            <table class="table">
                <thead>
                    <tr>
                        <th>Casino info</th>
                        <th>Netent data</th>
                        <th>Yggdrasil data</th>
                        <th>Red tiger data</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($casinos as $key => $casino) 
                        <tr>
                            <td>
                                <span class="casinos">
                                    Id: {{ $casino->id ?? '' }}
                                </span><br>
                            
                                <span class="casinos">
                                    {{ $casino->name ?? '' }}
                                </span>
                            </td>
                            <td>
                                @if (!empty($casino->netent_casino_data))
                                    <ul>
                                        @foreach ($casino->netent_casino_data as $key => $item)  
                                            <li>{{ $key }}: {{ $item ?? '' }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td>
                                @if (!empty($casino->yggdrasil_casino_data))
                                    <ul>
                                        @foreach ($casino->yggdrasil_casino_data as $key => $item)
                                            <li>{{ $key }}: {{ $item ?? '' }}</li> 
                                        @endforeach
                                    </ul>    
                                @endif
                            </td>
                            <td>
                                @if (!empty($casino->redtiger_casino_data))
                                    <ul>
                                        @foreach ($casino->redtiger_casino_data as $key => $item)
                                            <li>{{ $key }}: {{ $item ?? '' }}</li> 
                                        @endforeach
                                    </ul>    
                                @endif
                                
                            </td>
                            <td>
                                <a href="{{ route('casinos.edit', $casino->id) }}" class="btn btn-sm btn-action tooltip tooltip-left" data-tooltip="Edit Casino">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                                    </svg> 
                                </a>
                                <button class="btn btn-sm btn-error btn-action tooltip tooltip-left delet_casino_btn" data-tooltip="Remove Casino">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                                    </svg>
                                </button>
                                    
                                <div class="modal modal-sm confirm_casino_delete_modal">
                                    <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                    <div class="modal-container">
                                        <div class="modal-header">
                                            <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                            <div class="modal-title h5">Remove {{ ucfirst($casino->name) }} casino?</div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="content">
                                                <form action="{{ route('casinos.destroy', $casino->id) }}" method="POST"> 
                                                    @method('DELETE')
                                                        <div class="form-group text-center">
                                                            <button class="btn btn-action">{{ __('Yes') }}</button>
                                                            <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                                        </div>
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        <tr>
                    @empty
                        <tr>
                            <td colspan="6">
                                <h3 class="posts_cnt__item text-center">No casinos</h3> 
                            </td>
                        </tr>   
                    @endforelse
                </tbody>
            </table>
            @section('pagination')

                @if(count($casinos) > 0)
                    <section class="container grid-lg pag_cnt">
                        <div class="columns">
                            <div class="column col-12">
                                {{ $casinos->links() }}
                            </div>
                        </div>
                    </section>
                @endif

            @endsection
        </div> 
    </section> 
@endsection

@section('sidebar')
    @include('dashboard.inc.wide_sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush