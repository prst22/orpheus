@extends('layouts.main_dashboard')

@section('title', 'Edit Casino')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 

    <section class="column col-8 col-md-12 col-mx-auto edit_casino_form">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Edit Casino</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
        
        <div class="update_casino">
            @include('inc.errors')
            <form action="{{ route('casinos.update', $casino->id) }}" method="POST" enctype="multipart/form-data"> 
                @method('PUT')
                <div class="form-group">
                    <div class="form_heading_cnt form_heading_cnt--create_post">
                        <label class="form-checkbox form-inline">
                            <input type="checkbox" name="tracked" value="1" {{ $casino->tracked ? 'checked="checked"' : '' }}>
                            <i class="form-icon"></i> Trace Jackpots
                        </label>
                    </div> 
                </div>
                <div class="form-group @error('name') has-error @enderror">
                    <label class="form-label" for="name">Casino Name</label>
                    <input class="form-input" type="text" name="name" value="{{ $casino->name }}" required>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Netent casino data</h4>
                        @if (!empty($casino->netent_casino_data))
                            @foreach ($casino->netent_casino_data as $key => $item)  
                                <div class="container">
                                    <div class="columns">
                                        <div class="column col-6 field provider_data">
                                            <input class="form-input" type="text" name="netentkey[]" value="{{ $key }}" required>
                                        </div>
                                        <div class="column col-6 field provider_data">
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="netentvalue[]" value="{{ $item }}" required>
                                                <button class="btn input-group-btn field_remove" type="button">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_netent_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Yggdrasil casino data</h4>
                        @if (!empty($casino->yggdrasil_casino_data))
                            @foreach ($casino->yggdrasil_casino_data as $key => $item)  
                                <div class="container">
                                    <div class="columns">
                                        <div class="column col-6 field provider_data">
                                            <input class="form-input" type="text" name="yggdrasilkey[]" value="{{ $key }}" required>
                                        </div>
                                        <div class="column col-6 field provider_data">
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="yggdrasilvalue[]" value="{{ $item }}" required>
                                                <button class="btn input-group-btn field_remove" type="button">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        
                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_yggdrasil_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Red Tiger casino data</h4>
                        @if (!empty($casino->redtiger_casino_data))
                            @foreach ($casino->redtiger_casino_data as $key => $item)  
                                <div class="container">
                                    <div class="columns">
                                        <div class="column col-6 field provider_data">
                                            <input class="form-input" type="text" name="redtigerkey[]" value="{{ $key }}" required>
                                        </div>
                                        <div class="column col-6 field provider_data">
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="redtigervalue[]" value="{{ $item }}" required>
                                                <button class="btn input-group-btn field_remove" type="button">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        
                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_redtiger_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Push Gaming casino data</h4>
                        @if (!empty($casino->pushgaming_casino_data))
                        
                            @foreach ($casino->pushgaming_casino_data as $key => $item)  
                                <div class="container">
                                    <div class="columns">
                                        <div class="column col-6 field provider_data">
                                            <input class="form-input" type="text" name="pushgamingkey[]" value="{{ $key }}" required>
                                        </div>
                                        <div class="column col-6 field provider_data">
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="pushgamingvalue[]" value="{{ $item }}" required>
                                                <button class="btn input-group-btn field_remove" type="button">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        
                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_pushgaming_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <h4 class="column col-12">Quick Spin casino data</h4>
                        @if (!empty($casino->quickspin_casino_data))
                        
                            @foreach ($casino->quickspin_casino_data as $key => $item)  
                                <div class="container">
                                    <div class="columns">
                                        <div class="column col-6 field provider_data">
                                            <input class="form-input" type="text" name="quickspinkey[]" value="{{ $key }}" required>
                                        </div>
                                        <div class="column col-6 field provider_data">
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="quickspinvalue[]" value="{{ $item }}" required>
                                                <button class="btn input-group-btn field_remove" type="button">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        
                        <div class="column col-12 add_provider_data_cnt">
                            <button class="btn btn-sm add_quickspin_field" type="button">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                                </svg> 
                                Add field
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="columns">
                        <div class="column col-12">
                            <label class="form-label" for="url">Casino Url</label>
                            <input class="form-input" type="text" name="url" value="{{ $casino->url }}">
                        </div>
                    </div>
                </div>

                @if (isset($casino->image['branding']))
                    
                    @if (isset($casino->image['branding']['logo']))
                        <div class="columns">
                            <div class="column col-6">
                                <div class="form-group">
                                    <label class="form-label" for="image">Casino Logo</label>
                                    <figure class="edit_post_thumbnail">
                                        <img src="{{ $casino->image['branding']['logo']['lg'] }}" class="edit_casino_logo">
                                    </figure>
                                </div> 
                            </div> 
                            <div class="column col-6">
                                <div class="form-group">  
                                    <label class="form-label" for="logo">New Logo</label>
                                    <input class="form-input" type="file" id="logo" name="logo">
                                </div>
                            </div>
                        </div>
                    @else

                        <div class="form-group">  
                            <label class="form-label" for="logo">New logo</label>
                            <input class="form-input" type="file" id="logo" name="logo">
                        </div>
                        
                    @endif 
                      
                @else

                    <div class="form-group">  
                        <label class="form-label" for="logo">New logo</label>
                        <input class="form-input" type="file" id="logo" name="logo">
                    </div>

                @endif  

                @isset($casino->image['branding'])

                    @isset($casino->image['branding']['banner'])
                        <div class="form-group">
                            <label class="form-label">Casino Banner</label>
                            <figure class="edit_post_thumbnail">
                                <img src="{{ $casino->image['branding']['banner']['lg'] }}" class="edit_post_thumbnail__img">
                            </figure>
                        </div>
                    @endisset 

                @endisset   

                <div class="form-group">
                    <label class="form-label" for="banner">New Banner</label>
                    <input class="form-input" type="file" id="banner" name="banner">
                </div>
                <div class="form-group">
                    <label class="form-label" for="rating">Casino Rating</label>
                    <input class="form-input" type="number" id="rating" name="rating" min="0" max="10" step="0.1" value="{{ $casino->rating ?? '' }}">
                </div>

                @include('dashboard.casino.inc.casino_info_form_group')

                <div class="form-group">
                    <label class="form-label" for="description">Casino Description</label>
                    <textarea class="form-input" id="description" name="description" rows="5">
                        {{ $casino->description }}
                    </textarea>
                </div>
                @csrf
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Update" class="btn btn-lg" id="submit">
                </div>
            </form> 
        </div> 
    </section> 

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush