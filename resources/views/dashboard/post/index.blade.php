@extends('layouts.main_dashboard')

@section('title', 'Manage Articles')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 

    <section class="column col-8 col-md-12 col-mx-auto manage_posts">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Articles</h1>
        </header>
        <div class="card">
            <div class="card-header">
                <div class="card-title h4">Create Article</div>
            </div>
            <div class="card-footer">
                <a href="{{ route('posts.create') }}" class="btn btn-lg btn-primary">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                    </svg> 
                    Create New
                </a>
            </div>
        </div>
        <div class="edit_posts mt">
            
            @include('inc.errors')
            
            @forelse($posts as $post)
                <div class="edit_posts__post {{ $post->published ? 'published' : 'not_published' }}">
                    <h4 class="mr-2">{{ $post->title }}</h4>
                    <div class="edit_post_btns">
                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-action tooltip tooltip-left" data-tooltip="Edit Article">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                            </svg> 
                        </a>
                        <a href="#" class="btn btn-error btn-action tooltip tooltip-left delete_post_btn" data-tooltip="Delete Article">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                            </svg> 
                        </a>
                        <div class="modal modal-sm confirm_post_delete_modal">
                            <a href="#close" class="modal-overlay" aria-label="Close"></a>
                            <div class="modal-container">
                                <div class="modal-header">
                                    <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                    <div class="modal-title h5">Remove {{ ucfirst($post->title) }}?</div>
                                </div>
                                <div class="modal-body">
                                    <div class="content">
                                        <form action="{{ route('posts.destroy', $post->id) }}" method="POST"> 
                                            @method('DELETE')
                                            <div class="form-group text-center">
                                                <button class="btn btn-action">{{ __('Yes') }}</button>
                                                <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                            </div>
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            @empty
                <h3 class="posts_cnt__item text-center">{{ __('No articles') }}</h3>
            @endforelse
        </div>
    </section>

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('pagination')
    @if(!empty($posts))
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $posts->links() }}
                </div>
            </div>
        </section>
    @endif
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush