@extends('layouts.main_dashboard')

@section('title', 'Add new article')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-8 col-md-12 col-mx-auto add_post_form">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Add new article</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
        @include('inc.errors')
        <div class="form-group">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="form_flex_end">
                        <label class="form-checkbox form-inline">
                            <input type="checkbox" name="published" value="1">
                            <i class="form-icon"></i> Published
                        </label> 
                        <label class="form-checkbox form-inline">
                            <input type="checkbox" name="sticky" value="1">
                            <i class="form-icon"></i> Sticky
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label" for="thumbnail">Thumbnail</label>
                    <input class="form-input" type="file" id="thumbnail" name="thumbnail" required>
                </div>
                @if( !empty($categories) )
                    <div class="form-label">Categories</div>
                    
                    <div class="form-group">
                        @foreach($categories as $key => $category)
                            @php
                                $category_checkbox_values = old("category") ?? '';
                                if(!empty($category_checkbox_values)){
                                    $found_key = array_search($category->id, $category_checkbox_values); 
                                    $checked = $found_key !== false ? $category_checkbox_values[$found_key] : false;
                                }
                            @endphp
                            @if (!empty($category_checkbox_values))
                                @if($category->id == $checked)
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}" checked>
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @else
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{  $category->id }}">
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label> 
                                @endif
                            @else
                                @if($key === 0)
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}" checked>
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @else
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}">
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @endif
                            @endif
                            
                        @endforeach
                    </div>
                @endif
                
                <div class="form-group">
                    <label class="form-label" for="title">Title</label>
                    <input class="form-input" type="text" id="title" name="title" value="{{ old('title') ?? '' }}" placeholder="Title" required>
                </div>
                <div class="form-group">
                    <label class="form-label" for="body">Article content</label>
                    <textarea class="form-input" id="editor" name="body" placeholder="Article content">
                        {{ old('body') ?? '' }}
                    </textarea>
                </div>
                @csrf
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Create" class="btn btn-lg" id="submit">
                </div>
            </form>
        </div> 
        {{-- gallery modal --}}
        <div class="modal modal-lg" id="gallery_modal">
            <a href="#close-gallery" class="modal-overlay" aria-label="Close"></a>
            <div class="modal-container">
                <div class="modal-header">
                    <a href="#close-gallery" class="btn btn-clear float-right" aria-label="Close"></a>
                    <div class="modal-title h3">{{ __('Gallery') }}</div>
                </div>
                <div class="modal-body">
                    <div class="content">
                        <div class="container">
                            <div class="columns attacments_cnt">
                                <div class="column col-12">
                                    <div class="loading loading-lg" id="modal_loader"></div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="columns">
                                    <div class="column col-12 col-ml-auto gallery_modal_pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 
@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush