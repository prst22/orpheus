@extends('layouts.wide')

@section('title', 'Manage Slots')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content')
    
    <section class="column col-12 index_slots">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Slots</h1>
        </header>

        <div class="card">
            <div class="card-header">
                <div class="card-title h4">Add Slot</div>
            </div>
            <div class="card-footer">
                <a href="{{ route('slots.create') }}" class="btn btn-lg btn-primary" id="add_slot">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                    </svg> 
                    Add New
                </a>
            </div>
        </div>

        <div class="slots mt">

            @include('inc.errors')

            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Slot Name</th>
                        <th>Casino Name</th>
                        <th>Provider</th>
                        <th>Game Id</th>
                        <th>Active</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($slots as $key => $slot) 
                        <tr class="text-small">
                            <td>
                                <span>
                                    {{ $slot->id ?? '' }}
                                </span>
                            </td>
                            <td>
                                <div>
                                    <span class="slots mr-1">
                                        {{ $slot->name ?? '' }}
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span>
                                    {{ $slot->casino ?? '' }}
                                </span>
                            </td>
                            <td>
                                <span>
                                    {{ str_limit($slot->provider, 3, '') }}
                                </span>
                            </td>
                            <td>
                                <span>
                                    {{ $slot->game_id ?? '' }}
                                </span>
                            </td>
                            <td>
                                <div class="slots_edit_cnt">
                                    <form action="{{ route('slots.set-active', $slot->id) }}" method="POST" class="set_active_form"> 
                                        @method('POST')
                                            <label class="form-switch form-inline">
                                                <input type="checkbox" name="active" value="{{ $slot->active }}" {{ $slot->active ? 'checked' : '' }}>
                                                <i class="form-icon"></i> 
                                            </label>
                                            <input class="form-input" type="hidden" name="casinoid" value="{{ $slot->casino_id }}">
                                        @csrf
                                    </form>
                                </div>
                            </td>
                            <td>

                                @if($slot->casino_id !== null)  
                                    <button class="btn btn-sm btn-action tooltip tooltip-left modal_trigger" data-tooltip="Edit Slot">
                                        <svg class="icon">
                                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                                        </svg> 
                                    </button>

                                    <div class="modal modal update_slot_modal">
                                        <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                        <div class="modal-container">
                                            <div class="modal-header">
                                                <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                                <div class="modal-title h5">Update {{ ucfirst($slot->name) }}</div>
                                            </div>
                                            <div class="modal-body">
                                                <div class="content">
                                                    <form action="{{ route('slots.update', $slot->id) }}" method="POST"> 
                                                        @method('PUT')
                                                        <div class="form-group">
                                                            <label class="form-label" for="name">Name</label>
                                                            <input class="form-input" type="text" name="name" value="{{ $slot->name }}">
                                                            <input class="form-input" type="hidden" name="current-name" value="{{ $slot->name }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="casino">Casino Name</label>
                                                            <select class="form-select" name="new-casino">

                                                                @foreach ($casinos as $casino)
                                                                    @if ($slot->casino === $casino->name)
                                                                        <option value="{{ $casino->id }}" selected>{{ $casino->name }}</option> 
                                                                    @else
                                                                        <option value="{{ $casino->id }}">{{ $casino->name }}</option> 
                                                                    @endif   
                                                                @endforeach

                                                            </select>
                                                            
                                                            @foreach ($casinos as $casino)
                                                                @if ($slot->casino === $casino->name)
                                                                    <input class="form-input" type="hidden" name="current-casino" value="{{ $casino->id }}">
                                                                @endif 
                                                            @endforeach
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="casino">Provider</label>
                                                            <input class="form-input" type="text" name="provider" value="{{ $slot->provider }}">
                                                            <input class="form-input" type="hidden" name="current-provider" value="{{ $slot->provider }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="gameid">Game Id</label>
                                                            <input class="form-input" type="text" name="gameid" value="{{ $slot->game_id }}">
                                                            <input class="form-input" type="hidden" name="current-gameid" value="{{ $slot->game_id }}">
                                                        </div>
                                                        <div class="form-group">
                                                            @if ($slot->type)
                                                                <div class="jackpot_type">Jackpot type: {{ $slot->type }}</div>
                                                            @else 
                                                                <div class="jackpot_type">Add jackpot type to the slot</div>
                                                            @endif  
                                                        </div> 
                                                       
                                                        <div class="form-group">
                                                            <button class="btn">Update</button>
                                                        </div>
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <button href="#" class="btn btn-sm btn-action tooltip tooltip-left detach_slot_btn" data-tooltip="Detach slot from casino">
                                        <svg class="icon">
                                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#minus"></use>
                                        </svg> 
                                    </button>         
                                    <div class="modal modal-sm confirm_slot_detach_modal">
                                        <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                        <div class="modal-container">
                                            <div class="modal-header">
                                                <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                                <div class="modal-title h5">Detach {{ ucfirst($slot->name) }} from {{ $slot->casino }} casino?</div>
                                            </div>
                                            <div class="modal-body">
                                                <div class="content">
                                                    @php
                                                        $casino_id;
                                                    @endphp
                                                    @foreach ($casinos as $casino)
                                                        @if ($slot->casino === $casino->name)
                                                            @php
                                                                $casino_id = $casino->id;
                                                            @endphp
                                                            <form action="{{ route('slots.detach', ['id' => $slot->id, 'casinoid' => $casino->id]) }}" method="POST"> 
                                                                @method('POST')
                                                                <div class="form-group text-center">
                                                                    <button class="btn btn-action">{{ __('Yes') }}</button>
                                                                    <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                                                </div>
                                                                @csrf
                                                            </form>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <form action="{{ route('slots.destroy', $slot->id) }}" method="POST"> 
                                        @method('DELETE')
                                            <button class="btn btn-sm btn-error btn-action tooltip tooltip-right" data-tooltip="Remove Slot">
                                                <svg class="icon">
                                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                                                </svg>
                                            </button>
                                        @csrf
                                    </form>
                                @endif
        
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">
                                <h3 class="posts_cnt__item text-center">No slots</h3> 
                            </td>
                        </tr>
                    @endforelse
                    
                </tbody>
            </table>
        </div>
        
    </section>
@endsection

@section('pagination')

    @if(count($slots) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-12">
                    {{ $slots->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('sidebar')
    @include('dashboard.inc.wide_sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush