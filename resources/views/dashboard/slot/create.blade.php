@extends('layouts.main_dashboard')

@section('title', 'Add new slot')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-8 col-md-12 col-mx-auto add_slot_form">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Add new slot</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
        @include('inc.errors')
        <div class="create_slot">
            <form action="{{ route('slots.store') }}" method="POST"> 
                <div class="form-group">
                    <label class="form-label" for="name">Slot Name</label>
                    <input class="form-input" type="text" name="name" value="{{ old('name') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="casino">Casino Name</label>
                    <select class="form-select" name="casino">
                        @foreach ($casinos as $casino)
                            <option value="{{ $casino->id }}" {{ old('casino') == $casino->id ? 'selected' : ''}}>{{ $casino->name }}</option>  
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label" for="casino">Provider</label>
                    <input class="form-input" type="text" name="provider" value="{{ old('provider') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="gameid">Game Id</label>
                    <input class="form-input" type="text" name="gameid" value="{{ old('gameid') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="form-switch form-inline">
                        <input type="checkbox" name="active" value="{{ old('active') ?? '0' }}" {{ old('active') !== null ? 'checked' : '' }}>
                        <i class="form-icon"></i> Active
                    </label>
                </div>
                
                @csrf
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Add slot" class="btn btn-lg" id="submit">
                </div>
            </form>
            
        </div> 
       
    </section> 
@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush