@extends('layouts.main_dashboard')

@section('title', 'Manage Slot Jackpots')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content')
    
    <section class="column col-8 col-md-12 index_jackpots">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Slot Jackpots</h1>
        </header>

        <div class="card">
            <div class="card-header">
                <div class="card-title h4">Add Jackpot</div>
            </div>
            <div class="card-footer">
                <a href="{{ route('jackpots.create') }}" class="btn btn-lg btn-primary" id="add_slot">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                    </svg> 
                    Add New
                </a>
            </div>
        </div>

        <div class="jackpots mt">

            @include('inc.errors')
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Slot Name</th>
                        <th>Casino Name</th>
                        <th>Slot Id</th>
                        <th>Jackpot type</th>
                        <th>Jackpot name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($jackpots) > 0)
                        @foreach ($jackpots as $jackpot)
                            <tr>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->id }}
                                    </span>
                                </td>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->slot_name }}
                                    </span>
                                </td>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->casino_name }}
                                    </span>
                                </td>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->slot_id }}
                                    </span>
                                </td>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->type }}
                                    </span>
                                </td>
                                <td>
                                    <span class="jackpots_data">
                                        {{ $jackpot->jackpot_name }}
                                    </span>
                                </td>
                                <td> 
                                    <div class="jackpot_edit_btns">
                                        
                                        <button class="btn btn-sm btn-action tooltip tooltip-left modal_trigger" data-tooltip="Edit Jackpot">
                                            <svg class="icon">
                                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                                            </svg> 
                                        </button>

                                        <div class="modal modal update_jackpot_modal">
                                            <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                            <div class="modal-container">
                                                <div class="modal-header">
                                                    <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                                    <div class="modal-title h5">Update {{ ucfirst($jackpot->id) }}</div>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="content">
                                                        <form action="{{ route('jackpots.update', $jackpot->id) }}" method="POST"> 
                                                            @method('PUT')
                                                            
                                                            <div class="form-group">
                                                                <label class="form-label" for="casino-id">Casino Name</label>
                                                                <select class="form-select" name="casino-id" id="casino-id">

                                                                    @foreach ($casinos as $casino)
                                                                        @if ($jackpot->casino_name === $casino->name)
                                                                            <option value="{{ $casino->id }}" selected>{{ $casino->name }}</option> 
                                                                        @else
                                                                            <option value="{{ $casino->id }}">{{ $casino->name }}</option> 
                                                                        @endif   
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="slot-id">Slot Name</label>
                                                                <select class="form-select" name="slot-id" id="slot-id">

                                                                    @foreach ($slots as $slot)
                                                                        @if ($jackpot->slot_name === $slot->name)
                                                                            <option value="{{ $slot->id }}" selected>{{ $slot->name }}</option> 
                                                                        @else
                                                                            <option value="{{ $slot->id }}">{{ $slot->name }}</option> 
                                                                        @endif   
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="jackpot-type">Jackpot type</label>
                                                                <input class="form-input" type="text" name="jackpot-type" id="jackpot-type" value="{{ $jackpot->type }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="jackpot-name">Jackpot name</label>
                                                                <input class="form-input" type="text" name="jackpot-name" id="jackpot-name" maxlength="40" value="{{ $jackpot->jackpot_name }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <button class="btn">Update</button>
                                                            </div>
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <button class="btn btn-sm btn-error btn-action tooltip tooltip-left delet_jackpot_btn" data-tooltip="Remove Jackpot">
                                            <svg class="icon">
                                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                                            </svg>
                                        </button>
                                         
                                        <div class="modal modal-sm confirm_jackpot_delete_modal">
                                            <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                            <div class="modal-container">
                                                <div class="modal-header">
                                                    <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                                    <div class="modal-title h5">Remove  jackpot{{ ucfirst($jackpot->id) }}?</div>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="content">
                                                        <form action="{{ route('jackpots.destroy', $jackpot->id) }}" method="POST"> 
                                                            @method('DELETE')
                                                                <div class="form-group text-center">
                                                                    <button class="btn btn-action">{{ __('Yes') }}</button>
                                                                    <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                                                </div>
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">
                                <h3 class="posts_cnt__item text-center">No slots</h3> 
                            </td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        
    </section>
@endsection

@section('pagination')

    @if(count($jackpots) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $jackpots->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush