@extends('layouts.main_dashboard')

@section('title', 'Add new jackpot')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-8 col-md-12 col-mx-auto add_jackpot_form">

        <header>
            <h1 class="text-right dasboard_new_post_heading">Add new jackpot</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
        @include('inc.errors')
        <div class="create_slot">
            <form action="{{ route('jackpots.store') }}" method="POST"> 
                <div class="form-group @error('casino-id') has-error @enderror">
                    <label class="form-label" for="casino-id">Casino Name</label>
                    <select class="form-select" name="casino-id" id="casino-id">
                        @foreach ($casinos as $casino)
                            <option value="{{ $casino->id }}" {{ old('casino') == $casino->id ? 'selected' : ''}}>{{ $casino->name }}</option>  
                        @endforeach
                    </select>
                </div>

                <div class="form-group @error('slot-id') has-error @enderror">
                    <label class="form-label" for="slot-id">Slot Name</label>
                    <select class="form-select" name="slot-id" for="slot-id">
                        @foreach ($slots as $slot)
                            <option value="{{ $slot->id }}" {{ old('slot-id') == $slot->id ? 'selected' : ''}}>{{ $slot->name }}</option> 
                        @endforeach
                    </select>
                </div>
                <div class="form-group @error('jackpot-type') has-error @enderror">
                    <label class="form-label" for="jackpot-type">Jackpot type</label>
                    <input class="form-input" type="text" name="jackpot-type" id="jackpot-type" value="{{ old('jackpot-type') ?? '' }}">
                </div>
                <div class="form-group @error('jackpot-name') has-error @enderror">
                    <label class="form-label" for="jackpot-name">Jackpot name</label>
                    <input class="form-input" type="text" name="jackpot-name" id="jackpot-name" maxlength="40" value="{{ old('jackpot-name') ?? '' }}">
                </div>
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Add jackpot" class="btn btn-lg" id="submit">
                </div>
                @csrf
            </form>
            
        </div> 
       
    </section> 
@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush