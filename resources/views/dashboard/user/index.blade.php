@extends('layouts.main_dashboard')

@section('title', 'Manage Articles')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 

    <section class="column col-8 col-md-12 col-mx-auto manage_posts">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Users</h1>
        </header>
        <div>
            
            @include('inc.errors')
            
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Registered at</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $user)
                        <tr class="text-small">
                            <td>
                                <span>
                                    {{ $user->id }}
                                </span>
                            </td>
                            <td>
                                <span>
                                    {{ $user->name }}
                                </span>
                            </td>
                            <td>
                                <span @isset($user->email_verified_at) class="badge" data-badge="{{__('✓')}}" @endisset>
                                    {{ $user->email }}
                                </span>
                            </td>
                            <td>
                                <span>
                                    {{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}
                                </span>
                            </td>
                            <td>
                                <a href="#" class="btn btn-sm btn-error btn-action tooltip tooltip-left delete_post_btn" data-tooltip="Delete User">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                                    </svg> 
                                </a>
                                <div class="modal modal-sm confirm_post_delete_modal">
                                    <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                    <div class="modal-container">
                                        <div class="modal-header">
                                            <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                            <div class="modal-title h5">Remove {{ $user->name }}?</div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="content">
                                                <form action="{{ route('users.destroy', $user->id) }}" method="POST"> 
                                                    @method('DELETE')
                                                    <div class="form-group text-center">
                                                        <button class="btn btn-action">{{ __('Yes') }}</button>
                                                        <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                                    </div>
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">
                                <h3 class="text-center">No users</h3> 
                            </td>
                        </tr>
                    @endforelse
                    
                </tbody>
            </table>
        </div>
    </section>

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('pagination')
    @if(!empty($users))
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $users->links() }}
                </div>
            </div>
        </section>
    @endif
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush