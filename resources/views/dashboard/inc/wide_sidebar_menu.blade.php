<div class="column col-12 dashboard_sidebar file_manager_menu">
    <ul class="menu">
        <!-- menu header text -->
        <li class="divider" data-content="MENU"></li>
        <!-- menu item standard -->
        <li class="menu-item">
            <a href="{{ route('dashboard') }}">
                Articles
            </a>  
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_posts ?? 0 }}</label>
            </div>
        </li>
        <li class="menu-item">
            <a href="{{ route('streams.index') }}">
                Streams 
            </a>        
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_streams ?? 0 }}</label>
            </div>        
        </li>
        <li class="menu-item">
            <a href="{{ route('categories.index') }}">
                Categories
            </a>
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_categories ?? 0 }}</label>
            </div>
        </li>
        <li class="menu-item">
            <a href="{{ route('dashboard.comments.index') }}">
                Comments
            </a>
            <div class="menu-badge">
                <label class="label label-primary">{{ $unapproved_comments ?? 0 }}</label>
            </div>
        </li>    
        <li class="menu-item">
            <a href="{{ route('casinos.index') }}">
                Casinos
            </a>
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_casinos ?? 0 }}</label>
            </div>
        </li>
        <li class="menu-item">
            <a href="{{ route('slots.index') }}">
                Slots
            </a>
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_slots ?? 0 }}</label>
            </div>
        </li>
        <li class="menu-item">
            <a href="{{ route('jackpots.index') }}">
                Slot Jackpots
            </a>
            <div class="menu-badge">
                <label class="label label-primary">{{ $total_jackpots ?? 0 }}</label>
            </div>
        </li>
        <li class="menu-item">
            <a href="{{ route('attachments.index') }}">
                Attachments
            </a>
        </li>
        <li class="menu-item">
            <a href="{{ route('users.index') }}">
                Users
            </a>
        </li>
    </ul>
</div>