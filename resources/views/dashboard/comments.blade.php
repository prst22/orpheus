@extends('layouts.main_dashboard')

@section('title', 'Manage Comments')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content')
    
    <section class="column col-8 col-md-12 edit_comments">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Comments</h1>
        </header>

        @include('inc.errors')
        
        @forelse($comments as $key => $comment) 
            <div class="comment">
                <p>
                    <small class="author_name @if($comment->commenter_type === 'App\Admin') text-bold text-primary @endif">
                        <i> @if($comment->commenter_type === 'App\Admin') Admin @endif {{ $comment->name ?? $comment->guest_name }} </i>
                    </small><br>
                    <small>
                        {{ $comment->comment }}
                    </small>
                </p>
                <div class="comment__controlls">
                    @if (!$comment->approved)
                        <form method="POST" action="{{ route('dashboard.comments.approve', $comment->id) }}">
                            @csrf
                            <button class="btn btn-sm tooltip tooltip-left" data-tooltip="Approve Comment">Approve</button>  
                        </form>  
                    @endif
                    <form method="POST" action="{{ route('dashboard.comments.destroy', $comment->id) }}">
                        @csrf
                        <button class="btn btn-sm btn-action btn-error tooltip tooltip-left delete_comment_btn" data-tooltip="Delete Comment">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                            </svg> 
                        </button>  
                    </form>  
                </div>
            </div>
        @empty
            <h3 class="posts_cnt__item text-center">No comments</h3> 
        @endforelse
        
    </section>
@endsection

@section('pagination')

    @if(count($comments) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $comments->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush