@extends('layouts.main_dashboard')

@section('title', 'Manage Categories')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content')
    
    <section class="column col-8 col-md-12 edit_categories">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Categories</h1>
        </header>
        @include('inc.errors')
        @forelse($categories as $key => $category) 
            <div class="tile">
                <div class="tile-content">
                    <p class="tile-title">
                        {{ ucfirst($category->name) }} 
                        <small class="category_type">
                            <i>{{ $category->type }} category</i>
                        </small>
                    </p>
                </div>
                <div class="tile-action edit_category_btns">
                    <button class="btn btn-sm btn-action tooltip tooltip-left modal_trigger" data-tooltip="Edit Category">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                        </svg> 
                    </button>
                    <button href="#" class="btn btn-sm btn-action btn-error tooltip tooltip-left delete_category_btn" data-tooltip="Delete Category">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                        </svg> 
                    </button>
                    <div class="modal modal-sm update_category_modal">
                        <a href="#close" class="modal-overlay" aria-label="Close"></a>
                        <div class="modal-container">
                            <div class="modal-header">
                                <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                <div class="modal-title h5">Update {{ ucfirst($category->name) }}</div>
                            </div>
                            <div class="modal-body">
                                <div class="content">
                                    <form action="{{ route('categories.update', $category->id) }}" method="POST"> 
                                        @method('PUT')
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="form-inline pr-2">For: </span>
                                                <label class="form-radio form-inline">
                                                    <input type="radio" name="type" value="posts" {{ ($category->type === 'posts') ? 'checked' : '' }}><i class="form-icon"></i> Posts
                                                </label>
                                                <label class="form-radio form-inline">
                                                    <input type="radio" name="type" value="streams" {{ ($category->type === 'streams') ? 'checked' : '' }}><i class="form-icon"></i> Streams
                                                </label>
                                            </div>
                                            <div class="input-group">
                                                <input class="form-input" type="text" name="name" value="{{ $category->name }}">
                                                <button class="btn">Update</button>
                                            </div>
                                        </div>
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal modal-sm confirm_category_delete_modal">
                        <a href="#close" class="modal-overlay" aria-label="Close"></a>
                        <div class="modal-container">
                            <div class="modal-header">
                                <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                <div class="modal-title h5">Remove {{ ucfirst($category->name) }} Category?</div>
                            </div>
                            <div class="modal-body">
                                <div class="content">
                                    <form action="{{ route('categories.destroy', $category->id) }}" method="POST"> 
                                        @method('DELETE')
                                        <div class="form-group text-center">
                                            <button class="btn btn-action">{{ __('Yes') }}</button>
                                            <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                        </div>
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h3 class="posts_cnt__item text-center">No categories</h3> 
        @endforelse
        <div class="add_category_cnt my-2 py-2">
            <button class="btn add_category_cnt__btn">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                </svg> 
                Add Category
            </button>
            <div class="modal modal-sm create_category_modal">
                <a href="#close" class="modal-overlay" aria-label="Close"></a>
                <div class="modal-container">
                    <div class="modal-header">
                        <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                        <div class="modal-title h5">{{ __('Create Category') }}</div>
                    </div>
                    <div class="modal-body">
                        <div class="content">
                            <form action="{{ route('categories.store') }}" method="POST"> 
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="form-inline pr-2">For: </span>
                                        <label class="form-radio form-inline">
                                            <input type="radio" name="type" value="posts" checked=""><i class="form-icon"></i> Posts
                                        </label>
                                        <label class="form-radio form-inline">
                                            <input type="radio" name="type" value="streams"><i class="form-icon"></i> Streams
                                        </label>
                                    </div>
                                    <div class="input-group">
                                        <input class="form-input" type="text" name="name" placeholder="{{ __('Category Name') }}">
                                        <button class="btn">Create</button>
                                    </div>
                                </div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush