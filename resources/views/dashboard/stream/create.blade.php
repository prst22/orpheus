
@extends('layouts.main_dashboard')

@section('title', 'Add New Stream')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-8 col-md-12 col-mx-auto add_post_form">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Add New Stream</h1>
        </header>
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-primary mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>

        @include('inc.errors')
        
        <div class="form-group">
            <form action="{{ route('streams.store') }}" method="POST">
                @if( !empty($categories) )
                    <div class="form-label">Categories</div>
                    <div class="form-group">
                        @foreach($categories as $key => $category)
                            @php
                                $category_checkbox_values = old("category") ?? '';
                                if(!empty($category_checkbox_values)){
                                    $found_key = array_search($category->id, $category_checkbox_values); 
                                    $checked = $found_key !== false ? $category_checkbox_values[$found_key] : false;
                                }
                            @endphp
                            @if (!empty($category_checkbox_values))
                                @if($category->id == $checked)
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}" checked>
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @else
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{  $category->id }}">
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label> 
                                @endif
                            @else
                                @if($key === 0)
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}" checked>
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @else
                                    <label class="form-checkbox form-inline">
                                        <input type="checkbox" name="category[]" value="{{ $category->id }}">
                                        <i class="form-icon"></i> {{ $category->name }}
                                    </label>
                                @endif
                            @endif
                        @endforeach
                    </div>
                @endif
                <div class="form-group">
                    <label class="form-label" for="channel_name">Channel Name</label>
                    <input class="form-input" type="text" id="channel_name" name="channel_name" value="{{ old('channel_name') ?? '' }}" placeholder="Channel Name">
                </div>
                <div class="form-group">
                    <label class="form-label" for="channel_id">Channel Id</label>
                    <input class="form-input" id="channel_id" name="channel_id" placeholder="Channel Id" type="number" max="999999999" value="{{ old('channel_id') ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="description">Channel Description</label>
                    <textarea class="form-input" id="description" name="description" rows="5">
                        {{ old('description') ?? '' }}
                    </textarea>
                </div>
                @csrf
                <div class="form-group text-center my-2 py-2">
                    <input type="submit" value="Create" class="btn btn-lg">
                </div>
            </form>
        </div>
    </section> 
@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush