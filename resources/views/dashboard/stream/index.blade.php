
@extends('layouts.main_dashboard')

@section('title', 'Manage Streams')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    
    <section class="column col-8 col-md-12 col-mx-auto manage_posts">
        <header>
            <h1 class="text-right dasboard_new_post_heading">Manage Streams</h1>
        </header>
        <div class="card">
            <div class="card-header">
                <div class="card-title h4">Create Stream</div>
            </div>
            <div class="card-footer">
                <a href="{{ route('streams.create') }}" class="btn btn-lg btn-primary">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#plus"></use>
                    </svg> 
                    Create New
                </a>
            </div>
        </div>
        <div class="edit_posts mt">
            
            @include('inc.errors')

            @forelse($streams as $stream)
                <div class="edit_posts__post">
                    <h4>{{ $stream->channel_name }}</h4>
                    <div class="edit_post_btns">
                        <a href="{{ route('streams.edit', $stream->id) }}" class="btn btn-action tooltip tooltip-left" data-tooltip="Edit Stream">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                            </svg> 
                        </a>
                        <a href="#" class="btn btn-error btn-action tooltip tooltip-left delete_stream_btn" data-tooltip="Delete Stream">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                            </svg> 
                        </a>
                        <div class="modal modal-sm confirm_stream_delete_modal">
                            <a href="#close" class="modal-overlay" aria-label="Close"></a>
                            <div class="modal-container">
                                <div class="modal-header">
                                    <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                    <div class="modal-title h5">Remove {{ ucfirst($stream->channel_name) }}?</div>
                                </div>
                                <div class="modal-body">
                                    <div class="content">
                                        <form action="{{ route('streams.destroy', $stream->id) }}" method="POST"> 
                                            @method('DELETE')
                                            <div class="form-group text-center">
                                                <button class="btn btn-action">{{ __('Yes') }}</button>
                                                <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                            </div>
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            @empty
                <h3 class="posts_cnt__item text-center">No streams</h3> 
            @endforelse
        </div>
    </section>

@endsection

@section('sidebar')
    @include('dashboard.inc.sidebar_menu')
@endsection

@section('pagination')
    <section class="container grid-lg pag_cnt">
        <div class="columns">
            <div class="column col-md-12 col-8 col-ml-auto">
                {{ $streams->links() }}
            </div>
        </div>
    </section>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush