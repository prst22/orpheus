@extends('layouts.wide')

@section('title', 'Manage Attachments')

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}" />   
@endpush

@section('content') 
    <div class="column col-12">
        @include('inc.errors')
    </div>
    
    @forelse($attachments as $attachment)
        @php
            $src = $attachment->image['2'] ?? $attachment->image['1'] ?? $attachment->image['0'];
            $parsed_url = parse_url($src);
            $pieces = explode("/", $parsed_url['path']);
            $date = isset($pieces[2]) ? str_replace('-', '/', $pieces[2]) : '';
        @endphp
        <div class="column col-sm-6 col-md-4 col-lg-3 col-2 attachment">
            <img src="{{ $src }}" alt="attachment">
            <div class="attachment__info mt-2 p-2">
                <button class="btn btn-sm btn-action btn-error remove_attachment_btn">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#trash"></use>
                    </svg> 
                </button>
                <span>
                    <small>{{ $date }}</small>
                </span>
            </div>
            
            <div class="modal modal-sm remove_attachment_modal">
                <a href="#close" class="modal-overlay" aria-label="Close"></a>
                <div class="modal-container">
                    <div class="modal-header">
                        <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                        <div class="modal-title h5">Remove attachment with id:{{ $attachment->id }}</div>
                    </div>
                    <div class="modal-body">
                        <div class="content">
                            <form action="{{ route('attachments.destroy', $attachment->id) }}" method="POST"> 
                                @method('DELETE')
                                <div class="form-group text-center">
                                    <button class="btn btn-action">{{ __('Yes') }}</button>
                                    <a href="#close" class="btn btn-action">{{ __('No') }}</a>
                                </div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
       <div class="column col-12">
            <h3 class="text-center">No attachments</h3>
        </div>
    @endforelse 

@endsection

@section('sidebar')
    <header class="column col-12">
        <h1 class="text-right dasboard_new_post_heading">Manage Attachments</h1>
    </header>    
    @includeWhen($filemanager, 'dashboard.inc.wide_sidebar_menu')
@endsection

@section('pagination')

    @if(count($attachments) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-mx-auto">
                    {{ $attachments->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/dashboard/main.js') }}"></script>
@endpush