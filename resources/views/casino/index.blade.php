@extends('layouts.main')

@section('title')Top online casios {{ date('Y') }}@endsection

@section('meta_description', 'List of top online casinos')

@section('meta_keywords', 'casino, casinos, top online casinos, online casinos reviews, casino ratings, independent casino rating, best casinos')

@push('header_styles')
    <style>
        .blur-up {
            -webkit-filter: blur(5px);
            filter: blur(5px);
            transition: filter 400ms, -webkit-filter 400ms;
        }
        .blur-up.lazyloaded {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    </style>
@endpush

@section('content') 
    <div class="column col-md-12 col-12 casio_list_cnt">
        @if(count($casinos) > 0)
            <header>
                <h1>Top online casios {{ date('Y') }}</h1>
            </header>
            
            @each('casino.inc.item', $casinos, 'casino')

        @else
            <div class="casino_card_cnt">
                <h3 class="casino_cnt__item text-center">{{ __('No casinos found') }}</h3>
            </div>
        @endif       
    </div>        
@endsection

@section('pagination')

    @if(count($casinos) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-mx-auto">
                    {{ $casinos->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush