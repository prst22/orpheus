<span class="game_type_logo game_type_logo--index mb-1 mr-1 tooltip" data-tooltip="{{ Str::of($game)->ucfirst()->replace('-', ' ') }}">
    <svg class="icon">
        <use xlink:href="{{ URL::asset('assets') }}/game-types.svg#{{ $game }}"></use>
    </svg> 
</span>
                        