@foreach ($items as $item)
    @php
        $item_name = Str::of($item)->ucfirst()->replace('-', ' ');
    @endphp
    {{ $loop->last ? $item_name : $item_name . ',' }}    
@endforeach
