<div class="casino_card_cnt card">
    <div class="casino_card card-body">
        <div class="casino_card__heading_cnt mb_1">
            
            @if(!empty($casino->getLogo('md')))
                <div class="casino_logo_cnt">

                    @if($casino->getLogo('md'))
                        @if(!empty($casino->url))
                            <a href="{{ $casino->url }}" target="_blank" rel="nofollow">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ $casino->getLogo('md') }}" alt="{{ $casino->name }}" class="casino_logo_cnt__image blur-up lazyload">  
                            </a> 
                        @else
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ $casino->getLogo('md') }}" alt="{{ $casino->name }}" class="casino_logo_cnt__image blur-up lazyload">
                        @endif
                    @endif

                </div>
            @endif

            <h2 class="casino_heading @if(empty($casino->getLogo('md')) && empty($casino->getLogo('lg'))) casino_heading--no_logo @endif">

                @if(!empty($casino->url))
                    <a href="{{ $casino->url }}" target="_blank" rel="nofollow">{{ $casino->name }}</a>
                @else
                    <span>{{ $casino->name }}</span>
                @endif

                @if($casino->rating)
                    <span class="casino_rating">
                        <span class="counter"></span>
                        <svg class="rating" width="110" height="110" viewPort="0 0 55 55" version="1.1" xmlns="http://www.w3.org/2000/svg" data-rating="{{ $casino->rating }}">
                            <circle class="rating__bg" r="47" cx="55" cy="55" fill="transparent" stroke-dasharray="295.30" stroke-dashoffset="0"></circle>
                            <circle class="rating__bar" r="47" cx="55" cy="55" fill="transparent" stroke-dasharray="295.30" stroke-dashoffset="295.30"></circle>
                        </svg>
                    </span>
                @endif

            </h2>
        </div>

        @if(!empty($casino->info))
            @if(isset($casino->info['games']))
                <div class="casino_card__info mb_1">
                    @each('casino.inc.info_game_index', $casino->info['games'], 'game')
                </div>
            @endif

            @if(isset($casino->info['payment-methods']))
                @php
                    $payment_methods = collect($casino->info['payment-methods'])->take(11)->all();
                @endphp
                <div class="payment_methods mb_1">
                    <b class="text-gray mr-1">Payment methods: </b>
                    @includeWhen(count($casino->info['payment-methods']) > 0, 'casino.inc.info_payment_index', $payment_methods)
                    @if(count($casino->info['payment-methods']) > 10)
                        <a href="{{ route('casinos.show', $casino->slug) }}#payment-method" class="btn btn-sm">All {{ count($casino->info['payment-methods']) }} methods</a> 
                    @endif
                </div>
            @endif

        @endif

        @if(!empty($casino->description))
            <div class="casino_card__description">
                <b class="text-gray">Description: </b>
                {{ Str::of($casino->description)->limit(380)->ltrim() }}
            </div>
        @endif

        <a href="{{ route('casinos.show', $casino->slug) }}" class="btn btn-lg mt-2">More details</a>
    </div>
</div>