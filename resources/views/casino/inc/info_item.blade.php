<div class="columns single_casino mb_1">
    <div class="column col-md-12 col-2 col-ml-auto">
        <b>{{ Str::of($key)->ucfirst()->replace('-', ' ') }}:</b>
    </div>
    <div class="column col-md-12 col-lg-8 col-6 col-mr-auto">
        
            @switch($key)
                @case('games')
                    <div class="single_casino__info">
                        @each('casino.inc.info_game', $items, 'game')
                    </div>
                    @break

                @case('providers')
                    <div class="single_casino__info">
                        @includeWhen(count($items) > 0, 'casino.inc.info_default', $items)
                    </div>
                    @break

                @case('payment-methods')
                    <div class="single_casino__info payment_methods" id="payment-method">
                        @foreach ($items as $method)
                            @php
                                
                                $method_name = Str::of($method)->ucfirst()->replace('-', ' ');
                                $method_name_arr = Str::of($method)->split('/\-/')->take(2);
                                $data_initials = $method_name_arr->map(fn ($item) => (string) Str::of($item)->substr(0, 1)->upper())
                                    ->implode('');
                        
                            @endphp
                            
                            <span class="chip">
                                <figure class="avatar avatar-sm" data-initial="{{ $data_initials }}"></figure>
                                {{ $method_name }}
                            </span>
                        @endforeach
                    </div>
                    @break

                @default
                    @includeWhen(count($items) > 0, 'casino.inc.info_default', $items)
            @endswitch 
        
    </div>
</div>