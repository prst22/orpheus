@foreach ($payment_methods as $method)
    @php
        
        $method_name = Str::of($method)->ucfirst()->replace('-', ' ');
        $method_name_arr = Str::of($method)->split('/\-/')->take(2);
        $data_initials = $method_name_arr->map(
            fn ($item) => (string) Str::of($item)->substr(0, 1)->upper()
        )->implode('');

    @endphp
    
    <span class="chip">
        <figure class="avatar avatar-sm" data-initial="{{ $data_initials }}"></figure>
        {{ $method_name }}
    </span>
@endforeach
                            