@extends('layouts.wide')

@php
    $year = date('Y');
    $logo_md = $casino->getLogo('md'); 
    $contains = Str::contains($casino->name, ['casino', 'Casino']);     
@endphp

@if($contains)
    @section('title', "$casino->name review {$year}")   
@else
    @section('title', "$casino->name casino review {$year}")   
@endif

@section('meta_description', Str::limit($casino->description, 120))

@push('header_styles')
    <style>
        .mediabox-img.ls-blur-up-is-loading,
        .mediabox-img.lazyload:not([src]) {
            visibility: hidden;
        }
        .ls-blur-up-img,
        .mediabox-img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            object-fit: cover;
        }
        .ls-blur-up-img {
            filter: blur(10px);
            opacity: 1;
            transition: opacity 1000ms, filter 1500ms;
        }
        .ls-blur-up-img.ls-inview.ls-original-loaded {
            opacity: 0;
            filter: blur(5px);
        }
    </style>

    @isset ($logo_md)
        <style>
            .main{
                position: relative;
            }
            .main::after{ 
                content: "";
                display: block;
                background: url({{ $logo_md }}) left bottom no-repeat;
                background-size: contain;
                height: 100px;
                width: calc( 100% - 2.4rem );
                max-width: calc( 100% - 2.4rem );
                opacity: 0.03;
                position: absolute;
                bottom: 0;
                left: 1.2rem;
                pointer-events: none;
                z-index: -10;
            }
        </style>
    @endisset
@endpush

@section('content') 

    <div class="column col-12 col-mx-auto">
        @include('inc.errors')

        @if(!empty($casino->getBanner('md')) || !empty($casino->getBanner('lg')))
            <div class="casino_top_banner_cnt">
                @if(!empty($casino->getBanner('md')) && !empty($casino->getBanner('lg')))
                    <div class="casino_top_banner_cnt__banner">
                        <img data-sizes="auto" 
                            data-lowsrc="{{ $casino->getBanner('sm') }}" 
                            data-srcset="{{ $casino->getBanner('sm') }} 400w, {{ $casino->getBanner('md') }} 836w, {{ $casino->getBanner('lg') }} 1360w" 
                            alt="{{ $casino->name }}" 
                            class="mediabox-img lazyload">
                        @if(!empty($casino->rating)) <div class="single_casino_rating_bg"></div> @endif 
                    </div>   
                    @if(!empty($casino->rating))
                        <div class="casino_rating casino_rating--single">
                            <div class="counter"></div>
                            <svg class="rating" width="140" height="140" viewPort="0 0 70 70" version="1.1" xmlns="http://www.w3.org/2000/svg" data-rating="{{ $casino->rating }}">
                                <circle class="rating__bg" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="0"></circle>
                                <circle class="rating__bar" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="364.42"></circle>
                            </svg>
                        </div>
                    @endif 
                @endif
                @if(!empty($casino->url))
                    <a href="{{ $casino->url }}" target="_blank" rel="nofollow" class="casino_top_banner_cnt__logo" aria-label="{{ $casino->name }} logo">
                        <img src="{{ $casino->getLogo('md') }}" alt="{{ $casino->name }} logo">
                    </a>
                @else
                    <div class="casino_top_banner_cnt__logo">
                        <img src="{{ $casino->getLogo('md') }}" alt="{{ $casino->name }} logo">
                    </div>
                @endif
            </div>
        @else
            @if(!empty($casino->getLogo('lg')))
                <div class="casino_top_banner_cnt casino_top_banner_cnt--no_banner"> 
                    @if(!empty($casino->url))
                        <a href="{{ $casino->url }}" target="_blank" rel="nofollow" class="casino_top_banner_cnt__logo--lg" aria-label="{{ $casino->name }} logo">
                            <img src="{{ $casino->getLogo('lg') }}" alt="{{ $casino->name }} logo">
                        </a>
                    @else
                        <div class="casino_top_banner_cnt__logo--lg">
                            <img src="{{ $casino->getLogo('lg') }}" alt="{{ $casino->name }} logo">
                        </div>
                    @endif

                    @if(!empty($casino->rating))
                        <div class="casino_rating casino_rating--single_no_banner">
                            <div class="counter"></div>
                            <svg class="rating" width="140" height="140" viewPort="0 0 70 70" version="1.1" xmlns="http://www.w3.org/2000/svg" data-rating="{{ $casino->rating }}">
                                <circle class="rating__bg" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="0"></circle>
                                <circle class="rating__bar" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="364.42"></circle>
                            </svg>
                        </div>
                    @endif 
                </div>
            @endif       
        @endif
    </div>

    @if(!empty($casino->getLogo('lg')) || !empty($casino->getBanner('md')))
        
        @if(!empty($casino->url))
            <div class="column col-12 col-mx-auto">
                <div class="casino_link_cnt @if((!empty($casino->getBanner('md')) || !empty($casino->getBanner('lg')))) mt_1 @endif @if(empty($casino->getBanner('md')) || empty($casino->getLogo('lg'))) mb_1 @endif">
                    <a href="{{ $casino->url }}" target="_blank" rel="nofollow" class="btn btn-lg casino_play_btn">{{ __('Play') }}</a>
                </div>
            </div>
        @endif
        
    @endif
    
    <div class="column col-md-12 col-lg-10 col-8 col-mx-auto">
        
        @if(!empty($casino->getLogo('lg')) || !empty($casino->getBanner('lg')))
            <h1 class="single_casino_title">
                {{ $casino->name }}
            </h1>
        @else
            <div class="single_casino_title_cnt">
                <h1 class="single_casino_title">
                    {{ $casino->name }}
                </h1>
                @if(!empty($casino->rating))
                    <div class="casino_rating casino_rating--single_no_banner">
                        <div class="counter"></div>
                        <svg class="rating" width="140" height="140" viewPort="0 0 70 70" version="1.1" xmlns="http://www.w3.org/2000/svg" data-rating="{{ $casino->rating }}">
                            <circle class="rating__bg" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="0"></circle>
                            <circle class="rating__bar" r="58" cx="70" cy="70" fill="transparent" stroke-dasharray="364.42" stroke-dashoffset="364.42"></circle>
                        </svg>
                    </div>
                @endif 
            </div>
            
            @if(!empty($casino->url))
                <div class="casino_link_cnt @if((!empty($casino->getBanner('md')) || !empty($casino->getBanner('lg')))) mt_1 @endif @if(empty($casino->getBanner('md')) || empty($casino->getLogo('lg'))) mb_1 @endif">
                    <a href="{{ $casino->url }}" target="_blank" rel="nofollow" class="btn btn-lg casino_play_btn">{{ __('Play') }}</a>
                </div>
            @endif

        @endif
        
    </div>
    <div class="container grid-xl">
        @if(!empty($casino->info) && (count($casino->info) > 0))
            @each('casino.inc.info_item', $casino->info, 'items')
        @endif

        @if(!empty($casino->description))   
            <div class="columns single_casino">
                <div class="column col-md-12 col-2 col-ml-auto">
                    <b>{{ __('Description') }}:</b>
                </div>
                <div class="column col-md-12 col-lg-8 col-6 col-mr-auto">            
                    <div class="single_casino__description">
                        <p>{{ Str::of($casino->description)->ltrim() }}</p>
                    </div>
                </div>
            </div> 
        @endif 
        <div class="columns"> 
            <div class="column col-md-12 col-lg-10 col-8 col-mx-auto">
                <div class="bottom_controlls">

                    @auth('admin')
                        <a href="{{ route('casinos.edit', $casino->id) }}" class="btn btn-lg btn-action btn-success ml-2 single_edit_btn">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                            </svg> 
                        </a>
                    @endauth   

                    <a href="{{ url()->previous() }}" class="btn btn-lg ml-2 single_back_btn {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                        </svg> 
                        Back
                    </a>
                    
                </div>
            </div>
        </div>
    </div> 
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush