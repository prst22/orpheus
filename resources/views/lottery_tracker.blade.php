@extends('layouts.main')

@section('title', $title)

@section('meta_description', 'National lottery results. Powerball, Megamillions, Euromillions, Lotto Max')

@section('meta_keywords', 'lottery, lottery results, euromillions, euromillions results, lotto, lotto powerball jackpot, powerball results, megamillions, megamillions results, euromillions jackpot, national lottery')

@push('header_styles')
    <style>
        .blur-up {
            -webkit-filter: blur(5px);
            filter: blur(5px);
            transition: filter 400ms, -webkit-filter 400ms;
        }
        .blur-up.lazyloaded {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    </style>
@endpush

@section('content')
    <div class="column col-md-12 col-8 lottery_cnt">
        <header class="lottery_header">
            <h1 class="d-inline-block">Lottery results</h1>
            
            <div class="form-group">
                <select class="form-select" onchange="window.location.href = this.value">
                    <option value="{{ route('lottery-tracker') }}" {{ $name === 'all' ? 'selected' : '' }}>All</option>
                    <option value="{{ route('lottery-name', 'euromillions') }}" {{ $name === 'euromillions' ? 'selected' : '' }}>Euromillions</option>
                    <option value="{{ route('lottery-name', 'lottomax') }}" {{ $name === 'lottomax' ? 'selected' : '' }}>Lottomax</option>
                    <option value="{{ route('lottery-name', 'megamillions') }}" {{ $name === 'megamillions' ? 'selected' : '' }}>Megamillions</option>
                    <option value="{{ route('lottery-name', 'powerball') }}" {{ $name === 'powerball' ? 'selected' : '' }}>Powerball</option>
                </select>
            </div>
        </header> 
        <p>Gamblingcentral is the easiest way to follow your favourite lotto game results. 
            The Lottery Results Tracker allows you to track any lottery draw. 
            It shows you the numbers that are most important to you, and helps you see what numbers progress to the next round of a jackpot.</p> 
        <div class="container px-0 mt_2">
            <div class="columns">
                @if(count($jackpots) > 0)    
                    @each('inc.lottery_card', $jackpots, 'jackpot')
                @else
                    <div class="column col-12">
                        <h3 class="text-center">{{ __('No jackpots') }}</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            @if($all_categories)
                <li class="divider" data-content="CATEGORY"></li>
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                </li>
                @foreach ($all_categories as $key => $category_link)
                    <li class="menu-item">{!! $category_link !!}</li>
                @endforeach
            @endif
        </ul>
    </div>

@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <aside class="menu">
            @if($all_categories)
                <ul class="category_btn" tabindex="0">
                    <li class="divider" data-content="CATEGORY">
                        <span id="category_icon">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-down"></use>
                            </svg> 
                        </span>
                    </li>
                </ul>
                <ul class="menu__inner">
                    <li class="menu-item">
                        <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                    </li>

                    @foreach ($all_categories as $key => $category_link)
                        <li class="menu-item">{!! $category_link !!}</li>
                    @endforeach
                </ul>
            @endif
        </aside>
    </div>

@endsection

@section('pagination')

    @if(count($jackpots) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $jackpots->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush