@extends('layouts.main')

@section('title', 'Profile')

@section('meta_description', 'Your profile page')

@section('content')

    <div class="column col-md-12 col-8 posts_cnt profile">
        <header>
            <h1>Profile</h1>
            <p>Name: {{ $user->name }}</p>
        </header> 
        <div>
            <p>
                Email: <span @isset($user->email_verified_at) class="badge" data-badge="{{__('✓')}}" @endisset>
                    {{ $user->email }}
                </span>
            </p>
        </div>

        @if ($user->email_verified_at === NULL)
            <div>
                <p> 
                    <a href="{{ route('verification.notice') }}">{{ __('Please verify your email') }}</a>
                </p>    
            </div>
        @endif

        <div class="my_comments mt">
            <h2 class="h3">My comments:</h2>
        
            @forelse($comments as $key => $comment) 
                <div class="my_comments__item">
                    <p>
                        {{ $comment->comment }}
                    </p>
                    <div class="comment_link">
                        <a href="{{ route('posts.show', $comment->slug) }}" class="btn btn-link">Go To Article</a>
                    </div>
                </div>
            @empty
                <p>No comments</p>
            @endforelse
        </div>
        {{ $comments->links() }}
        
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn btn-lg mt mb-2 {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
        </div>
    </div>

@endsection

@section('sidebar')
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            <li class="divider" data-content="{{ str_limit(Str::upper(Auth::user()->name), 14, '...') }}"></li>
       
            @auth('web')

                @if ($user->email_verified_at !== NULL)

                    <li class="menu-item">
                        <a href="{{ route('password.request') }}" class="categories__link">
                            Reset password
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#resend"></use>
                            </svg>
                        </a>
                    </li>

                @endif

            @endauth
        </ul>
    </div>
@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <ul class="menu">
            <li class="divider" data-content="{{ str_limit(Str::upper(Auth::user()->name), 14, '...') }}"></li>
       
            @auth('web')

                @if ($user->email_verified_at !== NULL)

                    <li class="menu-item">
                        <a href="{{ route('password.request') }}" class="categories__link">
                            Reset password
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#resend"></use>
                            </svg>
                        </a>
                        
                    </li>

                @endif

            @endauth
        </ul>
    </div>

@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush