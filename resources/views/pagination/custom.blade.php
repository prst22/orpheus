@if ($paginator->hasPages())
    <nav>
        <ul class="pagination pagination--custom text-center">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-left"></use>
                        </svg> 
                    </span>
                </li>
            @else
                <li class="page-item">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-left"></use>
                        </svg> 
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span><b>{{ $element }}</b></span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-right"></use>
                        </svg> 
                    </a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true">
                        <svg class="icon">
                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-right"></use>
                        </svg> 
                    </span>
                </li>
            @endif
        </ul>
    </nav>
@endif
