<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GC | @yield('title')</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="canonical" href="{{ url()->current() }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#4a7856">
        <meta name="theme-color" content="#ffffff">
        <meta name="robots" content="index, follow">
        @hasSection('meta_description')
        <meta name="description" content="@yield('meta_description')">
        <meta property="og:description" content="@yield('meta_description')">
        @else
        <meta name="description" content="Netent, yggdrasil, red tiger, microgaming, blueprint, quickspin, push gaming progressive jackpots tracker">
        <meta property="og:description" content="Netent, yggdrasil, red tiger, microgaming, blueprint, quickspin, push gaming progressive jackpots tracker">
        @endif
        <meta name="keywords" content="progressive jackpot, progressive jackpots, track progrssive jackpot, track slot jackpot, progressive slot jackpots, track slot jackpots, yggdrasil, red tiger, netent, microgaming, blueprint, quickspin, push gaming, jackpot tracker, divine fortune jackpot, mega fortune jackpot, mercy of the gods jackpot, empire fortune, joker millions, jackpot king">
        <meta property=og:type content="website">
        <meta property=og:locale content="en_US">
        <meta property="og:site_name" content="GamblingCentral">
        <meta property="og:url" content="{{ url()->current() }}">
        <meta property="og:title" content="@yield('title')">
        @hasSection('meta_image')
        <meta property="og:image" content="@yield('meta_image')">
        <meta property="og:image:secure_url" content="@yield('meta_image')">
        @else
        <meta property="og:image" content="{{ asset('assets/logo-social.jpg') }}">
        <meta property="og:image:secure_url" content="{{ asset('assets/logo-social.jpg') }}">
        @endif
        <style>
            .mobile_menu_wrapper .mobile_menu_cnt__inner.hidden{
                -webkit-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0);
            }
        </style>
        @stack('header_scripts')
        @stack('header_styles')
        <link rel="preload" href="{{ asset('fonts/Function-Regular.woff2') }}" as="font" type="font/woff2" crossorigin>
        <link rel="preload" href="{{ asset('assets/logo.svg') }}" as="image">
        <link rel="preload" href="{{ asset('assets/arabesque.png') }}" as="image">
        <link rel="stylesheet" href="{{ mix('css/main.css') }}" />    
        @stack('header_styles_after_main') 
    </head>
    
    <body @hasSection('footer_roulette') class="r_f" @endif>

        <div class="overflow"><!--menu wrapper for blur effect -->
            @include('inc.top_menu')

            <main class="main mt">
      
                @yield('content')

            </main>    
            
            @hasSection('footer_roulette')

                @yield('footer_roulette')

            @else

                @yield('footer')

            @endif

        </div>  

        @include('inc.mobile_menu')

        @stack('footer_scripts')

    </body>
</html>
