@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ URL::asset('assets') }}/logo.png" alt="{{ config('app.name') }}" style="width: 200px;height: auto;">
    @endcomponent
@endslot

<h1>Message From Contact Form</h1>
<b>From:</b> {{ $messageContent['email'] }}<br>
<b>Name:</b> {{ $messageContent['name'] }}<br>
<b>Message:</b> {{ $messageContent['message-body'] }}


@slot('footer')
    @component('mail::footer')
        &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
    @endcomponent
@endslot
@endcomponent
