@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ URL::asset('assets') }}/logo.png" alt="{{ config('app.name') }}" style="width: 200px;height: auto;">
    @endcomponent
@endslot

<h1>New comment created</h1>
<b>Comment:</b><br> 
<p>{{ $messageContent['message_body'] }}</p>
<p>{{ $messageContent['approved'] }}</p>

@component('mail::button', ['url' => $messageContent['comment_url']])
    Go to comment
@endcomponent

@slot('footer')
    @component('mail::footer')
        &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
    @endcomponent
@endslot
@endcomponent
