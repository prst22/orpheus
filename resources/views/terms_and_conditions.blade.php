@extends('layouts.main')

@section('title', 'Terms and Conditions')

@section('meta_description', 'Our terms and conditions outline the rules and regulations for using our website or services. Read through our agreement to understand your rights and responsibilities as a user. Protect yourself and stay informed with our terms and conditions.')

@section('content') 
    <div class="column col-md-12 col-8 posts_cnt">
        <h1>Terms and Conditions</h1>
        <h2>1. Introduction</h2>
        <p>Welcome to our site about gambling. By using our website, you agree to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern our relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.</p>
        <h2>2. Gambling Responsibility</h2>
        <p>Gambling can be addictive, and we encourage our users to gamble responsibly. We provide information and tools to help our users gamble responsibly and seek help if needed. We do not accept any liability for any losses incurred as a result of gambling on our website.</p>
        <h2>3. Use of the Website</h2>
        <p>The content of the pages of this website is for your general information and use only. It is subject to change without notice. Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness, or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors, and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
        <h2>4. Age Restriction</h2>
        <p>You must be at least 18 years of age to use this website. We reserve the right to request proof of age and identity from you and to suspend or terminate your account if we suspect that you are underage or have provided false information about your age.</p>
        <h2>5. Limitation of Liability</h2>
        <p>We do not accept any liability for any losses, damages, or expenses incurred as a result of using this website or any linked websites. We do not accept any liability for any interruptions or errors in the operation of the website. We do not accept any liability for any viruses or other harmful components that may infect your computer or other devices as a result of using this website.</p>
        <h2>6. Intellectual Property</h2>
        <p>All intellectual property rights in the website and its content, including but not limited to graphics, images, logos, and text, are owned by us or our licensors. You may not copy, reproduce, distribute, or display any part of the website without our prior written consent.</p>
    </div>        
@endsection

@section('sidebar')
    
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            @if($all_categories)
                <li class="divider" data-content="CATEGORY"></li>
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                </li>
                @foreach ($all_categories as $key => $category_link)
                    <li class="menu-item">{!! $category_link !!}</li>
                @endforeach
            @endif
        </ul>
    </div>

@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <aside class="menu">
            @if($all_categories)
                <ul class="category_btn" tabindex="0">
                    <li class="divider" data-content="CATEGORY">
                        <span id="category_icon">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-down"></use>
                            </svg> 
                        </span>
                    </li>
                </ul>
                <ul class="menu__inner">
                    <li class="menu-item">
                        <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                    </li>

                    @foreach ($all_categories as $key => $category_link)
                        <li class="menu-item">{!! $category_link !!}</li>
                    @endforeach
                </ul>
            @endif
        </aside>
    </div>

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@if (Route::currentRouteName() === 'home' && (request('page') === null || request('page') === '1'))
    @push('footer_scripts')
        <script type="application/ld+json">
            {"@context":"https://schema.org","@type":"Organization","name":"GamblingCentral","url":"{{ URL::to('/') }}","logo":"{{ URL::asset('assets') }}/logo.svg","sameAs": ["https://gcentralin.tumblr.com/"],"potentialAction": [{"@type": "SearchAction","target": {"@type": "EntryPoint","urlTemplate": "{{ URL::to('/search') }}?s={search_term_string}"},"query-input": "required name=search_term_string"}]}
        </script>   
    @endpush
@endif

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush