<article class="post posts_cnt__item">
    @php
        $comments_count = $post->get_approved_comments_count($post->id);
        $categories = $post->get_categories($post->id);
        
        if ($categories) {
            $category_array = $post->get_categories($post->id, true);
            $found_key = array_search('video', $category_array);
            $found_key_two = array_search('videos', $category_array);
        } 
    @endphp

    <figure class="thumbnail--post">
        @if(isset($found_key) && $found_key !== false || isset($found_key_two) && $found_key_two !== false)
            <div class="thumbnail--post__video_icon">
                <svg class="icon">
                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#video"></use>
                </svg> 
            </div> 
        @endif

        <a href="{{ route('posts.show', $post->slug) }}" class="thumbnail--post__link" aria-label="{{ $post->title }}">
            <img data-sizes="auto" 
                data-lowsrc="{{ $post->thumbnail['l']['sm']['url'] }}" 
                data-srcset="{{ $post->thumbnail['l']['sm']['url'] }} 278w, {{ $post->thumbnail['l']['md']['url'] }} 640w, {{ $post->thumbnail['l']['lg']['url'] }} 836w" 
                alt="{{ $post->title }}" 
                class="mediabox-img lazyload">
        </a>

        @if($post->sticky && Route::current()->named('home'))
            <div class="thumbnail--post__sticky">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#fire"></use>
                </svg> 
            </div>
        @endif

        <div class="info--post">
            @if($categories)
                <small class="mr-1"> 
                    <span>{{ __('Categories:') }}</span>
                    {!! $categories !!}
                </small>
            @endif

            <small class="mr-1">{{ $post->created_at->format('j M Y') }}</small>
            
            <small class="info--post__views views tooltip tooltip-left" data-tooltip="{{__('Total Views')}}">
                <span class="views__icon"> 
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#views"></use>
                    </svg> 
                </span> 
                {{ $post->views }}
            </small>

        </div>

        @if((!isset($found_key) || $found_key === false) && (!isset($found_key_two) || $found_key_two === false))
            @if($post->word_count > 190)
                @php
                    $float_minutes = $post->word_count / 190;
                    $time = sprintf('%d', (int) round($float_minutes, 0, PHP_ROUND_HALF_DOWN));
                @endphp 

                <div class="thumbnail--post__estimate_reading">
                    <small>{{ __("Reading time: {$time} min,") }} {{ $post->word_count }} {{ __('words')}}</small>
                </div>
            @elseif($post->word_count < 190 && $post->word_count != 0)
                @if (isset($found_key) && isset($found_key_two))
                    @if($found_key === false && $found_key_two === false)
                        <div class="thumbnail--post__estimate_reading">
                            <small>{{ __('Reading time: less than 1 min,') }} {{ $post->word_count }} {{ __('words')}} </small>
                        </div> 
                    @endif
                @endif
            @endif
        @endif
        
    </figure>
    <h2 class="post__heading">
        <a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
    </h2>
    @if ($post->excerpt !== NULL)
        <div class="post__excerpt">
            <p>
                {{ $post->excerpt }}
            </p>
        </div>    
    @endif
</article>