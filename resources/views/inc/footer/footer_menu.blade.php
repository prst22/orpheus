<ul class="menu_cnt">
    <li class="menu_cnt__item">
        <a href="{{ route('jackpots-tracker') }}" class="{{ request()->routeIs('jackpots-tracker') ? 'active' : '' }}">
            {{ __('Slot tracker') }}
        </a>
    </li>
    <li class="menu_cnt__item">
        <a href="{{ route('lottery-tracker') }}" class="{{ request()->routeIs('lottery-tracker') ? 'active' : '' }}">
            {{ __('Lottery tracker') }}
        </a>
    </li>
    <li class="menu_cnt__item">
        <a href="{{ route('all-casinos') }}" class="{{ request()->routeIs('all-casinos') ? 'active' : '' }}">
            {{ __('Casinos') }}
        </a>
    </li>
    <li class="menu_cnt__item">
        <a href="{{ route('about') }}" class="{{ request()->routeIs('about') ? 'active' : '' }}">
            {{ __('About us') }}
        </a>
    </li>
    <li class="menu_cnt__item">
        <a href="{{ route('contact') }}" class="{{ request()->routeIs('contact') ? 'active' : '' }}">
            {{ __('Contact') }}
        </a>
    </li>
    <li class="menu_cnt__item">
        <a href="{{ route('terms') }}" class="{{ request()->routeIs('terms') ? 'active' : '' }}">
            {{ __('Terms') }}
        </a>
    </li>
</ul>