<footer class="container footer footer--roulette mt">
    <div class="roulette">
        <a href="#" id="spin" aria-label="spin roulette"></a>
        <img src="{{ asset('assets') }}/roulette.svg" class="roulette__img" id="wheel" aria-hidden="true" alt="roulette wheel">
    </div>
    <div class="columns footer__content footer__content--roulette">
        <div class="column col-12 first_footer">
            <div class="first_footer__inner">
                <div class="footer_menu">

                    <div class="hide-md">

                        @include('inc.footer.footer_menu')
                        
                    </div>

                    <div class="social">
                        <a href="https://www.facebook.com/groups/gamblingcentral" class="social__link" title="facebook" target="_blank" rel="noopener">
                            <svg class="icon">
                                <use xlink:href="{{ asset('assets') }}/symbol-defs.svg#facebook"></use>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="bga_cnt hide-xs">
                    <a href="https://www.begambleaware.org/" class="bga_cnt__link" title="begambleaware" target="_blank" rel="noopener">
                        <img src="{{ asset('assets/bga.svg') }}" alt="GambleAware">
                    </a>
                </div>

                <div class="menu_small show-md hide-lg">

                    @include('inc.footer.footer_menu')

                </div>
                
            </div>
        </div>
        <div class="column col-md-12 col-8 footer_copy">
            <small>© {{ date('Y') }} ALL RIGHTS RESERVED </small>
        </div>
        
        <div class="column col-md-12 col-4 footer_right">
            {{-- <div class="search">
                <form method="GET" action="{{ route('search') }}">
                    <div class="input-group">
                        <input class="form-input input" type="text" name="query" placeholder="Search" required>
                        <button class="btn btn btn-primary input-group-btn" aria-label="search button">
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#search"></use>
                            </svg> 
                        </button>
                    </div>
                </form>
            </div> --}}
            <button class="btn btn-primary btn-action go_top" aria-label="Go top">
                <svg class="icon">
                    <use xlink:href="{{asset('assets')}}/symbol-defs.svg#arrow-up"></use>
                </svg> 
            </button>
        </div>   
    </div>
</footer>
       