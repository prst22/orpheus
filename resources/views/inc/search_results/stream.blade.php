<h2 class="h3">
    <a href="{{ route('streams.show', $result->slug) }}">
        <svg class="icon">
            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#monitor"></use>
        </svg> 
        {{ $result->channel_name }}
    </a>
</h2>
<div class="info--results mb-1">
    @if($categories)
        <small> 
            <span>{{ __('Categories:') }}</span>
            {!! $categories !!}
        </small>
    @endif
</div>
@if($result->description)
    <div class="post__excerpt post__excerpt--search_result">
        <p>
            {{ $result->description }}
        </p>
    </div>
@else
    <div class="post__excerpt post__excerpt--search_result">
        <p>
            {{ __('Online Gambling Stream') }}
        </p>
    </div>
@endif