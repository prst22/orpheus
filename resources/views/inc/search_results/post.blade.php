@php
    if($categories){
        $category_array = $result->get_categories($result->id, true);
        $found_key = array_search('video', $category_array);
        $found_key_two = array_search('videos', $category_array);
    }   
@endphp
<h2 class="h3">
    <a href="{{ route('posts.show', $result->slug) }}">
        @if(isset($found_key) && $found_key !== false || isset($found_key_two) && $found_key_two !== false)
            <svg class="icon">
                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#video"></use>
            </svg> 
        @else
            <svg class="icon">
                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#text"></use>
            </svg> 
        @endif
        {{ $result->title }}
    </a>
</h2>
<div class="info--results mb-1">
    @if($categories)
        <small> 
            <span>{{ __('Categories:') }}</span>
            {!! $categories !!}
        </small>
    @endif
    @if(isset($result->views))    
        <small>Posted on: {{ $result->created_at->format('j M Y') }}</small>
        
        <small class="info--results__views views tooltip tooltip-left" data-tooltip="{{__('Total Views')}}">
            <span class="views__icon"> 
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#views"></use>
                </svg> 
            </span> 
            {{ $result->views }}
        </small>
    @endif
</div>
@if(isset($result->excerpt))
    <div class="post__excerpt post__excerpt--search_result">
        <p>
            {{ $result->excerpt }}
        </p>
    </div>
@endif