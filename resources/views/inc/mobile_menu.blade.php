<section class="mobile_menu_wrapper">
    <nav class="mobile_menu_cnt hidden">
        <div class="mobile_menu_cnt__inner hidden">
            <div class="menu_content_scroll">
                <div class="menu_content">
                    <div class="close_menu_cnt">
                        <button class="btn btn-primary btn-action" id="close_menu_btn" aria-label="close menu button">
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#close"></use>
                            </svg> 
                        </button>
                    </div>
                    <ul class="mobile_menu_list_cnt">
                        <li class="menu_item">
                            <a href="{{ route('home') }}" class="btn btn-lg btn-link {{ request()->routeIs('home') ||
                                request()->routeIs('posts.category') ||
                                request()->routeIs('posts.sortby') ||
                                request()->routeIs('posts.category.sortby') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#text"></use>
                                </svg> 
                                {{ __('Articles') }}
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="{{ route('all-streams') }}" class="btn btn-lg btn-link {{ request()->routeIs('all-streams') || request()->routeIs('streams.category') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#monitor"></use>
                                </svg> 
                                {{ __('Live Streams') }}
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="{{ route('jackpots-tracker') }}" class="btn btn-lg btn-link {{ request()->routeIs('jackpots-tracker') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#slot"></use>
                                </svg> 
                                {{ __('Slot tracker') }}
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="{{ route('lottery-tracker') }}" class="btn btn-lg btn-link {{ request()->routeIs('lottery-tracker') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#lottery"></use>
                                </svg> 
                                {{ __('Lottery tracker') }}
                            </a>
                        </li>

                        <li class="menu_item">
                            <a href="{{ route('all-casinos') }}" class="btn btn-lg btn-link {{ request()->routeIs('all-casinos') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#casino"></use>
                                </svg> 
                                {{ __('Casinos') }}
                            </a>
                        </li>
                        @guest
                            @if(Auth::guard('admin')->check() === false)
                                <li class="menu_item">
                                    <a href="{{ route('login') }}" class="btn btn-lg btn-link {{ request()->routeIs('login') ? 'active' : '' }}">
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#login"></use>
                                        </svg> 
                                        {{ __('Login') }}
                                    </a>
                                </li>
                                <li class="menu_item">
                                    <a href="{{ route('register') }}" class="btn btn-lg btn-link {{ request()->routeIs('register') ? 'active' : '' }}">
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#register"></use>
                                        </svg>
                                        {{ __('Register') }}
                                    </a>
                                </li>
                            @endif
                        @endguest
                        @if(Auth::guard('web')->check())
                            <li class="menu_item">
                                <div class="accordion">
                                    <input type="checkbox" id="accordion-1" name="accordion-checkbox" hidden>
                                    <label class="accordion-header mobile_favourites_trigger" for="accordion-1">
                                        {{ __('Favourites') }} 
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#dropdown"></use>
                                        </svg> 
                                    </label>
                                    <div class="accordion-body">
                                        <ul class="mobile_sub_menu">
                                            <li class="mobile_sub_menu__item">
                                                <a href="{{ route('favourite.streams.index') }}" class="btn btn-lg btn-link {{ request()->routeIs('favourite.streams.index') ? 'active' : '' }}">
                                                    <svg class="icon">
                                                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#monitor"></use>
                                                    </svg> 
                                                    {{ __('Favourite Streams') }}
                                                </a>
                                            </li>
                                            <li class="mobile_sub_menu__item">
                                                <a href="{{ route('favourite.posts.index') }}" class="btn btn-lg btn-link {{ request()->routeIs('favourite.posts.index') ? 'active' : '' }}">
                                                    <svg class="icon">
                                                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#text"></use>
                                                    </svg> 
                                                    {{ __('Favourite Articles') }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="menu_item">
                                <a href="{{ route('profile') }}" class="btn btn-lg btn-link {{ request()->routeIs('profile') ? 'active' : '' }}">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#profile"></use>
                                    </svg> 
                                    {{ ucfirst(Auth::user()->name) }}
                                </a>
                            </li>
                            <li class="menu_item">
                                <a class="btn btn-lg btn-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout_form_mobile_menu').submit();">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#logout"></use>
                                    </svg> 
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout_form_mobile_menu" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endif

                        @auth('admin')
                            <li class="menu_item">
                                <a class="btn btn-lg btn-link" href="{{ route('admin.logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form-mobile').submit();">
                                        <svg class="icon">
                                            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#logout"></use>
                                        </svg> 
                                        {{ __('Admin Logout') }}
                                </a>
                                <form id="logout-form-mobile" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                            <li class="menu_item">
                                <a href="{{ route('dashboard') }}" class="btn btn-lg btn-success">
                                    <svg class="icon">
                                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#settings"></use>
                                    </svg> 
                                    Dashboard 
                                </a>
                            </li>
                        @endauth

                        {{-- <li class="menu_item">
                            <form method="GET" action="{{ route('search') }}">
                                <div class="input-group mt">
                                    <input class="form-input input" type="text" name="query" placeholder="Search" required>
                                    <button class="btn btn btn-primary input-group-btn" aria-label="search button">
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#search"></use>
                                        </svg> 
                                    </button>
                                </div>
                            </form>
                        </li>        --}}
                    </ul> 
                </div> 
            </div>            
        </div>
    </nav>
</section> 