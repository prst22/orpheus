@php
    $link = new \App\Share\ShareButtons($cur_url, $title);
@endphp

<div class="share_block">

    <span class="share_block__lable mr-1">Share: </span> 

    {!! $link->facebook() !!}
    {!! $link->twitter() !!}
    {!! $link->whatsapp() !!}
    {!! $link->telegram() !!}
    {!! $link->tumblr() !!}
    
</div>