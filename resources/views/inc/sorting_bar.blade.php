@if(count($posts) > 0)
    <div class="sorting_controlls">
        @if(Route::currentRouteName() !== 'posts.category' && Route::currentRouteName() !== 'posts.category.sortby')
        
            <div class="btn-group btn-group-block">
                <a href="{{ route('home') }}" class="btn btn-lg btn-primary {{ (!isset($type) && !isset($ascendance)) ? 'active' : '' }}">{{ __('NEW') }}</a>
                <a href="{{ route('posts.sortby', [ 'type' => 'date', 'ascendance' => 'old']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'date' && $ascendance === 'old') ? 'active' : '' }}">{{ __('OLD') }}</a>
                <a href="{{ route('posts.sortby', ['type' => 'popularity', 'ascendance' => 'mostviewed']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'popularity' && $ascendance === 'mostviewed') ? 'active' : '' }}">{{ __('POPULAR') }}</a>
                <a href="{{ route('posts.sortby', ['type' => 'popularity', 'ascendance' => 'leastviewed']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'popularity' && $ascendance === 'leastviewed') ? 'active' : '' }}">{{ __('LEAST POPULAR') }}</a>
            </div>
            
        @else
        
            <div class="btn-group btn-group-block"> 
                <a href="{{ route('posts.category', $slug) }}" class="btn btn-lg btn-primary {{ (!isset($type) && !isset($ascendance)) ? 'active' : '' }}">{{ __('NEW') }}</a>
                <a href="{{ route('posts.category.sortby', ['slug' => $slug, 'type' => 'date', 'ascendance' => 'old']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'date' && $ascendance === 'old') ? 'active' : '' }}">{{ __('OLD') }}</a>
                <a href="{{ route('posts.category.sortby', ['slug' => $slug, 'type' => 'popularity', 'ascendance' => 'mostviewed']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'popularity' && $ascendance === 'mostviewed') ? 'active' : '' }}">{{ __('POPULAR') }}</a>
                <a href="{{ route('posts.category.sortby', ['slug' => $slug, 'type' => 'popularity', 'ascendance' => 'leastviewed']) }}" class="btn btn-lg btn-primary {{ (isset($type) && $type === 'popularity' && $ascendance === 'leastviewed') ? 'active' : '' }}">{{ __('LEAST POPULAR') }}</a>                
            </div>

        @endif
    </div>
@endif