<div class="column col-md-12 col-8 col-mx-auto mt singel_post_casino_block">
    <div class="columns">
        @foreach ($casinos as $casino)
        
            @if ($loop->index <= 1)
                <div class="column col-6 col-sm-12 singel_post_casino_block_casino">
                    
                    <div class="singel_post_casino_block_casino_img_cnt">
                        
                        @if(isset($casino->image['branding']['logo']))
                            <a href="{{ $casino->url }}" title="{{ $casino->name }}" class="singel_post_casino_block_casino_img_cnt__link">
                                <img src="{{$casino->image['branding']['logo']['sm']}}" alt="{{ $casino->name }}" class="singel_post_casino_block_casino_img_cnt__image">
                            </a>
                        @endif
                        {{-- <h2 class="h4 singel_post_casino_block_casino__image">
                            <a href="{{ $casino->url }}">
                                {{ $casino->name }}
                            </a>
                        </h2> --}}
                    </div>
                    
                    <div class="singel_post_casino_block_casino__rating">
                        <span class="d-block text-center"><small>Rating:</small></span>
                        <span class="d-block text-center"><b>{{ $casino->rating }}</b></span>
                    </div>

                </div>
                @php 
                    $casinos->forget($loop->index);
                @endphp
            @else
                @break
            @endif    
        @endforeach
    </div>
</div>