
@if(Route::currentRouteName() !== 'posts.category' && Route::currentRouteName() !== 'posts.category.sortby')

    @if(isset($ascendance))

        @if($ascendance === 'old')
            @section('meta_keywords','old articles, articles sort by date, videos sort by date, gambling articles, gambling videos')
        @endif

        @if($ascendance === 'mostviewed')
            @section('meta_keywords','most viewed articles, popular articles, gambling articles, gambling videos, popular, sort by popularity')
        @endif

        @if($ascendance === 'leastviewed')
            @section('meta_keywords','least viewed articles, least viewed videos, gambling articles, gambling videos, least popular, sort by popularity')
        @endif

    @endif

@else

    @if(isset($ascendance))

        @if($ascendance === 'old')
            @section('meta_keywords',"category {$category_name}, sort by date, old")
        @endif

        @if($ascendance === 'mostviewed')
            @section('meta_keywords', "category {$category_name}, sort by popularity, most viewed")
        @endif

        @if($ascendance === 'leastviewed')
            @section('meta_keywords', "category {$category_name}, sort by popularity, least viewed")
        @endif

    @endif

@endif
