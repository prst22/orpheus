@if($errors->any())
    @foreach ($errors->all() as $error)
        <div class="toast_outer">    
            <div class="toast toast-error my-10">
                <button class="btn btn-clear float-right"></button>
                {{ $error }}
            </div>
        </div>
    @endforeach
@endif

@if (session('success'))
    <div class="toast_outer">    
        <div class="toast toast-success my-10">
            <button class="btn btn-clear float-right"></button>
            {{ session('success') }}
        </div>
    </div>
@endif

@if (session('error'))
    <div class="toast_outer">    
        <div class="toast toast-error my-10">
            <button class="btn btn-clear float-right"></button>
            {{ session('error') }}
        </div>
    </div>
@endif