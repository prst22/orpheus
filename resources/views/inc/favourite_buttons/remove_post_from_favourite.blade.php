<form action="{{ route('favourite.posts.destroy', $post->id) }}" method="POST" class="fav_form"> 
    <button class="btn btn-lg ml-2">
        <svg class="icon">
            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#remove-fav"></use>
        </svg> 
        Remove from favourite
    </button>
    @csrf
</form>