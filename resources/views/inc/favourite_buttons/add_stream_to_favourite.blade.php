<form action="{{ route('favourite.streams.store', $stream->id) }}" method="POST" class="fav_form"> 
    <button class="btn btn-lg ml-2">
        <svg class="icon">
            <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#favourite"></use>
        </svg> 
        Add to favourite
    </button>
    @csrf
</form> 