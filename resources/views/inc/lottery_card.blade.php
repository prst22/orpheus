@isset($jackpot->jackpot_data['prize_pool'])          
    @if ($jackpot->name === 'Powerball')
        <div class="column col-lg-12 col-6">
            <div class="lottery_card">
                <div class="lottery_card__inner">
                    <div class="powerball">
                        
                        <div class="lottery_image_cnt">
                            <img src="{{ URL::asset('assets') }}/lottery/powerball.png" alt="powerball">
                        </div>

                        <h2 class="lottery_heading h4 mb_0">{{ $jackpot->name }} <small>{{ $jackpot->getFormatedDate(' ') }} result</small></h2>

                        <div class="numbers_cnt">
                            @isset($jackpot->jackpot_data['winning_numbers'])
                                @foreach ($jackpot->jackpot_data['winning_numbers'] as $number)
                                    <span class="number">{{ $number }}</span>
                                @endforeach
                            @endisset
                        </div>
                        <div class="draw_date"> 
                            <i>Draw date: </i>{{ $jackpot->getFormatedDate() }}
                        </div>
                    
                        <div class="amount_cnt">
                            <div class="amount_cnt__prize_pool">
                                <i>Prize pool: </i> {{ $jackpot->getFormatedAmount('prize_pool') }}
                            </div>
                            @isset($jackpot->jackpot_data['cash_value'])
                                <div class="amount_cnt__cash_value">
                                    <i>Cash value: </i> {{ $jackpot->getFormatedAmount('cash_value') }}
                                </div>
                            @endisset
                        </div>
                        
                        @isset($jackpot->jackpot_data['extra']['multiplier'])
                            <div class="multiplier"><i>Power Play: </i>
                                <span class="multiplier__value">
                                    {{ $jackpot->jackpot_data['extra']['multiplier'] }}X
                                </span>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    @endif 

    @if ($jackpot->name === 'Euromillions')
        <div class="column col-lg-12 col-6">
            <div class="lottery_card">
                <div class="lottery_card__inner">
                    <div class="euromillions">
                        <div class="lottery_image_cnt">
                            <img src="{{ URL::asset('assets') }}/lottery/euro-millions.png" alt="Euro millions">
                        </div>

                        <h2 class="lottery_heading h4 mb_0">{{ $jackpot->name }} <small>{{ $jackpot->getFormatedDate(' ') }} result</small></h2>

                        <div class="numbers_cnt">
                            @foreach ($jackpot->jackpot_data['winning_numbers'] as $number)
                                <span class="number">{{ $number }}</span>
                            @endforeach
                            @foreach ($jackpot->jackpot_data['extra']['lucky_stars'] as $number)
                                <span class="number star">{{ $number }}</span>
                            @endforeach
                            
                        </div>

                        <div class="draw_date"> 
                            <i>Draw date: </i>{{ $jackpot->getFormatedDate() }}
                        </div>

                        <div class="amount_cnt">
                            <div class="amount_cnt__prize_pool">
                                <i>Prize pool: </i> {{ $jackpot->getFormatedAmount('prize_pool') }}
                            </div>
                                
                            @isset($jackpot->jackpot_data['cash_value'])
                                <div class="amount_cnt__cash_value">
                                    <i>Cash value: </i> {{ $jackpot->getFormatedAmount('cash_value') }}
                                </div>
                                
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($jackpot->name === 'Megamillions')
        <div class="column col-lg-12 col-6">
            <div class="lottery_card">
                <div class="lottery_card__inner">
                    <div class="megamillions">
                        <div class="lottery_image_cnt">
                            <img src="{{ URL::asset('assets') }}/lottery/mega-millions.png" alt="mega millions">
                        </div>

                        <h2 class="lottery_heading h4 mb_0">{{ $jackpot->name }} <small>{{ $jackpot->getFormatedDate(' ') }} result</small></h2>

                        <div class="numbers_cnt">
                            @foreach ($jackpot->jackpot_data['winning_numbers'] as $number)
                                <span class="number">{{ $number }}</span>   
                            @endforeach
                            
                            @isset($jackpot->jackpot_data['extra']['m_ball'])
                                <span class="number m_ball">{{ $jackpot->jackpot_data['extra']['m_ball'] }}</span>
                            @endisset
                        </div>
                        <div class="draw_date"> 
                            <i>Draw date: </i>{{ $jackpot->getFormatedDate() }}
                        </div>
                        <div class="amount_cnt">
                        
                            <div class="amount_cnt__prize_pool">
                                <i>Prize pool: </i> {{ $jackpot->getFormatedAmount('prize_pool') }}
                            </div>
 
                        </div>
                        @isset($jackpot->jackpot_data['extra']['multiplier'])
                            <div class="multiplier"><i>Megaplier: </i>
                                <span class="multiplier__value">
                                    {{ $jackpot->jackpot_data['extra']['multiplier'] }}X
                                </span>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($jackpot->name === 'Lottomax')
        <div class="column col-lg-12 col-6">
            <div class="lottery_card">
                <div class="lottery_card__inner">
                    <div class="lottomax">
                        <div class="lottery_image_cnt">
                            <img src="{{ URL::asset('assets') }}/lottery/lotto-max.png" alt="Lottomax">
                        </div>

                        <h2 class="lottery_heading h4 mb_0">{{ $jackpot->name }} <small>{{ $jackpot->getFormatedDate(' ') }} result</small></h2>

                        <div class="numbers_cnt">
                            @foreach ($jackpot->jackpot_data['winning_numbers'] as $number)
                                @if ($loop->last)
                                    <span class="number m_ball">{{ $number }}</span>
                                @else
                                    <span class="number">{{ $number }}</span>
                                @endif
                            @endforeach
                        </div>
                        <div class="draw_date"> 
                            <i>Extra number: </i>{{ $jackpot->jackpot_data['extra']['extra_number'] }}
                        </div>
                        <div class="draw_date"> 
                            <i>Draw date: </i>{{ $jackpot->getFormatedDate() }}
                        </div>

                        <div class="amount_cnt">
                            <div class="amount_cnt__prize_pool">
                                <i>Prize pool: </i> {{ $jackpot->getFormatedAmount('prize_pool') }}
                            </div>
                                
                            @if(isset($jackpot->jackpot_data['extra']['max_millions_numbers']))
                                <div class="max_millions_numbers text-center">
                                    <button class="btn btn-primary max_millions_btn">
                                        <img src="{{ URL::asset('assets') }}/lottery/maxmillions.png" alt="maxmillions">
                                    </button>
                                </div>
                                <div class="modal max_millions_numbers_modal">
                                    <a href="#close" class="modal-overlay" aria-label="Close"></a>
                                    <div class="modal-container">
                                        <div class="modal-header">
                                            <a href="#close" class="btn btn-clear float-right" aria-label="Close"></a>
                                            <div class="modal-title h5">Max millions numbers</div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="content">
                                                <div class="numbers_cnt numbers_cnt--lottomax text-center">
                                                    @foreach ($jackpot->jackpot_data['extra']['max_millions_numbers'] as $number)
                                                       
                                                        @if(($loop->index + 1) % 7 == 0)
                                                            <span class="number">{{ $number }}</span> <br>
                                                        @else
                                                            <span class="number">{{ $number }}</span>
                                                        @endif
                                                       
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        
                                        </div>
                                    </div>
                                </div>
                            @else 
                                <p class="max_millions_numbers">No MAXMILLIONS draws this week</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif  

@endisset   