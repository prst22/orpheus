<nav class="container main_navigation">
    <div class="columns">
        <div class="column col-sm-4 col-md-3 col-xl-1 col-3">
            <a class="main_logo_cnt" href="{{ url('/') }}">
                <img src="{{ URL::asset('assets') }}/logo.svg" alt="{{ config('app.name') }}">

                @if ( request()->routeIs('home') || request()->routeIs('posts.sortby'))
                    <h1 class="home_title m-0">
                        {{ __('Articles, videos, streams, jackpots tracker') }}
                    </h1>
                @endif
            </a> 
        </div>
        <div class="column col-sm-8 col-md-9 col-xl-11 col-9">
            <ul class="hide-lg main_navigation__menu">  
                <li class="menu_item">
                    <a href="{{ route('home') }}" class="btn @if(!Auth::guard('web')->check() && !Auth::guard('admin')->check()) btn-lg @endif btn-link {{ request()->routeIs('home') ||
                        request()->routeIs('posts.category') ||
                        request()->routeIs('posts.sortby') ||
                        request()->routeIs('posts.category.sortby') ? 'active' : '' }}">
                        <svg class="icon">
                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#text"></use>
                        </svg> 
                        {{ __('Articles') }}
                    </a>
                </li>
                <li class="menu_item">
                    <a href="{{ route('all-streams') }}" class="btn @if(!Auth::guard('web')->check() && !Auth::guard('admin')->check()) btn-lg @endif btn-link {{ request()->routeIs('all-streams') || request()->routeIs('streams.category') ? 'active' : '' }}">
                        <svg class="icon">
                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#monitor"></use>
                        </svg> 
                        {{ __('Live Streams') }}
                    </a>
                </li>
                <li class="menu_item">
                    <div class="dropdown">
                        <a href="#" class="btn btn-link dropdown-toggle" tabindex="0">
                            {{ __('Jackpots') }} 
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#dropdown"></use>
                            </svg> 
                        </a>
                        <ul class="menu">
                            <li>
                                <a href="{{ route('jackpots-tracker') }}" class="btn @if(!Auth::guard('web')->check() && !Auth::guard('admin')->check()) btn-lg @endif btn-link {{ request()->routeIs('jackpots-tracker') ? 'active' : '' }}">
                                    <svg class="icon">
                                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#slot"></use>
                                    </svg> 
                                    {{ __('Slot tracker') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lottery-tracker') }}" class="btn @if(!Auth::guard('web')->check() && !Auth::guard('admin')->check()) btn-lg @endif btn-link {{ request()->routeIs('lottery-tracker') ? 'active' : '' }}">
                                    <svg class="icon">
                                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#lottery"></use>
                                    </svg> 
                                    {{ __('Lottery tracker') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu_item">
                    <a href="{{ route('all-casinos') }}" class="btn @if(!Auth::guard('web')->check() && !Auth::guard('admin')->check()) btn-lg @endif btn-link {{ request()->routeIs('all-casinos') ? 'active' : '' }}">
                        <svg class="icon">
                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#casino"></use>
                        </svg> 
                        {{ __('Casinos') }}
                    </a>
                </li>
                @if(Auth::guard('web')->check())
                    <li class="menu_item">
                        <div class="dropdown">
                            <a href="#" class="btn btn-link dropdown-toggle" tabindex="0">
                                {{ __('Favourites') }} <svg class="icon">
                                    <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#dropdown"></use>
                                </svg> 
                            </a>
                            <!-- menu component -->
                            <ul class="menu">
                                <li>
                                    <a href="{{ route('favourite.streams.index') }}" class="btn btn-link {{ request()->routeIs('favourite.streams.index') ? 'active' : '' }}">
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#monitor"></use>
                                        </svg> 
                                        {{ __('Favourite Streams') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('favourite.posts.index') }}" class="btn btn-link {{ request()->routeIs('favourite.posts.index') ? 'active' : '' }}">
                                        <svg class="icon">
                                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#text"></use>
                                        </svg> 
                                        {{ __('Favourite Articles') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="menu_item">
                        <a href="{{ route('profile') }}" class="btn btn-link {{ request()->routeIs('profile') ? 'active' : '' }}">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#profile"></use>
                            </svg> 
                            {{ str_limit(ucfirst(Auth::user()->name), 14, '...') }}
                        </a>
                    </li>
                    <li class="menu_item">
                        <a class="btn btn-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#logout"></use>
                            </svg> 
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endif
                @guest
                    @if(Auth::guard('admin')->check() === false)
                        <li class="menu_item">
                            <a href="{{ route('login') }}" class="btn btn-lg btn-link {{ request()->routeIs('login') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#login"></use>
                                </svg> 
                                {{ __('Login') }}
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="{{ route('register') }}" class="btn btn-lg btn-link {{ request()->routeIs('register') ? 'active' : '' }}">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#register"></use>
                                </svg>
                                {{ __('Register') }}
                            </a>
                        </li> 
                    @endif   
                @endguest     

                @auth('admin')
                    <li class="menu_item">
                        <a class="btn btn-link" href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#logout"></use>
                                </svg> 
                                {{ __('Admin Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <li class="menu_item">
                        <a href="{{ route('dashboard') }}" class="btn btn-success">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#settings"></use>
                            </svg> 
                            Dashboard 
                        </a>
                    </li>
                @endauth
            </ul>  
            <div class="mobile_menu_toggle show-lg hide-xl">
                <div>
                    <button class="btn btn-lg" id="mobile_menu_btn" aria-label="site menu">
                        <span class="menu_icon_text">MENU</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</nav>
