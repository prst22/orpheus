<a class="btn btn-link mb-2" href="{{ route('google.login') }}">
    <svg class="icon">
        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#google"></use>
    </svg> {{ __('Login with google') }}
</a>
<a class="btn btn-link mb-2" href="{{ route('facebook.login') }}">
    <svg class="icon">
        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#facebook"></use>
    </svg> {{ __('Login with facebook') }}
</a>