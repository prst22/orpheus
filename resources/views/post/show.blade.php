@extends('layouts.main')

@section('title', Str::limit($post->title, 57))

@section('meta_description', Str::limit($post->excerpt, 120))

@section('meta_image', $post->thumbnail['s']['lg']['url'])

@push('header_styles')
    <style>
        .mediabox-img.ls-blur-up-is-loading,
        .mediabox-img.lazyload:not([src]) {
            visibility: hidden;
        }
        .ls-blur-up-img,
        .mediabox-img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            object-fit: cover;
        }
        .ls-blur-up-img {
            filter: blur(10px);
            opacity: 1;
            transition: opacity 1000ms, filter 1500ms;
        }
        .ls-blur-up-img.ls-inview.ls-original-loaded {
            opacity: 0;
            filter: blur(5px);
        }
    </style>
@endpush

@push('header_styles_after_main')
    <link rel="stylesheet" href="{{ URL::asset('css/glightbox.min.css') }}" />
@endpush

@section('content') 

    @php
        $comments_count = $post->get_approved_comments_count($post->id);
        $categories = $post->get_categories($post->id);

        if($categories) {
            $category_array = $post->get_categories($post->id, true);
            $found_key = array_search('video', $category_array);
            $found_key_two = array_search('videos', $category_array);
        } 
    @endphp

    <div class="column col-12 col-mx-auto">
        @include('inc.errors')
        <div class="single_thumbnail_outer">
            
            @if(isset($found_key) && $found_key !== false || isset($found_key_two) && $found_key_two !== false)
                <div class="single_thumbnail_outer__video_icon">
                    <svg class="icon">
                        <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#video"></use>
                    </svg> 
                </div> 
                <figure class="thumbnail thumbnail--post_single">
                    <img data-sizes="auto" 
                        data-lowsrc="{{ $post->thumbnail['s']['sm']['url'] }}" 
                        data-srcset="{{ $post->thumbnail['s']['sm']['url'] }} 278w, {{ $post->thumbnail['s']['md']['url'] }} 680w, {{ $post->thumbnail['s']['lg']['url'] }} 836w" 
                        alt="{{ $post->title }}" 
                        class="mediabox-img lazyload">
                </figure>
            @else
                <figure class="thumbnail thumbnail--post_single">
                    <img data-sizes="auto" 
                        data-lowsrc="{{ $post->thumbnail['s']['sm']['url'] }}" 
                        data-srcset="{{ $post->thumbnail['s']['sm']['url'] }} 278w, {{ $post->thumbnail['s']['md']['url'] }} 680w, {{ $post->thumbnail['s']['lg']['url'] }} 836w" 
                        alt="{{ $post->title }}" 
                        class="mediabox-img lazyload">
                </figure>
                @if($post->word_count > 190)
                    @php
                        $float_minutes = $post->word_count / 190;
                        $time = sprintf('%d', (int) round($float_minutes, 0, PHP_ROUND_HALF_DOWN));
                    @endphp
                    <div class="single_thumbnail_outer__estimate_reading">
                        <small>{{ __("Reading time: {$time} min,") }} {{ $post->word_count }} {{ __('words')}}</small>
                    </div>
                @elseif($post->word_count < 190 && $post->word_count != 0)
                    @if (isset($found_key) && isset($found_key_two))
                        @if($found_key === false && $found_key_two === false)
                            <div class="single_thumbnail_outer__estimate_reading">
                                <small>{{ __('Reading time: less than 1 min,') }} {{ $post->word_count }} {{ __('words')}} </small>
                            </div> 
                        @endif
                    @endif
                @endif
            @endif

        </div>
    </div>

    @if (count($casinos) > 0)

        @include('inc.casino_ad')

    @endif
   
    <article class="column col-md-12 col-8 col-mx-auto single_post">
        <h1 class="post_title">
            {{ $post->title }}
        </h1>

        <div class="info mb-2">
            @if($categories)
                <small class="mr-1"> 
                    <span>{{ __('Categories:') }}</span>
                    {!! $categories !!}
                </small>
            @endif
            <small class="mr-1">{{ $post->created_at->format('j M Y') }}</small>

            <small class="info__views views tooltip tooltip-left mr-1" data-tooltip="{{__('Total Views')}}">
                <span class="views__icon">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#views"></use>
                    </svg> 
                </span> 
                {{ $post->views }}
            </small>
            <small class="info--post__comments comments tooltip tooltip-left" data-tooltip="{{__('Total Comments')}}">
                <span class="comments__icon"> 
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#comments"></use>
                    </svg> 
                </span> 
                {{ $comments_count }}
            </small>
        </div>

        <div class="post_content">
            {!! $post->body !!}
        </div>

    </article>

    <div class="column col-md-12 col-8 col-mx-auto">
        <div class="bottom_controlls">

            @include('inc.social_share_buttons', ['cur_url' => url()->current(), 'title' => $post->title ?? ''])

            @auth('web')
                @if($is_fav)
                    @include('inc.favourite_buttons.remove_post_from_favourite')
                @else
                    @include('inc.favourite_buttons.add_post_to_favourite')
                @endif
            @endauth

            @auth('admin')
                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-lg btn-action btn-success ml-2 single_edit_btn">
                    <svg class="icon">
                        <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#edit"></use>
                    </svg> 
                </a>
            @endauth   

            <a href="{{ url()->previous() }}" class="btn btn-lg ml-2 single_back_btn {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>
            
        </div>
    </div>

    @if (count($casinos) > 0)

        @include('inc.casino_ad')

    @endif
    
    <div class="column col-md-12 col-8 col-mx-auto comments_container mt" id="comments">
        @comments([
            'model' => $post,
            'perPage' => 20,
            'approved' => true
        ])
    </div>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="application/ld+json">
        {"@context": "http://schema.org","@type": "NewsArticle","headline": "{{ $post->title }}","description": "{{ !empty($post->excerpt) ? Str::limit($post->excerpt, 120) : $post->title }}","datePublished": "{{ \Carbon\Carbon::parse($post->created_at)->format('Y-m-d\TH:i:sP') }}","dateModified": "{{ \Carbon\Carbon::parse($post->updated_at)->format('Y-m-d\TH:i:sP') }}","inLanguage":"en-US","mainEntityOfPage": {"@type": "WebPage","@id": "{{ URL::current() }}"},"publisher": {"@type": "Organization","@id": "{{ URL::to('/') }}","name": "GamblingCentral","logo": {"@type": "ImageObject","url": "{{ URL::asset('assets') }}/small-logo.svg","height": 120,"width": 120}},"image": ["{{ $post->thumbnail['s']['lg']['url'] }}"]}
    </script>  
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush