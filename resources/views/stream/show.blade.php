@extends('layouts.main')

@section('title', "{$stream->channel_name} - Live stream")

@section('meta_description', isset($stream->description) ? Str::limit($stream->description, 120) : 'Casino and poker live stream. Live slots and roulette games')

@section('meta_keywords', "stream, gambling stream, live stream")

@push('header_styles')
    <style>
        .lazyload,
        .lazyloading {
            opacity: 0;
        }
        .lazyloaded {
            opacity: 1;
            transition: opacity 600ms;
        }
    </style>
@endpush

@section('content') 
    <article class="column col-md-12 col-12 col-mx-auto">

        @include('inc.errors')

        <h1>
            {{ $stream->channel_name }}
        </h1>
       
        @php
            $categories = $stream->get_categories($stream->id);
        @endphp
        
        @if($categories)
            <div class="categories mb-1">
                <small> 
                    <span>{{ __('Categories:') }}</span>
                    {!! $categories !!}
                </small>
            </div>   
        @endif
 
        <div class="article_content">
            <div class="twitch_player_cnt">
                <div id="twitch_player">
                    <div class="loading loading-lg"></div>
                    <iframe
                        class="lazyload"
                        data-src="https://player.twitch.tv/?channel={{ strtolower($stream->channel_name) }}&parent=gamblingcentral.info&muted=false&autoplay=true"
                        height="100%"
                        width="100%"
                        importance="high"
                        frameborder="0"
                        scrolling="no"
                        allow="autoplay"
                        allowfullscreen="true">
                    </iframe>
                </div>
            </div>
        </div>

        @if(isset($description) && !empty($description))
            <div class="stream_description">
                <div class="accordion mt_1">
                    <input type="checkbox" id="accordion-1" name="accordion-checkbox" hidden>
                    <label class="accordion-header" for="accordion-1">
                        About:
                        <svg class="icon mr-1">
                            <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#dropdown"></use>
                        </svg> 
                    </label>
                    <div class="accordion-body">
                        <p class="mt_1">{{ $description }}</p>
                    </div>
                </div>
            </div>   
        @endif

        <div class="bottom_controlls">

            @include('inc.social_share_buttons', ['cur_url' => url()->current(), 'title' => $stream->channel_name ? "{$stream->channel_name} - live stream" : "live stream"])

            @auth('web')
                @if($is_fav)
                    @include('inc.favourite_buttons.remove_stream_from_favourite')
                @else
                    @include('inc.favourite_buttons.add_stream_to_favourite')
                @endif
            @endauth

            <a href="{{ url()->previous() }}" class="btn btn-lg ml-2 single_back_btn {{ url()->previous() == url()->current() ? 'disabled' : '' }}">
                <svg class="icon">
                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#back"></use>
                </svg> 
                Back
            </a>

        </div>
    </article>
@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush