@extends('layouts.main')

@section('title', isset($category_name) ? ucwords($category_name) : '')

@section('meta_description', isset($category_name) ? ucwords($category_name) : 'Casino and poker games. Live poker streams. Live casino game streams. Live slots and roulette games.')

@section('meta_keywords', "{$category_name}, streams, gambling streams, live streams, stream")

@push('header_styles')
    <style>
        .mediabox-img.ls-blur-up-is-loading,
        .mediabox-img.lazyload:not([src]) {
            visibility: hidden;
        }
        .ls-blur-up-img,
        .mediabox-img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            object-fit: cover;
        }
        .ls-blur-up-img {
            filter: blur(10px);
            opacity: 1;
            transition: opacity 1000ms, filter 1500ms;
        }
        .ls-blur-up-img.ls-inview.ls-original-loaded {
            opacity: 0;
            filter: blur(5px);
        }
    </style>
@endpush

@section('content') 
    <header class="column col-md-12 col-12 col-mx-auto">
        @if (isset($category_name))
            <h1 class="stream_category_heading badge" data-badge="{{ $total_streams }}">Live: {{ ucwords($category_name) }}</h1>
        @endif
    </header>

    @if (count($streams) > 0)
        <div class="column col-md-12 col-8 stream_sorting_cnt">
            <div class="btn-group btn-group-block">
                <button class="btn btn-lg btn-primary" data-sort-value="viewers">Viewers</button>
                <button class="btn btn-lg btn-primary active" data-sort-value="name">Name</button>
            </div>
        </div>
    @endif

    <div class="streams streams--category">
        @if (count($streams) > 0)
            <div class="grid_sizer"></div>
            <div class="gutter_sizer"></div>
            
            @foreach ($streams as $stream)

                <div class="stream_container">
                    <figure class="thumbnail--stream channel">
                        <a href="{{ route('streams.show', [$stream->slug]) }}" class="thumbnail--stream__link">
                            <img data-sizes="auto"
                                data-lowsrc="{{ url('/assets/no-image.png') }}"
                                data-src="{{ $live_streams[$stream->channel_id]['img_src'] ?? url('/assets/no-image.png') }}" 
                                alt="{{ $stream->channel_name }}" 
                                class="mediabox-img lazyload thumbnail--stream_img">
                            
                            <h2 class="h5 channel__title">{{ $live_streams[$stream->channel_id]['title'] ?? '' }}</h2>
                        </a> 
                        
                        <span class="info_name name">
                            {{ $stream->channel_name }}
                        </span>
                        
                        <div class="info_views tooltip tooltip-left" data-tooltip="{{__('Now watching')}}">
                            <span class="stream_views">
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#views"></use>
                                </svg> 
                            </span>
                            <div>
                                <span class="number">
                                    {{ $live_streams[$stream->channel_id]['viewers'] ?? '' }}
                                </span>
                                <span>
                                    {{ $live_streams[$stream->channel_id]['game'] ?? '' }}
                                </span>
                            </div>  
                        </div>
                    
                        <div class="live">
                            <span class="live__text">
                                Live
                            </span>
                            <span class="live__lang">
                                {{ $live_streams[$stream->channel_id]['lan'] ?? '' }}
                            </span>    
                        </div>
                    </figure>
                </div>
               
            @endforeach
        </div>
    @else
        <div class="no_live_streams">
            <h3 class="mt">{{ __('There are no live streams in this category right now') }}</h3>
        </div>
    @endif

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush