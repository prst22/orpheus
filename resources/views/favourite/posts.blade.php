@extends('layouts.main')

@section('title', 'Favourites')

@section('meta_description', 'Favourite articles and videos about online gambling')

@push('header_styles')
    <style>
        .mediabox-img.ls-blur-up-is-loading,
        .mediabox-img.lazyload:not([src]) {
            visibility: hidden;
        }
        .ls-blur-up-img,
        .mediabox-img{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: block;
            object-fit: cover;
        }
        .ls-blur-up-img{
            filter: blur(10px);
            opacity: 1;
            transition: opacity 700ms, filter 1500ms;
        }
        .ls-blur-up-img.ls-inview.ls-original-loaded{
            opacity: 0;
            filter: blur(5px);
        }
    </style>
@endpush

@section('content') 
    <header class="column col-12">
        <h1 class="favourite_heading--articles">Favourite Articles</h1>
    </header>
           
    @forelse($posts as $post)
        <div class="column col-md-12 col-6 posts_cnt">
            <article class="post posts_cnt__item favourite_item">
                @php
                    $categories = $post->get_categories($post->id);
                    $category_array = $post->get_categories($post->id, true);
                    $found_key = array_search('video', $category_array);
                    $found_key_two = array_search('videos', $category_array);
                    $excerpt = Str::words($post->excerpt, 20, '...');
                @endphp

                <figure class="thumbnail--post">
                    @if($found_key || $found_key_two)
                        <div class="thumbnail--post__video_icon">
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#video"></use>
                            </svg> 
                        </div> 
                    @endif
                    <a href="{{ route('posts.show', $post->slug) }}" class="thumbnail--post__link">
                        <img data-sizes="auto" 
                            data-lowsrc="{{ $post->thumbnail['l']['sm']['url'] }}"
                            data-srcset="{{ $post->thumbnail['l']['sm']['url'] }} 278w, {{ $post->thumbnail['l']['md']['url'] }} 640w, {{ $post->thumbnail['l']['lg']['url'] }} 836w" 
                            alt="{{ $post->title }}" 
                            class="mediabox-img lazyload">
                    </a>
                    <div class="info--post">
                        @if($categories)
                            <small> 
                                <span>{{ __('Categories:') }}</span>
                                {!! $categories !!}
                            </small>
                        @endif

                        <small>Posted on: {{ $post->created_at->format('j M Y') }}</small>
                        
                        <small class="info--post__views views tooltip tooltip-left" data-tooltip="Total Views">
                            <span class="views__icon"> 
                                <svg class="icon">
                                    <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#views"></use>
                                </svg> 
                            </span> 
                            {{ $post->views }}
                        </small>
                    </div> 
                </figure>
                <h2 class="post__heading">
                    <a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a>
                </h2>

                <div class="post__excerpt">
                    <p>
                        {{ $excerpt }}
                    </p>
                </div>
            </article>
        </div>
        
    @empty
        <div class="column col-12 posts_cnt">
            <h3 class="mt posts_cnt__item text-center">{{ __('No favourite articles found') }}</h3>
        </div>
    @endforelse

@endsection

@section('pagination')

    @if(count($posts) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-mx-auto">
                    {{ $posts->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush
