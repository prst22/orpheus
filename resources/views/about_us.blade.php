@extends('layouts.main')

@section('title', 'About Us')

@section('meta_description', 'Get to know the team behind our site and our passion for the world of betting and gambling. Discover expert insights, analysis, and opinions on everything from casino games to sports betting. Join our community and stay informed on the latest trends and news in the gambling industry.')

@section('content') 
    <div class="column col-md-12 col-8 posts_cnt">
        <h1>About Us</h1>
        <p>Welcome to our site about online gambling and online casinos. Our team is made up of passionate and experienced professionals who are committed to providing the best possible service to our users.</p>
        <h2>Our Team</h2>
        <p>Our team is made up of individuals who have a deep passion for online gambling and online casinos. Each member of our team brings a unique set of skills and expertise to our site, and we work together to provide the best possible service to our users.</p>
        <h3>David Lee - Content Manager</h3>
        <p>David is our content manager and has over 5 years of experience in the online gambling industry. She is responsible for ensuring that our site has high-quality and informative content that meets the needs of our users.</p>
        <h3>Jane Doersy - SEO Specialist</h3>
        <p>Jane is our SEO specialist and has over 7 years of experience in the industry. He is responsible for ensuring that our site is optimized for search engines and that we are reaching our target audience.</p>
        <h3>Mark Thompson - Content Writer</h3>
        <p>Mark is our content writer and has over 4 years of experience in the online gambling industry. He is responsible for researching and creating new content for our site, as well as updating and revising existing content to ensure accuracy and relevance.</p>
        <h3>Emily Chen - Content Writer</h3>
        <p>Emily is our content writer and has over 3 years of experience in the industry. She is responsible for creating engaging and informative content for our site that is tailored to the needs of our users.</p>
        <p>Together, our team is committed to providing the best possible service to our users and ensuring that our site is a valuable resource for those interested in online gambling and online casinos.</p>
        <a href="{{ route('contact') }}" class="btn btn-lg btn-link">Contact</a>
    </div>        
@endsection

@section('sidebar')
    
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            @if($all_categories)
                <li class="divider" data-content="CATEGORY"></li>
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                </li>
                @foreach ($all_categories as $key => $category_link)
                    <li class="menu-item">{!! $category_link !!}</li>
                @endforeach
            @endif
        </ul>
    </div>

@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <aside class="menu">
            @if($all_categories)
                <ul class="category_btn" tabindex="0">
                    <li class="divider" data-content="CATEGORY">
                        <span id="category_icon">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-down"></use>
                            </svg> 
                        </span>
                    </li>
                </ul>
                <ul class="menu__inner">
                    <li class="menu-item">
                        <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                    </li>

                    @foreach ($all_categories as $key => $category_link)
                        <li class="menu-item">{!! $category_link !!}</li>
                    @endforeach
                </ul>
            @endif
        </aside>
    </div>

@endsection

@section('footer_roulette')
    @include('inc.footer.footer_roulette')
@endsection

@if (Route::currentRouteName() === 'home' && (request('page') === null || request('page') === '1'))
    @push('footer_scripts')
        <script type="application/ld+json">
            {"@context":"https://schema.org","@type":"Organization","name":"GamblingCentral","url":"{{ URL::to('/') }}","logo":"{{ URL::asset('assets') }}/logo.svg","sameAs": ["https://gcentralin.tumblr.com/"],"potentialAction": [{"@type": "SearchAction","target": {"@type": "EntryPoint","urlTemplate": "{{ URL::to('/search') }}?s={search_term_string}"},"query-input": "required name=search_term_string"}]}
        </script>   
    @endpush
@endif

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush