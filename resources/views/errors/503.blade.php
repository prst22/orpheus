@extends('errors.layouts.main')

@section('title', '503')

@push('header_styles')
    <style>
        html, body{
            margin: 0;
            padding: 0;
        }
        img{
            display: block;
            height: auto;
            width: 100%;
            max-width: 500px;
        }
        .error{
            font-family: 'Function', sans-serif;
            background-color: #101010;
            text-align: center;
            color: #d9d9d9;
        }
        .error_content{
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
        .error_content>div{
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            min-height: 500px;
            overflow: hidden;
        }
        .animate{
            padding: 2rem;
        }
    </style>
@endpush

@section('content')

    <div class="error_content">
        <div>
            <div class="animate error_image">
                <img src="{{ URL::asset('assets/404.svg') }}" alt="500">
            </div>
            <h1 class="animate">Service Temporarily Unavailable</h1>
        </div>
    </div>
    
@endsection
