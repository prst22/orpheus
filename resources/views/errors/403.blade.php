@extends('errors.layouts.main')

@section('title', '403')

@push('header_styles')
    <style>
        html, body{
            margin: 0;
            padding: 0;
        }
        img{
            display: block;
            height: auto;
            width: 100%;
            max-width: 500px;
        }
        .error{
            font-family: 'Function', sans-serif;
            background-color: #101010;
            text-align: center;
            color: #d9d9d9;
        }
        .error_content{
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
        .error_content>div{
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            min-height: 500px;
            overflow: hidden;
        }
        .animate{
            padding: 2rem;
        }
        a{
            color: #769db9;
            outline: none;
            text-decoration: none;
        }
        a:focus{
            box-shadow: 0 0 0 .1rem rgba(85,133,167,.2);
        }
        a:focus,
        a:hover,
        a:active,
        a.active{
            color: #446a85;
            text-decoration: underline;
        }
        a:visited{
            color: #769db9;
        }
        .error_content >div a{
            margin-bottom: 0.4rem;
        }
    </style>
@endpush

@section('content')

    <div class="error_content">
        <div>
            <div class="animate error_image">
                <img src="{{ URL::asset('assets/404.svg') }}" alt="404">
            </div>
            @php
                $err = trim($exception->getMessage(), ".");   
            @endphp
            <h1 class="animate">403 | {{ $err }}</h1>
            <a href="{{ url('/') }}" class="animate">Go Home</a>
        </div>
    </div>

@endsection
