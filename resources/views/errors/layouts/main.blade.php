<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}} | @yield('title')</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ URL::asset('fonts/Function-Regular.ttf') }}" rel="stylesheet">
        @stack('header_styles')
        @stack('header_scripts')
    </head>
    <body class="error">
        @yield('content')
    </body>
</html>