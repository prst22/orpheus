@extends('layouts.main')

@section('title', $title)

@section('meta_description', 'Search results')

@section('content')

    <div class="column col-md-12 col-8 search_results">

        <header class="search_results__header">
            @if ($query !== '')
                <h1 class="search_title">
                    Search Results For: <span class="query"><b>{{ $query }}</b></span>
                </h1>
            @endif
            {{-- <div class="search mr-0">
                <form method="GET" action="{{ route('search') }}">
                    <div class="input-group">
                    <input class="form-input input" type="text" name="query" placeholder="{{ __('Search') }}" required value="{{ $query }}">
                        <button class="btn btn btn-primary input-group-btn" aria-label="search button">
                            <svg class="icon">
                                <use xlink:href="{{ URL::asset('assets') }}/symbol-defs.svg#search"></use>
                            </svg> 
                        </button>
                    </div>
                </form>
            </div> --}}
        </header>
       
        @forelse($results as $result)
            @php
                $categories = $result->get_categories($result->id);
            @endphp
            
            @if( get_class($result) == 'App\Post' )
                <div class="search_results_item search_results_item--post">
                    @include('inc.search_results.post', [ 'categories' => $categories ])
                </div> 
            @elseif( get_class($result) == 'App\Stream' )
                <div class="search_results_item search_results_item--stream">
                    @include('inc.search_results.stream')
                </div> 
            @endif
        @empty
            <h3 class="text-center mt">{{ __('Nothing found') }}</h3>
        @endforelse
    </div>

@endsection

@section('sidebar')
    
    <div class="column col-4 hide-md sidebar">
        <ul class="menu">
            @if($all_categories)
                <li class="divider" data-content="CATEGORY"></li>
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                </li>
                @foreach($all_categories as $key => $category_link)
                    <li class="menu-item">{!! $category_link !!}</li>
                @endforeach
            @endif
        </ul>
    </div>

@endsection

@section('mobile_sidebar')

    <div class="column col-12 show-md sidebar sidebar--mobile">
        <aside class="menu">
            @if($all_categories)
                <ul class="category_btn" tabindex="0">
                    <li class="divider" data-content="CATEGORY">
                        <span id="category_icon">
                            <svg class="icon">
                                <use xlink:href="{{URL::asset('assets')}}/symbol-defs.svg#chevron-down"></use>
                            </svg> 
                        </span>
                    </li>
                </ul>
                <ul class="menu__inner">
                    <li class="menu-item">
                        <a href="{{ route('home') }}" class="{{ Route::currentRouteName() === 'home' || Route::currentRouteName() === 'posts.sortby' ? 'active_category' : ''}}"> {{ __('All') }} </a>
                    </li>
                    @foreach ($all_categories as $key => $category_link)
                        <li class="menu-item">{!! $category_link !!}</li>
                    @endforeach
                </ul>
            @endif
        </aside>
    </div>

@endsection

@section('pagination')

    @if(count($results) > 0)
        <section class="container grid-lg pag_cnt">
            <div class="columns">
                <div class="column col-md-12 col-8 col-ml-auto">
                    {{ $results->appends(['query' => $query])->onEachSide(2)->links() }}
                </div>
            </div>
        </section>
    @endif

@endsection

@section('footer')
    @include('inc.footer.footer')
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ mix('js/main.js') }}"></script>
@endpush