require('./polyfills.js');
import lazySizes from 'lazysizes';
import 'lazysizes/plugins/blur-up/ls.blur-up';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
// polyfills
import 'lazysizes/plugins/respimg/ls.respimg';
if (!('object-fit' in document.createElement('a').style)){
	require('lazysizes/plugins/object-fit/ls.object-fit');
}

import jumper from 'jump.js-cancelable';
import debounce from 'lodash/debounce';

const Isotope = require('isotope-layout');

//lazysizes options
window.lazySizesConfig = window.lazySizesConfig || {};
lazySizesConfig.expand = 1000;
lazySizesConfig.expFactor = 3;
lazySizesConfig.loadMode = 2;
lazySizes.cfg.blurupMode = 'auto';

require('./svgxuse.js');
require('./toasts.js');
require('./mobile_menu.js');
require('./after_post_gallery');
require('./casino_rating');
require('./lottery_tracker');

const url = new URL(window.location),
urlParams = new URLSearchParams(url.search),
goTop = document.querySelector('.go_top'),
postsContainer = document.querySelector('.posts_cnt'),
singlePostContainer = document.querySelector('.single_thumbnail_outer'),
streamsContainer = document.querySelector('.streams'),
streamItemContainer = document.querySelectorAll('.stream_container'),
favStreamsContainer = document.querySelectorAll('.favourite_streams .channel'),
loginCard = document.querySelector('.login'),
spinBtn = document.getElementById('spin'),
searchContainer = document.querySelector('.search_results'),
// searchResultItem = document.querySelectorAll('.search_results_item'),
contactForm = document.querySelector('.contact_form'),
commentEditmodalTrigger = document.querySelectorAll('.comments_container .modal_edit_trigger'),
commentEditmodalCloseBtn = document.querySelectorAll('.comments_container [href="#close"]'),
singlePostContentCnt = document.querySelector('.single_post'),
singleStreamCnt = document.querySelector('.twitch_player_cnt'),
shareFbBtn = document.getElementById('fb'),
shareTwitterBtn = document.getElementById('tw'),
dropDownToggle = document.querySelectorAll('.dropdown-toggle'),
mobileCategoriesBtn = document.querySelector('.category_btn'),
mobileCategoriesCnt = document.querySelector('.menu__inner'),
categoryRevealIcon = document.getElementById('category_icon'),
// slideUp = {
// 	ease: 'cubic-bezier(0.445, 0.050, 0.550, 0.950)',
// 	origin: 'top',
// 	distance: '15px',
// 	opacity: 0,
// 	delay: 60,
// 	duration: 550,
// 	interval: 100
// },
// slideUpStream = {
// 	ease: 'cubic-bezier(0.445, 0.050, 0.550, 0.950)',
// 	origin: 'top',
// 	distance: '15px',
// 	opacity: 0,
// 	delay: 60,
// 	duration: 550,
// 	interval: 100
// },
// searchResAnimation = {
// 	ease: 'cubic-bezier(0.445, 0.050, 0.550, 0.950)',
// 	origin: 'top',
// 	distance: '15px',
// 	opacity: 0,
// 	delay: 60,
// 	duration: 350,
// 	interval: 100
// },
filterButtons = document.querySelectorAll('.stream_sorting_cnt [data-filter]'),
sortingButtons = document.querySelectorAll('.stream_sorting_cnt [data-sort-value]'),
wheel = document.querySelector('.roulette__img');

let transitionEvent = whichTransitionEvent(),
currentRotation = 0,
handleDebouncedWheel = null;

// if (postsContainer && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal('.posts_cnt__item', slideUp);
// if (singlePostContainer && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal(singlePostContainer, slideUp);
// if (favStreamsContainer && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal(favStreamsContainer, slideUpStream);
// if (loginCard && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal(loginCard, slideUp);
// if (searchContainer && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal(searchResultItem, searchResAnimation);
// if (contactForm && typeof ScrollReveal !== 'undefined') ScrollReveal().reveal(contactForm, slideUp);

if (mobileCategoriesBtn) {
	mobileCategoriesBtn.addEventListener('click', toggleCategories);
}

// streams page grid start
if (streamsContainer) {

	if (streamItemContainer.length > 0) {

		const grid = document.querySelector('.streams'),
		iso = new Isotope( grid, {
			itemSelector: '.stream_container',
			percentPosition: true,
			transitionDuration: 600,
			masonry: {
				columnWidth: '.grid_sizer',
				gutter: '.gutter_sizer'
			},
			getSortData: {
				name: '.name',
				viewers: '.number parseInt', 
			},
			sortAscending: {
				viewers: false
			},
		});

		if (filterButtons.length > 0) {
			const type = 'type',
			streamType = urlParams.get(type),
			allStreams = document.querySelector('[data-filter="*"]'),
			casinoStreams = document.querySelector('[data-filter=".casino"]'),
			pokerStreams = document.querySelector('[data-filter=".poker"]');

			filterButtons.forEach( btn => {
				btn.addEventListener('click', filterStreams);
			});

			function filterStreams(e) {
				filterButtons.forEach( btn => {
					btn.classList.remove('active');
				});

				const filterValue = e.currentTarget.getAttribute('data-filter');
				iso.arrange({ filter: filterValue });
				e.currentTarget.classList.add('active');
				
				if (filterValue === '.poker') {

					setUrlParametrs(type, 'poker');

				} else if (filterValue === '.casino') {

					setUrlParametrs(type, 'casino-games');

				} else {

					setUrlParametrs(type, 'all');

				}
			}

			if (streamType === 'casino-games') {

			    casinoStreams.click()

			} else if (streamType === 'poker') {

			    pokerStreams.click()    

			} else {

			    allStreams.click()

			}
		}

		if (sortingButtons.length > 0) {
			const sorting = 'sorting',
			sortingType = urlParams.get(sorting),
			sortByName = document.querySelector('[data-sort-value="name"]'),
			sortByViewers = document.querySelector('[data-sort-value="viewers"]');

			sortingButtons.forEach( btn => {
				btn.addEventListener('click', sortStreams);
			});
		
			function sortStreams(e) {
				sortingButtons.forEach( btn => {
					btn.classList.remove('active');
				});
				const sortValue = e.currentTarget.getAttribute('data-sort-value');
				iso.arrange({ sortBy: sortValue });
				e.currentTarget.classList.add('active');

				if (sortValue === 'viewers') {

					setUrlParametrs(sorting, 'viewers')

				} else {

					setUrlParametrs(sorting, 'name')

				}
			}

			if (sortingType === 'viewers') {

			    sortByViewers.click()

			} else {

			    sortByName.click()

			}
		}

	}
	
}
//streams sorting by query str params
function setUrlParametrs(param, value, title = '') {
	if (!param) return
	if (!value) return

	url.searchParams.set(param, value);
	window.history.replaceState({
		param: value
	}, title, url);
}
// streams page grid end

function whichTransitionEvent() {
	let t,
	el = document.createElement("fakeelement"),
	transitions = {
		"transition"      : "transitionend",
		"OTransition"     : "oTransitionEnd",
		"MozTransition"   : "transitionend",
		"WebkitTransition": "webkitTransitionEnd"
	}

	for (t in transitions) {
		if (el.style[t] !== undefined) {
			return transitions[t];
		}
	}
}

if (spinBtn) {
	spinBtn.addEventListener('click', spinRoulette);  

	function spinRoulette(e){
		e.preventDefault();
		let transitionDuration = window.getComputedStyle(wheel).transitionDuration;
		
		spinBtn.classList.add('is_disabled');
		// wheel.style.transitionDuration = `${5}s`;
		let newAngle = currentRotation + 360 + getRandomArbitrary(1, 361);
		wheel.style.transform = `rotate(${0}deg)`;
		wheel.style.transform = `rotate(${newAngle}deg)`;
		currentRotation = newAngle;

		if(transitionDuration === '0s'){
			spinBtn.classList.remove('is_disabled');
			return    
		}

		wheel.addEventListener(transitionEvent, e => {
			spinBtn.classList.remove('is_disabled');
		});  
	}
}

function getRandomArbitrary(min, max) {
	return Math.abs(Math.random() * (max - min) + min);
}

goTop.addEventListener('click', () => {
	jumper.jump('html', {
		duration: 1200,
		offset: 0
	});
});

// comments modals start
if (commentEditmodalTrigger.length > 0) {
	commentEditmodalTrigger.forEach( btn => {
		btn.addEventListener('click', (e) => {
			e.preventDefault();
			const eidtCommentModal = btn.parentElement.parentElement.querySelector('.edit_comment_modal');
			eidtCommentModal.classList.add('active');
		});
	});
}
if (commentEditmodalCloseBtn.length > 0) {
	commentEditmodalCloseBtn.forEach( btnClose => {
		btnClose.addEventListener('click', (e) => { 
			e.preventDefault();
			btnClose.closest('.edit_comment_modal').classList.remove('active');
		});
	});
}
// comments modals end

// share buttons start
if (singlePostContentCnt || singleStreamCnt) {
	if (shareFbBtn) {
		document.getElementById('fb').addEventListener('click', function(e){
			e.preventDefault();
			const shareFbUrl = document.getElementById('fb').href;
			window.open(shareFbUrl, "pop", "width=600, height=400", "scrollbars=no");
		});
	}
	if (shareTwitterBtn) {
		document.getElementById('tw').addEventListener('click', function(e){
			e.preventDefault();
			const shareTwUrl = document.getElementById('tw').href;
			window.open(shareTwUrl, "pop", "width=600, height=400", "scrollbars=no");
		});
	}
}
// share buttons end

// top bar drop down start
if (dropDownToggle) {
	dropDownToggle.forEach( trigger => {
		trigger.addEventListener('click', e => { e.preventDefault() });
	});
}
// top bar drop down end

function toggleCategories() {
	categoryRevealIcon.classList.toggle('rotate');

	if(mobileCategoriesCnt.classList.contains('expanded')){
		mobileCategoriesCnt.classList.add('hidden'); 
		mobileCategoriesCnt.classList.remove('expanded'); 
	}else{
		mobileCategoriesCnt.classList.remove('hidden');
		mobileCategoriesCnt.classList.add('expanded'); 
	}
}

function cancelJumper() {
	if (jumper.isJumping()) {
		jumper.cancel()
	}
}

//add events to prevet jump js on user scroll
document.addEventListener('DOMContentLoaded', () => {
	handleDebouncedWheel = debounce(cancelJumper, 20);
	window.addEventListener("wheel", handleDebouncedWheel, false);
	window.addEventListener("touchstart", cancelJumper, false);
});
