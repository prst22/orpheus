import Vue from 'vue'
import Vuex from 'vuex'
import api from './api/apiCall.js'
import { createDatasetsForChart } from './helpers/functions.js'

Vue.use(Vuex)

const url = new URL(window.location),
defaultPickedSlotId = url.searchParams.get('slot'),
defaultPickedCasinoId = url.searchParams.get('casino'),
currencyObj = JSON.parse(sessionStorage.getItem('currency'));

export default new Vuex.Store({
    state: {
        apiResponseData: [],
        chartData: {},
        dataError: {},
        sessionError: {},
        loading: true,
        activeSlotsinCasino: [],
        casinoSlots: [],
        slotCasinos: [],
        slotsLoading: true,
        chatColors: {
            yellow: "rgb(231, 250, 88)",
            greeen: "rgb(0, 243, 177)",
        },
        tableLoading: true,
        slotName: '',
        activeCasinoName: '',
        pickedCasinoName: '',
        pickedCasinoId: defaultPickedCasinoId || null,
        pickedSlotId: defaultPickedSlotId || 1,
        activeCasinoId: null,
        queryCasinoId: null,
        querySlotId: defaultPickedSlotId || 1,
        currency: currencyObj || { 
            code: 'EUR',
            symbol: '€',
            value: 100
        },
    },
	mutations: {
        setApiResponseData(state, data) {
            state.apiResponseData = data.value
        },
		loading(state, data) {
            state.loading = data.value
        },
        setSlotsLoading(state, data) {
            state.slotsLoading = data.value
        },
        setTableLoading(state, data) {
            state.tableLoading = data.value
        },
        setDataError(state, data) {
            state.dataError = data.value
        },
        setSessionError(state, data) {
            state.sessionError = data.value
        },
        setСhartData(state, data) {
            state.chartData = data.value
        },
        setActiveSlotsinCasino(state, data) {
            state.activeSlotsinCasino = data.value
        },
        setCasinoSlots(state, data) {
            state.casinoSlots = data.value
        },
        setSlotCasinos(state, data) {
            state.slotCasinos = data.value
        },
        setSlotName(state, data) {
            state.slotName = data.value
        },
        setActiveCasinoName(state, data) {
            state.activeCasinoName = data.value
        },
        setPickedCasinoName(state, data) {
            state.pickedCasinoName = data.value
        },
        setPickedCasinoId(state, data) {
            state.pickedCasinoId = data.value
        },
        setPickedSlotId(state, data) {
            state.pickedSlotId = data.value
        },
        setActiveCasinoId(state, data) {
            state.activeCasinoId = data.value
        },
        setQueryCasinoId(state, data) {
            state.queryCasinoId = data.value
        },
        setQuerySlotId(state, data) {
            state.querySlotId = data.value
        },
        setCurrency(state, data) {
            state.currency = data.value
        },
	},
	actions: {
        constructDataForChart: ({ commit, state }) => {
            
            if (state.apiResponseData === undefined || state.apiResponseData === null) {
                commit('setApiResponseData', { value: [] }) 
                commit('setActiveSlotsinCasino', { value: [] })
                return    
            }

            if (state.apiResponseData.length <= 0) {
                commit('setApiResponseData', { value: [] })
                commit('setActiveSlotsinCasino', { value: [] })
                return
            } 

            let allLables = [],
            datasets = [],
            jackpotValues = []
            // filter only slots that are active
            const slotsData = state.apiResponseData.filter( (slot) => {
                if (slot.active === 1) return slot
            })

            commit('setActiveSlotsinCasino', { value: slotsData })
            
            createDatasetsForChart(state.activeSlotsinCasino, allLables, datasets, jackpotValues, state.chatColors, state.currency)

            commit('setСhartData', { value: {
                    labels: allLables,
                    datasets
                } 
            }) 
        },
        async getData(context, data) { 
            await api.request(data.casino_id, data.slot_id)
            this.dispatch('constructDataForChart')
            this.dispatch('getSlotName')
            this.dispatch('getActiveCasinoName')
            this.dispatch('getActiveCasinoId')
            this.commit('loading', { value: false }) 
            // SET RADIO BUTTONS TO PICKED CASINO AND SLOTS WHEN CLICKED ON JACPOTS TABLE
            if (context.state.pickedSlotId !== data.slot_id) {
                this.dispatch('getPickedSlotId', { value: data.slot_id })
            }
            if (context.state.pickedCasinoId !== data.casino_id) {
                this.dispatch('getPickedCasinoId', { value: data.casino_id }) 
            } 
        },
        async getJackpotTableData(context, data) {
            await api.requestSlotCasions(data.casino_ids, data.slot_id)
            this.dispatch('getSlotName')
            this.commit('setTableLoading', { value: false }) 
        },
        async getCasinoSlotsData(context, data) {
            await api.requestCasinoSlots(data.id)
            this.commit('setSlotsLoading', { value: false }) 
        },
        getQueryParam() {
            const url = new URL(window.location),
            params = new URLSearchParams(url.search),
            c_id = params.get('casino'),
            s_id = params.get('slot'),
            casinoId = !isNaN(c_id) && c_id !== null && c_id != 0 ? parseInt(c_id, 10) : null,
            slotId = !isNaN(s_id) && s_id !== null && s_id != 0  ? parseInt(s_id, 10) : null
            this.commit('setQueryCasinoId', { value: casinoId })
            this.commit('setQuerySlotId', { value: slotId })
            // set picked slot/casino ids
            if (casinoId && slotId) {
                this.dispatch('getPickedSlotId', { value: slotId })
                this.dispatch('getPickedCasinoId', { value: casinoId })
            }
        },
        getSlotName(context) {
            if (context.state.activeSlotsinCasino !== null && context.state.activeSlotsinCasino.length > 0) {
                return context.state.activeSlotsinCasino[0].slot_name !== undefined ? this.commit('setSlotName', { value: context.state.activeSlotsinCasino[0].slot_name }) : this.commit('setSlotName', {value: '' })
            } 
            if (context.state.slotCasinos !== null && context.state.slotCasinos.length > 0) {
                return context.state.slotCasinos[0].slot_name !== undefined ? this.commit('setSlotName', { value: context.state.slotCasinos[0].slot_name }) : this.commit('setSlotName', {value: '' })
            }

            this.commit('setSlotName', {value: ''})
        },
        getActiveCasinoName(context) {
            if (context.state.activeSlotsinCasino !== null && context.state.activeSlotsinCasino.length > 0) {
                if (context.state.activeSlotsinCasino[0].casino_name !== null && context.state.activeSlotsinCasino[0].casino_name !== undefined) {
                    this.commit('setActiveCasinoName', { value: context.state.activeSlotsinCasino[0].casino_name })
                } else {
                    this.commit('setActiveCasinoName', { value: context.state.pickedCasinoName })
                }
            } else {
                this.commit('setActiveCasinoName', { value: context.state.pickedCasinoName })
            }
        },
        getActiveCasinoId(context) {
            if (context.state.activeSlotsinCasino !== null && context.state.activeSlotsinCasino.length > 0) {
                if (context.state.activeSlotsinCasino[0].casino_id !== null && context.state.activeSlotsinCasino[0].casino_id !== undefined) {
                    this.commit('setActiveCasinoId', { value: context.state.activeSlotsinCasino[0].casino_id })
                } 
            }
        },
        getPickedCasinoName(context, data) {
            this.commit('setPickedCasinoName', { value: data.value ? data.value : '' })
        }, 
        getPickedCasinoId(context, data) {            
            this.commit('setPickedCasinoId', { value: data.value })
            // set query string casino id
            this.dispatch('getQueryStringCasinoId', { value: data.value })
            //update adress bar
            this.dispatch('updateBrowserAdressBar')
        },
        getPickedSlotId(context, data) {
            this.commit('setPickedSlotId', { value: data.value })
            // set query string slot id
            this.dispatch('getQueryStringSlotId', { value: data.value })
            //update adress bar
            this.dispatch('updateBrowserAdressBar')
        },
        // query strings
        getQueryStringCasinoId(context, data) {
            this.commit('setQueryCasinoId', { value: data.value })
        },
        getQueryStringSlotId(context, data) {
            this.commit('setQuerySlotId', { value: data.value })
        },
        updateBrowserAdressBar(context) {
            const url = new URL(window.location),
            currentCasinoQuery = url.searchParams.get('casino'),
            currentSlotQuery = url.searchParams.get('slot')
            url.searchParams.set('casino', context.state.pickedCasinoId),
            url.searchParams.set('slot', context.state.pickedSlotId)

            if (currentCasinoQuery !== context.state.pickedCasinoId || currentSlotQuery !== context.state.pickedSlotId) {
                window.history.replaceState({}, '', url)
            }
        },
        async getCurrency(context, data) {
            if (data.value != 'EUR') {
                await api.requestCurrencyExchange(data.value)
            } else if (data.value == 'EUR') {
                this.commit('setCurrency', { value: { 
                        code: 'EUR',
                        symbol: '€',
                        value: 1
                    }
                })
            }
            sessionStorage.setItem('currency', JSON.stringify(context.state.currency))
        }
  	}
})