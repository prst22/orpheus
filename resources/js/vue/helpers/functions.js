export function randomColor(obj) {

    let keys = Object.keys(obj)
    return obj[keys[keys.length * Math.random() << 0]]

}

export function numberWithCommas(x) {

    if (isNaN(x) || x == null || typeof x === 'undefined') {
        return 0
    }
    
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

}

export function createDatasetsForChart(slots = [], allLables = [], datasets = [], jackpotValues = [], chatColors = {}, currency = { 
    code: 'EUR',
    symbol: '€',
    value: 1
}) {
    slots.forEach((element, index) => {
            
        if (element.date_amount === null) {
            return
        }
            
        if (element.date_amount.length <= 0) {
            return
        }

        jackpotValues.push([])
        element.date_amount.forEach(el => {
            
            allLables.push(el.date)
            // Currrency convertion if needed
            
            jackpotValues[index].push( convertCurrency(el.jack_data.amount, currency) )

        })
        
        const grafColor = randomColor(chatColors)
      
        datasets.push({})
        datasets[index].data = jackpotValues[index]
        datasets[index].label = `${element.slot_name} ${element.casino_name}`
        datasets[index].fill = true
        datasets[index].pointRadius = 4
        datasets[index].pointHoverRadius = 8
        datasets[index].borderColor = grafColor
        datasets[index].pointBackgroundColor = grafColor
        datasets[index].backgroundColor = 'rgba(0, 243, 177, 0.5)'

    })

}

export function convertCurrency(amount = 0, currency = { 
    code: 'EUR',
    symbol: '€',
    value: 1
}) {
    if (currency.code != 'EUR') {
        let floatVal = (Number(amount) / 100) * currency.value;  
        return parseFloat( (floatVal).toFixed(2) )
    } else {
        return  Number(amount) / 100
    }
} 