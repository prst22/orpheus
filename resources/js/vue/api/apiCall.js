import store from '../store';

export default {  
    request(casino_id = null, slot_id = null) {
        store.commit('loading', { value: true })
        store.commit('setApiResponseData', { value: [] })  

        return axios.get(`/api/jackpots?casino_id=${casino_id}&slot_id=${slot_id}`)
            .then((response) => {
                if(response.status === 200){
                    store.commit('setApiResponseData', { value: response.data.data })   
                } else {
                    store.commit('setApiResponseData', { value: [] })  
                }
            })
            .catch((error) => {
                store.commit('setApiResponseData', { value: [] })  
                // handle error   
                this.setApiErrors(error.response.data, error.response.status)
            })    
    },
    requestCasinoSlots(casino_id = null) {
        store.commit('setSlotsLoading', { value: true })

        return axios.get(`/api/casino-slots?casino_id=${casino_id}`)
            .then((response) => {
                if(response.status === 200){
                    store.commit('setCasinoSlots', { value: response.data.data.map( slot => slot.id ) })
                } else {
                    store.commit('setCasinoSlots', { value: [] })
                }   
            })
            .catch((error) => {
                store.commit('setCasinoSlots', { value: [] })
                // handle error setTableLoading   
                this.setApiErrors(error.response.data, error.response.status)
            }) 
    },
    requestSlotCasions(casino_ids = null, slot_id = null) {
        store.commit('setTableLoading', { value: true }) 

        let casinos = casino_ids !== null ? casino_ids.join(',') : null

        return axios.get(`/api/jackpots?casino_id=${casinos}&slot_id=${slot_id}`)
            .then((response) => {
                if(response.status === 200) {
                    store.commit('setSlotCasinos', { value: response.data.data })
                } else {
                    store.commit('setSlotCasinos', { value: [] })        
                }
            })
            .catch((error) => {
                store.commit('setSlotCasinos', { value: [] })  
                // handle error        
                console.log(error.response.data.message)
            }) 
    },
    requestCurrencyExchange(code) {
        return axios.get(`/api/currency?code=${code}`)
            .then((response) => {
                if(response.status === 200){
                    store.commit('setCurrency', { value: response.data }) 
                } else {
                    store.commit('setCurrency', { value: { 
                            code: 'EUR',
                            symbol: '€',
                            value: 1
                        }
                    })   
                }
            })
            .catch((error) => {
                // handle error
                store.commit('setCurrency', { value: {
                        code: 'EUR',
                        symbol: '€',
                        value: 1
                    }
                }) 
                console.log(error.message) 
            })    
    },
    setApiErrors(error, errorStatusCode){
        switch (errorStatusCode) {
            case 401:       
                    store.commit('setSessionError', { value: error })   
                break;
            case 422:
                    store.commit('setDataError', { value: error })   
                break;
            case 429:
                    store.commit('setDataError', { value: error })   
                break;
            default:
                console.log(error.message) 
                break;
        }
    }
};

