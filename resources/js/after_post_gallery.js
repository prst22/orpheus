import GLightbox from 'glightbox';
const Isotope = require('isotope-layout'),
packery = require('isotope-packery');

const singlePostContent = document.querySelector('.post_content');

let gallery,
allPostImages,
postImagesSrcsArr = [];

if (singlePostContent) allPostImages = singlePostContent.querySelectorAll('.image > img');

if (singlePostContent && allPostImages.length > 1) {
    
    gallery = document.createElement('div');
    gallery.classList.add('post_gallery');
    gallery.innerHTML = '<h3>Article Images:</h3><div class="loading loading-sm"></div>';
    let galleryInner = document.createElement('div');
    galleryInner.classList.add('post_gallery__inner');
    galleryInner.innerHTML = '<div class="gutter_sizer_post_gallery"></div>';

    allPostImages.forEach(image => {
        let srcsetArr = image.getAttribute('srcset') ? image.getAttribute('srcset').split(" ") : [image.src],
        lightboxItem = document.createElement('div');
        lightboxItem.setAttribute('class', 'post_gallery__item');
        lightboxItem.innerHTML = `<a href="${image.src}" class="glightbox" data-type="image" data-gallery="article-gallery"><img src="${srcsetArr[0]}" alt="gallery image"></a>`;
        galleryInner.appendChild(lightboxItem);
        postImagesSrcsArr.push({
            'href': image.src,
            'type': 'image',
        });
    });

    const inArticleImagesGlighInstance = GLightbox({
        touchNavigation: true,
        loop: true,
        openEffect: 'fade',
        closeEffect: 'fade',
        slideEffect: 'fade',
        elements: postImagesSrcsArr,
        autoplayVideos: false,
        svg: {
            close: '<svg  class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Close</title><path d="M5.293 6.707l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0l5.293-5.293 5.293 5.293c0.391 0.391 1.024 0.391 1.414 0s0.391-1.024 0-1.414l-5.293-5.293 5.293-5.293c0.391-0.391 0.391-1.024 0-1.414s-1.024-0.391-1.414 0l-5.293 5.293-5.293-5.293c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414z"></path></svg>',
            prev: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Previous</title><path d="M15.707 17.293l-5.293-5.293 5.293-5.293c0.391-0.391 0.391-1.024 0-1.414s-1.024-0.391-1.414 0l-6 6c-0.391 0.391-0.391 1.024 0 1.414l6 6c0.391 0.391 1.024 0.391 1.414 0s0.391-1.024 0-1.414z"></path></svg>',
            next: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Next</title><path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path></svg>'
        },
    });
    
    gallery.appendChild(galleryInner);   
    insertAfter(gallery, singlePostContent);

    const bottomGalleryGlighInstance = GLightbox({
        touchNavigation: true,
        loop: true,
        openEffect: 'fade',
        closeEffect: 'fade',
        slideEffect: 'fade',
        autoplayVideos: false,
        svg: {
            close: '<svg  class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Close</title><path d="M5.293 6.707l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0l5.293-5.293 5.293 5.293c0.391 0.391 1.024 0.391 1.414 0s0.391-1.024 0-1.414l-5.293-5.293 5.293-5.293c0.391-0.391 0.391-1.024 0-1.414s-1.024-0.391-1.414 0l-5.293 5.293-5.293-5.293c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414z"></path></svg>',
            prev: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Previous</title><path d="M15.707 17.293l-5.293-5.293 5.293-5.293c0.391-0.391 0.391-1.024 0-1.414s-1.024-0.391-1.414 0l-6 6c-0.391 0.391-0.391 1.024 0 1.414l6 6c0.391 0.391 1.024 0.391 1.414 0s0.391-1.024 0-1.414z"></path></svg>',
            next: '<svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24"><title>Next</title><path d="M9.707 18.707l6-6c0.391-0.391 0.391-1.024 0-1.414l-6-6c-0.391-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l5.293 5.293-5.293 5.293c-0.391 0.391-0.391 1.024 0 1.414s1.024 0.391 1.414 0z"></path></svg>'
        }
    });

    window.addEventListener('load', function(){
        let grid = document.querySelector('.post_gallery__inner'),
        iso = new Isotope( grid, {
            layoutMode: 'packery',
            itemSelector: '.post_gallery__item',
            percentPosition: true,
            transitionDuration: 110,
            stagger: 0,
            packery: {
                gutter: '.gutter_sizer_post_gallery'
            },
            sortAscending: {
                viewers: false
            },
            hiddenStyle: {
                opacity: 0,
                transform: 'translateY(-15px)'
            },
            visibleStyle: {
                opacity: 1,
                transform: 'translateY(0)'
            }
        }),
        loader = gallery.querySelector('.loading.loading-sm');
        gallery.removeChild(loader);
        galleryInner.classList.add('post_gallery__inner--revealed');
    }); 
    
    singlePostContent.querySelectorAll('.image > img').forEach((image, index)  => {
        image.addEventListener('click', () => inArticleImagesGlighInstance.openAt(index));
    });
} else if (singlePostContent && allPostImages.length === 1) {
    allPostImages.forEach(image => {
        image.style.cursor = 'auto';
    });
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}


