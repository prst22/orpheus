const lottoMaxCard = document.querySelectorAll('.lottomax'),
lottoMaxModalOpenBtn = document.querySelectorAll('.max_millions_btn'),
lottoMaxModalCloseBtn = document.querySelectorAll('.max_millions_numbers_modal [href="#close"]');

if (lottoMaxModalOpenBtn) {
    lottoMaxModalOpenBtn.forEach( btn => {
        btn.addEventListener('click', (btn) => {
            console.log(btn.currentTarget.parentElement.nextElementSibling)
            btn.currentTarget.parentElement.nextElementSibling.classList.add('active');
        });
    })
}

if (lottoMaxCard.length > 0) {
    lottoMaxModalCloseBtn.forEach( btnClose => {
        btnClose.addEventListener('click', (e) => { 
            e.preventDefault();
            btnClose.closest('.max_millions_numbers_modal').classList.remove('active');
        });
    });
}
