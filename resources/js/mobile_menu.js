// mobile menu start
const menuBtn = document.getElementById('mobile_menu_btn'),
closeMenuBtn = document.getElementById('close_menu_btn'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
mobileMenuList = document.querySelector('.mobile_menu_cnt__inner');
let x = window.matchMedia("(min-width: 1081px)");

if (menuBtn) {
    menuBtn.addEventListener('click', toggleMobileMenu);
    closeMenuBtn.addEventListener('click', toggleMobileMenu);
    // close mobile menu on click out of the drop down menu start
    window.addEventListener('click', (e) => {
        if(!x.matches && document.body.classList.contains('mobile_menu_active') 
            && !e.target.matches('.mobile_menu_cnt__inner') 
            && !e.target.matches('#mobile_menu_btn')
            && !e.target.matches('#mobile_menu_btn *')
            && !e.target.matches('.mobile_menu_cnt__inner *')){
                toggleMobileMenu(e); 
        }
    });
    // close mobile menu on click out of the drop down menu end 
}

function controllVpSize(x) {
    if (x.matches && document.body.classList.contains('mobile_menu_active')) { // If media query matches
        toggleMobileMenu(); 
    }
    if (!x.matches && document.body.classList.contains('dropdownmenu_active')) { 
        closeAllDropDowns(dropDownMenuCnt);
    } 
}

controllVpSize(x); // Call listener function at run time
x.addListener(controllVpSize);
document.addEventListener('keydown', closeMenuOnEsc);

function toggleMobileMenu(e) {
    function openCloseMenuWithNoTransitionDelay(){
        mobileMenu.classList.remove('transition_delay');
        document.body.classList.toggle('mobile_menu_active');
        mobileMenu.classList.toggle('hidden');
        mobileMenuList.classList.toggle('hidden');
    }
    if (e) { //check event to target click 
        if (e.target.closest('button') === closeMenuBtn 
            || e.target.matches('.mobile_menu_cnt')
            || e.key == "Escape" 
            || e.key == "Esc" 
            || e.x == 27) { //choose open or close or esc button to determin whether to use transition delay
                document.body.classList.toggle('mobile_menu_active');
                mobileMenu.classList.add('transition_delay');
                mobileMenu.classList.toggle('hidden');
                mobileMenuList.classList.toggle('hidden');
        } else {
            openCloseMenuWithNoTransitionDelay();
        }
    } else {
        openCloseMenuWithNoTransitionDelay();
    }
}

function closeMenuOnEsc(evt) {
    evt = evt || window.event;
    let isEscape = false,
    bodyClasses = document.body.classList;
    if ("key" in evt) {
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
        isEscape = (evt.keyCode == 27);
    }
    if (isEscape && bodyClasses.contains('mobile_menu_active')) {
        toggleMobileMenu(evt);
    }
}


