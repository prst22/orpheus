import { CountUp } from 'countup.js'

const casinosContainer = document.querySelectorAll('.casino_card_cnt'),
casinoBannerCnt = document.querySelector('.casino_top_banner_cnt'),
casinoTitleCnt = document.querySelector('.single_casino_title_cnt'),
options = {
    decimalPlaces: 1,
    duration: 1.9,
    separator: '.'
};

if (casinosContainer) {
    window.addEventListener('DOMContentLoaded', (event) => {
        casinosContainer.forEach( casinoCard => setupRating(casinoCard) )
    });
}

if (casinoBannerCnt) {
    window.addEventListener('DOMContentLoaded', (event) => {
        setupRating(casinoBannerCnt);
    });
}

if (casinoTitleCnt) {
    window.addEventListener('DOMContentLoaded', (event) => {
        setupRating(casinoTitleCnt);
    });
}

function setupRating(el) {
    let svg = el.querySelector('.casino_rating .rating');

    if (!svg) {
        return
    }

    let circle = el.querySelector('.casino_rating .rating > .rating__bar'),
    value = svg ? svg.getAttribute('data-rating') : null,
    r = circle.getAttribute('r'),
    c = Math.PI * (r * 2);

    if (value) {
        let intVal = Number(value) * 10,
        pct = ((100 - intVal) / 100) * c,
        counterInstace = new CountUp(el.querySelector('.counter'), value, options);
        circle.style.strokeDashoffset = pct + 'px';
        // start rating counter
        if (!counterInstace.error) counterInstace.start();
    }  
}