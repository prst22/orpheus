const removeToastBtn = document.querySelectorAll('.toast > .btn-clear');
let closeToastTimeout;

if (removeToastBtn.length > 0) {
    removeToastBtn.forEach( ( btn ) => {
        btn.addEventListener('click', () => {
            removeToast(btn);
        });
        setTimeout(() => {
            removeToast(btn);
        }, 5000);
    });
}

function removeToast(button) {
    button.parentElement.parentElement.style.maxHeight = '0px';
}