const slotEditmodalTrigger = document.querySelectorAll('.slots .modal_trigger'),
slotEditmodalCloseBtn = document.querySelectorAll('[href="#close"]'),
detachSlotBtn = document.querySelectorAll('.detach_slot_btn');

if (slotEditmodalTrigger.length > 0) {
    slotEditmodalTrigger.forEach( btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const eidtSlotModal = btn.parentElement.querySelector('.update_slot_modal');
            eidtSlotModal.classList.add('active');
        });
    });
}
if (slotEditmodalCloseBtn.length > 0) {
    slotEditmodalCloseBtn.forEach( btnClose => {
        btnClose.addEventListener('click', (e) => { 
            e.preventDefault();
            btnClose.closest('.modal').classList.remove('active');
        });
    });
}
if (detachSlotBtn.length > 0) {
    detachSlotBtn.forEach( btn =>{
        btn.addEventListener('click', (e) => { 
            const eidtSlotModal = btn.parentElement.querySelector('.confirm_slot_detach_modal');
            eidtSlotModal.classList.add('active');
        });
    });
}