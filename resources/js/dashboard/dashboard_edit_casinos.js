const casinoEditmodalCloseBtn = document.querySelectorAll('[href="#close"]'),
deleteCasinoBtn = document.querySelectorAll('.delet_casino_btn'),
addNetentField = document.querySelectorAll('.add_netent_field'),
addYggdrasilField = document.querySelectorAll('.add_yggdrasil_field'),
addRedTigerField = document.querySelectorAll('.add_redtiger_field'),
addPushGamingField = document.querySelectorAll('.add_pushgaming_field'),
addQuickSpinField = document.querySelectorAll('.add_quickspin_field'),
removeFIeldBtn = document.querySelectorAll('.field_remove');

if (casinoEditmodalCloseBtn.length > 0) {
    casinoEditmodalCloseBtn.forEach( btnClose => {
        btnClose.addEventListener('click', (e) => { 
            e.preventDefault();
            btnClose.closest('.modal').classList.remove('active');
        });
    });
}
if (deleteCasinoBtn.length > 0) {
    deleteCasinoBtn.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            const eidtSlotModal = btn.parentElement.querySelector('.confirm_casino_delete_modal');
            eidtSlotModal.classList.add('active');
        });
    });
}
if (addYggdrasilField.length > 0) {
    addYggdrasilField.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            casinoProviderField(e, 'yggdrasil');
        });
    });
}

if (addNetentField.length > 0) {
    addNetentField.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            casinoProviderField(e, 'netent');
        });
    });
}

if (addRedTigerField.length > 0) {
    addRedTigerField.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            casinoProviderField(e, 'redtiger');
        });
    });
}

if (addPushGamingField.length > 0) {
    addPushGamingField.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            casinoProviderField(e, 'pushgaming');
        });
    });
}
if (addQuickSpinField.length > 0) {
    addQuickSpinField.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            casinoProviderField(e, 'quickspin');
        });
    });
}
if (typeof(removeFIeldBtn) != 'undefined' && removeFIeldBtn != null) {
    if (removeFIeldBtn.length > 0) {
        removeFIeldBtn.forEach( btn => {
            btn.addEventListener('click', removeField);
        });
    }
}

function removeField(e) {
    const fields = e.target.closest('.container');
    fields.remove();
}

function casinoProviderField(e, field) {
    const cnt = e.target.parentElement.parentElement,
    newField = `
    <div class="container">
        <div class="columns">
            <div class="column col-6 field provider_data">
                <input class="form-input" type="text" name="${field}key[]" required>
            </div>
            <div class="column col-6 field provider_data">
                <div class="input-group">
                    <input class="form-input" type="text" name="${field}value[]" required>  
                    <button class="btn input-group-btn field_remove">
                        Remove 
                    </button>
                </div>
            </div>
        </div>
    </div>`;
    addBtnCnt = cnt.querySelector('.add_provider_data_cnt');
    addBtnCnt.insertAdjacentHTML('beforebegin', newField);
    let removeFieldBtn = document.querySelectorAll('.field_remove');
    if (removeFieldBtn.length > 0) {
        removeFieldBtn.forEach( btn => {
            btn.addEventListener('click', removeField);
        });
    }
}