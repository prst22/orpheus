const removeModalTrigger = document.querySelectorAll('.delete_stream_btn');

if (removeModalTrigger.length > 0) {
    removeModalTrigger.forEach( btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const removePostModal = btn.parentElement.querySelector('.confirm_stream_delete_modal');
            removePostModal.classList.add('active');
        });
    });
}