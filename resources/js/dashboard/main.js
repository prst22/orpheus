require('../polyfills.js');
import lazySizes from 'lazysizes';
import jumper from 'jump.js-cancelable';
import debounce from 'lodash/debounce';

//lazysizes options
window.lazySizesConfig = window.lazySizesConfig || {};
lazySizesConfig.expand = 1000;
lazySizesConfig.expFactor = 3;
lazySizesConfig.loadMode = 2;
lazySizes.cfg.blurupMode = 'auto';

require('../svgxuse.js');
require('./dashboard_edit_categories.js');
require('./dashboard_remove_posts.js');
require('./dashboard_remove_streams.js');
require('./dashboard_remove_attachments.js');
require('./dashboard_edit_slots.js');
require('./dashboard_edit_jackpots.js');
require('./dashboard_edit_casinos.js');
require('../mobile_menu.js');
require('../toasts.js');

const goTop = document.querySelector('.go_top'),
setActiveForms = document.querySelectorAll('.set_active_form');

let handleDebouncedWheel = null;

goTop.addEventListener('click', () => {
    jumper.jump('html', {
        duration: 1200,
        offset: 0
    });
});

if (setActiveForms) {
    setActiveForms.forEach(form => {
        form.addEventListener('click', function(){
            this.submit()
        });
    });
}

function cancelJumper() {
    if (jumper.isJumping()) {
        jumper.cancel()
    }
}
//add events to prevet jump js on user scroll
document.addEventListener('DOMContentLoaded', () => {
    handleDebouncedWheel = debounce(cancelJumper, 30);
    window.addEventListener("wheel", handleDebouncedWheel, false);
    window.addEventListener("touchstart", cancelJumper, false);
});
// let arr = [];

// document.querySelectorAll(" input[name='providers[]']").forEach(el => {
//     arr.push(el.getAttribute("value"))
// })
// console.log(arr.sort())