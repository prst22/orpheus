const categoryEditmodalTrigger = document.querySelectorAll('.edit_categories .modal_trigger'),
categoryEditmodalCloseBtn = document.querySelectorAll('[href="#close"]'),
addNewCategoryBtn = document.querySelector('.add_category_cnt__btn'),
deleteCategoryBtn = document.querySelectorAll('.delete_category_btn');

if (categoryEditmodalTrigger.length > 0) {
    categoryEditmodalTrigger.forEach( btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const eidtCategoryModal = btn.parentElement.querySelector('.update_category_modal');
            eidtCategoryModal.classList.add('active');
        });
    });
}
if (categoryEditmodalCloseBtn.length > 0) {
    categoryEditmodalCloseBtn.forEach( btnClose => {
        btnClose.addEventListener('click', (e) => { 
            e.preventDefault();
            btnClose.closest('.modal').classList.remove('active');
        });
    });
}
if (addNewCategoryBtn) {
    addNewCategoryBtn.addEventListener('click', () => {
        addNewCategoryBtn.nextElementSibling.classList.add('active');
    });
}
if (deleteCategoryBtn.length > 0) {
    deleteCategoryBtn.forEach( btn =>{
        btn.addEventListener('click', (e) => { 
            const eidtCategoryModal = btn.parentElement.querySelector('.confirm_category_delete_modal');
            eidtCategoryModal.classList.add('active');
        });
    });
}
