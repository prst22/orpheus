const jackpotEditmodalTrigger = document.querySelectorAll('.jackpots .modal_trigger'),
slotEditmodalCloseBtn = document.querySelectorAll('[href="#close"]'),
deleteJackpotBtn = document.querySelectorAll('.delet_jackpot_btn');

if (jackpotEditmodalTrigger.length > 0) {
    jackpotEditmodalTrigger.forEach( btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const eidtSlotModal = btn.parentElement.querySelector('.update_jackpot_modal');
            eidtSlotModal.classList.add('active');
        });
    });
}
if (slotEditmodalCloseBtn.length > 0) {
    slotEditmodalCloseBtn.forEach( btnClose => {
        btnClose.addEventListener('click', (e) => { 
            e.preventDefault();
            btnClose.closest('.modal').classList.remove('active');
        });
    });
}
if (deleteJackpotBtn.length > 0) {
    deleteJackpotBtn.forEach( btn => {
        btn.addEventListener('click', (e) => { 
            const eidtSlotModal = btn.parentElement.querySelector('.confirm_jackpot_delete_modal');
            eidtSlotModal.classList.add('active');
        });
    });
}