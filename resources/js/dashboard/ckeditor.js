require('./ckeditor/build/ckeditor');

import CustomUploadAdapter from './ckeditor/CustomUploadAdapter.js';

function ImageUploadPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        // Configure the URL to the upload script in your back-end here!
        return new CustomUploadAdapter(loader);
    };
}
const customColorPalette = [
    {
        color: 'hsl(0, 0%, 0%)',
        label: 'Black'
    },
    {
        color: 'hsl(0, 0%, 30%)',
        label: 'Dim grey'
    },
    {
        color: 'hsl(0, 0%, 60%)',
        label: 'Grey'
    },
    {
        color: 'hsl(0, 0%, 90%)',
        label: 'Light grey'
    },
    {
        color: 'hsl(0, 0%, 100%)',
        label: 'White',
        hasBorder: true
    },
    {
        color: 'hsl(0, 75%, 60%)',
        label: 'Red'
    },
    {
        color: 'hsl(11, 50%, 42%)',
        label: 'Pale red'
    },
    {
        color: 'hsl(60, 75%, 60%)',
        label: 'Yellow'
    },
    {
        color: 'hsl(90, 75%, 60%)',
        label: 'Light green'
    },
    {
        color: 'hsl(120, 75%, 60%)',
        label: 'Green'
    },
    {
        color: 'hsl(136, 24%, 38%)',
        label: 'Pale green'
    },
    {
        color: 'hsl(180, 75%, 60%)',
        label: 'Turquoise'
    },
    {
        color: 'hsl(210, 75%, 60%)',
        label: 'Light blue'
    },
    {
        color: 'hsl(240, 75%, 60%)',
        label: 'Blue'
    },
    {
        color: 'hsl(270, 75%, 60%)',
        label: 'Purple'
    }
];

let editor = ClassicEditor.create(document.querySelector( '#editor' ),
    {    
        image: {
			insert: {
				type: 'block'
			}
		},
        extraPlugins: [ ImageUploadPlugin ],
        mediaEmbed: {
            previewsInData: true,
            removeProviders: [ 'youtube', 'vimeo' ],
            providers: [
                {
                    name: 'custom_youtube',
                    url: [/^(?:m\.)?youtube\.com\/watch\?v=([\w-]+)/,/^(?:m\.)?youtube\.com\/v\/([\w-]+)/,/^youtube\.com\/embed\/([\w-]+)/,/^youtu\.be\/([\w-]+)/],
                    html: t => {
                        return'<div style="position: relative; padding-bottom: 100%; height: 0; padding-bottom: 56.2493%;">'+
                        `<iframe data-src="https://www.youtube.com/embed/${t[1]}?color=white" class="lazyload" `+
                        'style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>'
                    }
                },
                {
                    name: 'custom_vimeo',
                    url: [/^vimeo\.com\/(\d+)/,/^vimeo\.com\/[^/]+\/[^/]+\/video\/(\d+)/,/^vimeo\.com\/album\/[^/]+\/video\/(\d+)/,/^vimeo\.com\/channels\/[^/]+\/(\d+)/,/^vimeo\.com\/groups\/[^/]+\/videos\/(\d+)/,/^vimeo\.com\/ondemand\/[^/]+\/(\d+)/,/^player\.vimeo\.com\/video\/(\d+)/],
                    html: t => {
                        return'<div style="position: relative; padding-bottom: 100%; height: 0; padding-bottom: 56.2493%;">'+`<iframe data-src="https://player.vimeo.com/video/${t[1]}" class="lazyload" `+
                        'style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'
                    }
                },
                {
                    name: 'twitter',
                    url: /^twitter\.com/
                },
                {
                    name: 'instagram',
                    url: /^instagram\.com/
                },
                {
                    name: 'facebook',
                    url: /^facebook\.com/
                },
            ]
        },
        link: {
            // Automatically add target="_blank" and rel="noopener noreferrer" to all external links.
            addTargetToExternalLinks: true,
        },
        fontColor: {
            colors: customColorPalette
        },
        fontSize: {
            options: [
                'small',
                'default',
                'big',
            ]
        },
        table: {
            contentToolbar: [
                'tableColumn', 'tableRow', 'mergeTableCells',
                'tableProperties', 'tableCellProperties'
            ],

            // Set the palettes for tables.
            tableProperties: {
                borderColors: customColorPalette,
                backgroundColors: customColorPalette
            },

            // Set the palettes for table cells.
            tableCellProperties: {
                borderColors: customColorPalette,
                backgroundColors: customColorPalette
            }
        }
    })
    .catch( error => {
        console.error(error);
    });

