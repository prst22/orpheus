const removeModalTrigger = document.querySelectorAll('.remove_attachment_btn');

if (removeModalTrigger.length > 0) {
    removeModalTrigger.forEach( btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const removePostModal = btn.parentElement.parentElement.querySelector('.remove_attachment_modal');
            removePostModal.classList.add('active');
        });
    });
}