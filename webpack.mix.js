const mix = require('laravel-mix');
var LiveReloadPlugin = require('webpack-livereload-plugin');

mix.disableNotifications();

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});
mix.options({
    terser: {
      extractComments: false,
    }
  });
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/main.js', 'public/js')
    .js('resources/js/dashboard/ckeditor.js', 'public/js/dashboard')
    .js('resources/js/dashboard/main.js', 'public/js/dashboard')
    .js('resources/js/jackpots-tracker.js', 'public/js')
    .sass('resources/sass/main.scss', 'public/css').options({
        processCssUrls: false
    })
    .sass('resources/sass/dashboard.scss', 'public/css').options({
        processCssUrls: false
    });


if (mix.inProduction()) {
    mix.version();
}
