<?php

namespace App\Share;

class ShareButtons
{
    function __construct($url, $title = null){
        $this->url = urlencode($url);
        $this->title = urlencode($title);
    }

    public function facebook(){
        
        $share_fb_url = "http://www.facebook.com/share.php?u={$this->url}";

        $html = sprintf('<a href="%s" class="share_block__link" id="fb" aria-label="facebook share">
            <svg class="icon">
                <use xlink:href="%s/symbol-defs.svg#%s"></use>
            </svg>
        </a>', $share_fb_url, \URL::asset('assets'), __FUNCTION__);

        return $html;
    }

    public function twitter(){
        
        $share_tw_url = "https://twitter.com/intent/tweet?text={$this->title}&url={$this->url}";

        $html = sprintf('<a href="%s" class="share_block__link" id="tw" aria-label="twitter share">
            <svg class="icon">
                <use xlink:href="%s/symbol-defs.svg#%s"></use>
            </svg>
        </a>', $share_tw_url, \URL::asset('assets'), __FUNCTION__);

        return $html;
    }

    public function whatsapp(){
        
        $share_wa_url = "https://api.whatsapp.com/send?text={$this->title}%20{$this->url}";

        $html = sprintf('<a href="%s" class="share_block__link" id="wa" target="_blank" aria-label="whatsapp share">
            <svg class="icon">
                <use xlink:href="%s/symbol-defs.svg#%s"></use>
            </svg>
        </a>', $share_wa_url, \URL::asset('assets'), __FUNCTION__);

        return $html;
    }

    public function telegram(){

        $share_tg_url = "https://telegram.me/share/url?url={$this->url}&text={$this->title}";

        $html = sprintf('<a href="%s" class="share_block__link" id="tg" target="_blank" aria-label="telegram share">
            <svg class="icon">
                <use xlink:href="%s/symbol-defs.svg#%s"></use>
            </svg>
        </a>', $share_tg_url, \URL::asset('assets'), __FUNCTION__);

        return $html;
    }

    public function tumblr(){

        $share_tg_url = "https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl={$this->url}&posttype=link";

        $html = sprintf('<a class="share_block__link" target="_blank" href="%s" aria-label="tumblr">
            <svg class="icon">
                <use xlink:href="%s/symbol-defs.svg#%s"></use>
            </svg>
        </a>', $share_tg_url, \URL::asset('assets'), __FUNCTION__);

        return $html;
    }
}
