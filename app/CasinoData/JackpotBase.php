<?php

namespace App\CasinoData;

use App\Jackpot;
use Carbon\Carbon;

class JackpotBase
{
    private const DAYS_TO_STORE = 75;

    protected function getLastJackpotAmout(string $casino_id, string $slot_id): ?string
    {
        $jackpot = Jackpot::where([
                'casino_id'=> $casino_id, 
                'slot_id'=> $slot_id
            ])
            ->select('date_amount')
            ->first();
              
        if (!$jackpot) {
            return NULL;
        }

        $jackpot_amount_collection = collect($jackpot['date_amount'])->last();
        
        if (isset($jackpot_amount_collection['jack_data']['amount']) &&
            !empty($jackpot_amount_collection['jack_data']['amount'])) {
            return $jackpot_amount_collection['jack_data']['amount'];
        } else {
            return NULL;
        }
    }

    protected function updateJackpotsInDB(array $arr_with_jackpot, string $casino_id, string $slot_id)
    {   
        $jackpot = Jackpot::where([
                'casino_id'=> $casino_id, 
                'slot_id'=> $slot_id
            ])->first();    

        if ($jackpot->doesntExist()) {
            return false;
        }
        
        $date = Carbon::now()->format('d/m/Y');
        
        if ($jackpot->date_amount === null) {
            $jackpot->date_amount = [0 => ['date' => $date, 'jack_data' => $arr_with_jackpot]]; 
        } else { 
            $old_jackpots = $jackpot->date_amount; 
            $last_date = Carbon::createFromFormat('d/m/Y', end($old_jackpots)['date']);
            $last_key = array_key_last($old_jackpots);

            if (count($old_jackpots) > (self::DAYS_TO_STORE - 1)) {
                
                if (!isset(end($old_jackpots)['date'])) {
                    return false;
                }
                //check if last jackpot date equals to current date
                if (Carbon::createFromFormat('d/m/Y', $date)->eq($last_date)) {
                    $old_jackpots[$last_key] = ['date' => $date, 'jack_data' => $arr_with_jackpot];
                    //remove first element and reset array keys to 0 => value, 1 => value etc.
                    $jackpot->date_amount = $old_jackpots;
                } else {
                    $old_jackpots[] = ['date' => $date, 'jack_data' => $arr_with_jackpot];
                    //remove first element and reset array keys to 0 => value, 1 => value etc.
                    $jackpot->date_amount = array_values(array_slice($old_jackpots, 1, self::DAYS_TO_STORE, true));
                }
                
            } else {

                if (!isset(end($old_jackpots)['date'])) {
                    return false;
                }
                //check if last jackpot date equals to current date
                if (Carbon::createFromFormat('d/m/Y', $date)->eq($last_date)) {
                    $old_jackpots[$last_key] = ['date' => $date, 'jack_data' => $arr_with_jackpot];
                } else {
                    $old_jackpots[] = ['date' => $date, 'jack_data' => $arr_with_jackpot];
                }
                
                $jackpot->date_amount = $old_jackpots;
            } 
        }

        $jackpot->save();
    }
}