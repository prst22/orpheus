<?php

namespace App\CasinoData\Casinos;

use App\CasinoData\CasinoBasedJackpotAbstract;
use App\Casino;
use Illuminate\Support\Facades\Http;

class BlueprintBet365 extends CasinoBasedJackpotAbstract
{
    private string $currency;
    private string $url;
    private string $casino_name;
    protected string $provider;

    function __construct()
    {
        $this->currency = 'EUR';
        $this->url = "https://games.bet365.com/api/Jackpots/PollDetailed?currency={$this->currency}";
        $this->casino_name = 'bet365';
        $this->casino = Casino::tracked()->where('name', $this->casino_name)->first();
        $this->provider = 'Blueprint';
        if ($this->casino === null) {
            return;
        }
        $this->casino_id = $this->casino->id;
    }

    public function init()
    {
        if ($this->casino === null) {
            return;
        }
        $this->makeApiRequest();  
    }

    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_id); 
        $this->sendJackpotRequest($slots);
    }

    private function sendJackpotRequest(array $slots = null)
    {
        if (empty($slots)) {
            return false;
        }

        try {
            $response = Http::retry(2, 55)->get($this->url);
        } catch (\Throwable $th) {
            return false;
        }
       
        if ($response->status() !== 200) {
            return false;
        }
        $jackpots_obj = $response->body();
        
        ['JackpotData' => $response_arr, 'Success' => $success] = json_decode($jackpots_obj, true);

        if ($success === false) {
            return false;
        } 

        if (empty($response_arr)) {
            return false;
        }  

        $this->createJackpotsArray($response_arr); 
    }

    private function createJackpotsArray(array $response_arr)
    {
        $slot_jackpots_collection = collect($response_arr);
        $slots_ids = [5546, 5547, 55501, 5554, 5562, 5564, 5565];
        
        $jackpot_data = null;
        $i = 0;
       
        while ($jackpot_data === null && $i < count($slots_ids)) {
            $jackpot_data = $slot_jackpots_collection->firstWhere('GID', '===', $slots_ids[$i]);
            $i++;
        }

        if (empty($jackpot_data)) {
            return false;
        }

        $arr_with_jackpot['amount'] = isset($jackpot_data['I']) ? strval((int) ($jackpot_data['I'] * 100)) : null;
        
        Casino::tracked()
            ->find($this->casino_id)
            ->activeSlots()
            ->where([
                        'provider' => $this->provider
                    ])
            ->get()->each(function($blueprint_slot) use ($arr_with_jackpot) {
                    $blueprint_slot
                        ->jackpots()
                        ->each(function($blueprint_jackpot) use ($arr_with_jackpot) {
                            $this->updateJackpotsInDB($arr_with_jackpot, $this->casino_id, $blueprint_jackpot->slot_id); 
                        });
                });
    }
}