<?php

namespace App\CasinoData\Casinos;

use App\CasinoData\CasinoBasedJackpotAbstract;
use App\Casino;
use Illuminate\Support\Facades\Http;

class BlueprintBetsson extends CasinoBasedJackpotAbstract
{
    private string $url;
    private string $casino_name;
    protected string $provider;

    function __construct()
    {
        // $this->url = 'https://www.betsson.com/api/v1/jackpots';
        $this->url = 'https://www.supercasino.com/api/v1/jackpots';
        $this->casino_name = 'Betsson';
        $this->casino = Casino::tracked()->where('name', $this->casino_name)->first();
        $this->provider = 'Blueprint';
        if ($this->casino === null) {
            return;
        }
        $this->casino_id = $this->casino->id;
    }

    public function init()
    {   
        if ($this->casino === null) {
            return;
        }
        $this->makeApiRequest();  
    }

    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_id); 
        $this->sendJackpotRequest($slots);
    }

    private function sendJackpotRequest(array $slots = null)
    {
        if (empty($slots)) {
            return false;
        }

        // $response = Http::withHeaders([
        //     'x-obg-channel' => 'Web',
        //     'x-obg-device'  => 'Desktop',
        //     'brandid'       => 'e123be9a-fe1e-49d0-9200-6afcf20649af',
        //     'marketcode'    => 'en',
        // ])->retry(2, 55)->get($this->url);
        try {
            $response = Http::withHeaders([
                'x-obg-channel' => 'Web',
                'x-obg-device'  => 'Desktop',
                'brandid'       => '46a7291a-caf4-471d-ab8b-40605b1d5865',
                'marketcode'    => 'en',
            ])->retry(2, 55)->get($this->url); 
        } catch (\Throwable $th) {
            return false;
        }
       

        if ($response->status() !== 200) {
            return false;
        }

        $jackpots_obj = $response->body();

        $response_arr = json_decode($jackpots_obj, true);
        
        if (empty($response_arr)) {
            return false;
        }  

        $this->createJackpotsArray($response_arr); 
    }

    private function createJackpotsArray(array $response_arr)
    {
        $slot_jackpots_collection = collect($response_arr);
        $slots_ids = [
                        'blueprintBuffaloRisingMegawaysJackpotKing',
                        'blueprintEyeOfHorusJackpotKing', 
                        'blueprintKingKongCashJackpotKing', 
                        'blueprint7sDeluxeJackpotKing', 
                        'blueprintTedJackpotKing', 
                        'blueprintTheGooniesJackpotKing', 
                        'blueprintTikiTreasuresMegawaysJackpotKing',
                        'blueprintTopCat'
                    ];
        $jackpot_data = null;
        $i = 0;
       
        while ($jackpot_data === null && $i < count($slots_ids)) {
            $jackpot_data = $slot_jackpots_collection->firstWhere('gameId', '===', $slots_ids[$i]);
            $i++;
        }
        
        if (empty($jackpot_data)) {
            return false;
        }

        if (empty($jackpot_data['jackpotValue'])) {
            return false;
        }
        
        $arr_with_jackpot['amount'] = isset($jackpot_data['jackpotValue']['value']) ? strval((int) ($jackpot_data['jackpotValue']['value'] * 100)) : null;

        Casino::tracked()
            ->find($this->casino_id)
            ->activeSlots()
            ->where([
                        'provider' => $this->provider
                    ])
            ->get()->each(function($blueprint_slot) use ($arr_with_jackpot) {
                    $blueprint_slot
                        ->jackpots()
                        ->each(function($blueprint_jackpot) use ($arr_with_jackpot) {
                            $this->updateJackpotsInDB($arr_with_jackpot, $this->casino_id, $blueprint_jackpot->slot_id); 
                        });
                });
    }
}