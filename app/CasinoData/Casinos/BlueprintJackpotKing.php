<?php

namespace App\CasinoData\Casinos;

use App\CasinoData\Casinos\BlueprintBet365;
use App\CasinoData\Casinos\BlueprintCasinoeuro;
use App\CasinoData\Casinos\BlueprintBetsson;

class BlueprintJackpotKing
{
    public function __invoke()
    {
        (new BlueprintBet365())->init();  
        (new BlueprintCasinoeuro())->init();  
        (new BlueprintBetsson())->init();  
    }
}