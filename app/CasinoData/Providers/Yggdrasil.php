<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Slot;

class Yggdrasil extends ProviderBasedJackpotAbstract
{
    private $currency;

    function __construct(private array $casino_ids)
    {
        $this->currency = 'EUR'; 
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jackpot_urls = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jackpot_urls);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_type_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];
        
        foreach ($slot_jackpot_type_arr as $slot_jackpot_type) {

            $yggdrasil = $casino->yggdrasil_casino_data;  
            
            if (empty($yggdrasil['subdomain']) || empty($yggdrasil['domain']) || empty($yggdrasil['path']) || empty($yggdrasil['org']) || empty($game_id)) {
                continue;
            }

            switch ($game_id) {
                case '7373':
                    $jp_what = 'jackpotInfo';
                    break;
                default:
                    $jp_what = 'jackpot';
            }
          
            $url = "https://{$yggdrasil['subdomain']}.{$yggdrasil['domain']}/{$yggdrasil['path']}?fn=info&org={$yggdrasil['org']}&gameid={$game_id}&currency={$this->currency}&what={$jp_what}";               
            $api_url_arr[$slot_jackpot_type] = $url;
        }
        
        return $api_url_arr;
    }

    private function sendJackpotRequest(array $jackpot_url_arr)
    {
        if (empty($jackpot_url_arr)) {
            return false;
        }

        foreach ($jackpot_url_arr as $slot => $jackpots_arr) {

            if (empty($jackpots_arr)) {
                continue;
            }

            foreach ($jackpots_arr as $key => $url) {
                $response = Http::retry(2, 55)->get($url);
                if ($response->status() === 200) {
                    $jackpot_obj = $response->body();
                    $jackpots_arr = json_decode($jackpot_obj, true);
                    $this->createJackpotsArray($jackpots_arr, $key, $slot); 
                }
            }
            
        }   
    }

    private function createJackpotsArray(array $jackpots_array, string $jackpot_id, string $casino_id_slot_id)
    {
        [$casino_id, $slot_id] = explode("_", $casino_id_slot_id);
        $slot_game_id = Slot::find($slot_id)->game_id;

        switch ($slot_game_id) {
            case '7361':

                if (!isset($jackpots_array['data']['jackpot']['jackpotStatus'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot']['jackpotStatus'];
                }

                break;
            case '7317':

                if (!isset($jackpots_array['data']['jackpot'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot'];
                }
               
                break;
            case '7324':

                if (!isset($jackpots_array['data']['jackpot'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot'];
                }

                break;
            case '7345':

                if (!isset($jackpots_array['data']['jackpot'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot'];
                }
               
                break;
            case '7312':

                if (!isset($jackpots_array['data']['jackpot'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot'];
                }
              
                break;
            case '7360':

                if (!isset($jackpots_array['data']['jackpot']['jackpotInfo'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot']['jackpotInfo'];
                }

                break;
            case '7373':

                if (!isset($jackpots_array['data']['jackpotInfo']['jackpotStatus'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpotInfo']['jackpotStatus'];
                }

                break;
            case '7398':

                if (!isset($jackpots_array['data']['jackpot']['jackpotStatus'])) {
                    $jackpots_arr = [];
                } else {
                    $jackpots_arr = $jackpots_array['data']['jackpot']['jackpotStatus'];
                }

                break;
            default:
                $jackpots_arr = [];
        }
        
        // update jackpot value or set it to previous value if an error occurred
        if (!empty($jackpots_arr[$jackpot_id])) {
            $arr_with_jackpots['amount'] = strval($jackpots_arr[$jackpot_id] * 100);
            $this->updateJackpotsInDB($arr_with_jackpots, $casino_id, $slot_id); 
        } else {
            $last_valid_jackpot_value = $this->getLastJackpotAmout($casino_id, $slot_id);

            if (!empty($last_valid_jackpot_value)) {
                $arr_with_jackpots['amount'] = $last_valid_jackpot_value;
                $this->updateJackpotsInDB($arr_with_jackpots, $casino_id, $slot_id); 
            }
        }

    }
    
}