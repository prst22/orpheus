<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Slot;
use App\Casino;
use App\Jackpot;
use Carbon\Carbon;

class RedTiger extends ProviderBasedJackpotAbstract
{
    private $currency;

    function __construct(private array $casino_ids)
    {
        $this->currency = 'EUR'; 
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jackpot_url_arr = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jackpot_url_arr);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_types_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];

        foreach ($slot_jackpot_types_arr as $slot_jackpot_type) {

            $redtiger = $casino->redtiger_casino_data;  
            
            $redtiger['query'] ??= '';
            $redtiger['path'] ??= '';
            $and = $redtiger['query'] ? '&' : '';
            
            if (!isset($redtiger['subdomain']) || !isset($redtiger['domain'])) {
                return $api_url_arr[$slot_jackpot_type] = '';
            }

            $url = "https://{$redtiger['subdomain']}.{$redtiger['domain']}/jackpots/{$redtiger['path']}?{$redtiger['query']}{$and}currency={$this->currency}"; 
                          
            $api_url_arr[$slot_jackpot_type] = $url;
        }

        return $api_url_arr;
    }

    private function sendJackpotRequest(array $jackpot_url_arr)
    {
        //$jackpot_url_arr array of one jackpot type url for each casino, because all jackpots of a specific type in certain casino are the same
        if (empty($jackpot_url_arr)) {
            return false;
        }

        foreach ($jackpot_url_arr as $casino_id => $jackpots_arr) {

            if (empty($jackpots_arr)) {
                continue;
            }
            
            foreach ($jackpots_arr as $key => $url) {
            
                $response = Http::retry(2, 55)->get($url);
                if ($response->status() === 200) {
                    $jackpot_obj = $response->body();
                    $response_arr = json_decode($jackpot_obj, true);

                    if ($response_arr['success'] !== true) {
                        continue;
                    }
                    if (!isset($response_arr['result'])) {
                        continue;
                    }
                    if (!isset($response_arr['result']['jackpots'])) {
                        continue;
                    }
                    if (!isset($response_arr['result']['jackpots']['pots'])) {
                        continue;    
                    }
                    if (empty($response_arr['result']['jackpots']['pots'])) {
                        continue;
                    }
                    
                    //create jackpots array for DB, all jackpots of one type are similar for all slots of a specific casino
                    $this->createJackpotsArray($response_arr, $key, $casino_id); 
                }

            }
            
        }   
    }

    private function createJackpotsArray(array $response_arr, string $jackpot_type, int $casino_id)
    {
        if ($jackpot_type !== 'progressive') {
            return false;
        } 
        
        $arr_with_jackpot = [];
        $response_jackpots = $response_arr['result']['jackpots']['pots'];

        foreach ($response_jackpots as $jp) {

            $jp_collection = collect($jp);

            if ($jp_collection->contains($jackpot_type)) {

                $arr_with_jackpot['amount'] = $jp_collection->isNotEmpty() ? strval((int) ($jp_collection->all()['amount'] * 100)) : null;
                //loop through casino->slots->jackpots collection to update one jackpot at a time in DB
                Casino::tracked()->find($casino_id)
                    ->activeSlots()
                    ->where('provider', $this->getProviderName())
                    ->each(function($slot, $key) use ($casino_id, $jackpot_type, $arr_with_jackpot){

                        $slot->jackpotsInCasino($casino_id)
                            ->where('type', $jackpot_type)
                            ->each(function($jackpot) use ($arr_with_jackpot, $casino_id){

                                $this->updateJackpotsInDB($arr_with_jackpot, $casino_id, $jackpot->slot_id); 

                            });

                    });
            }

        } 
  
    }
}