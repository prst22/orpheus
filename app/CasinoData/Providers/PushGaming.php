<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Slot;
use App\Casino;
use App\Jackpot;
use Carbon\Carbon;

class PushGaming extends ProviderBasedJackpotAbstract
{
    private $currency;

    function __construct(private array $casino_ids)
    {
        $this->currency = 'EUR';   
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jackpot_url_arr = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jackpot_url_arr);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_types_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];
        
        foreach ($slot_jackpot_types_arr as $slot_jackpot_type) {
            $pushgaming = $casino->pushgaming_casino_data;  
            if (!isset($pushgaming['domain']) || empty($pushgaming['domain'])) {
                continue;
            }
            $url = "https://{$pushgaming['domain']}/hive/b2c/game/{$game_id}/api/jackpot/instances?instanceId0={$pushgaming['instanceId0']}&instanceId1={$pushgaming['instanceId1']}";     
            $api_url_arr[$slot_jackpot_type] = $url;
        }

        return $api_url_arr;
    }

    private function sendJackpotRequest(array $jackpot_url_arr)
    {
        if (empty($jackpot_url_arr)) {
            return false;
        }

        foreach ($jackpot_url_arr as $casino_id_slot_id => $jp_type_url) {
            
            if (empty($jp_type_url)) {
                continue;
            }
            
            $sessionId = $this->getSessionId($casino_id_slot_id);

            foreach ($jp_type_url as $key => $url) {
                $response = Http::retry(2, 55)->withHeaders([
                    'authorization' => "Bearer {$sessionId}",
                ])->get($url);

                if ($response->status() === 200) {
                    $jackpot_obj = $response->body();
                    $jackpots_arr = json_decode($jackpot_obj, true);
                    $this->createJackpotsArray($jackpots_arr, $key, $casino_id_slot_id); 
                }
            }
            
        } 
    }

    private function createJackpotsArray(array $response_arr, string $jackpot_type, string $casino_id_slot_id)
    {
        [$casino_id, $slot_id] = explode("_", $casino_id_slot_id);
        
        if (!isset($response_arr['instances'])) {
            $jackpots_arr = [];
        } else {
            $jackpots_arr = $response_arr['instances'];
        }
        $jackpot = collect($jackpots_arr)->firstWhere('type', '===', $jackpot_type);
        if (!empty($jackpot) && $jackpot !== null) {
            $arr_with_jackpots['amount'] = strval($jackpot['amount']);
            $this->updateJackpotsInDB($arr_with_jackpots, $casino_id, $slot_id); 
        } 
    }

    private function getSessionId(string $casino_id_slot_id = ''): ?string
    {
        if ($casino_id_slot_id === '') {
            return null;
        }

        [$casino_id, $slot_id] = explode("_", $casino_id_slot_id);
        $casino = Casino::tracked()->find($casino_id);
        $pushgaming = $casino->pushgaming_casino_data;

        $response = Http::retry(2, 55)->post("https://{$pushgaming['domain']}/hive/b2c/game/mountmagmas/api/sessions", [
            'type' => 'guestLogin',
            'igpCode' => $pushgaming['igpCode'],
            'gameCode' => 'mountmagmas',
            'currency' => $this->currency,
            'lang' => 'en',
            'mode' => 'demo'
        ]);
        ['sessionId' => $sessionId] = $response->json();

        if (!$response->successful()) {
            return null;
        }
        if (!isset($sessionId)) {
            return null;
        }
        
        return $sessionId;
    }
}