<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Jackpot;
use Illuminate\Support\Facades\Log;

class Netent extends ProviderBasedJackpotAbstract
{
    private $currency;
    private $format;
    private $operation;

    function __construct(private array $casino_ids)
    {
        $this->currency = 'EUR'; 
        $this->format = 'parameters'; 
        $this->operation = 'jackpot'; 
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jacpot_urls = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jacpot_urls);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_type_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];

        foreach ($slot_jackpot_type_arr as $slot_jackpot_type) {
            $netent = $casino->netent_casino_data;
            if (empty($netent['subdomain']) || empty($netent['domain']) || empty($netent['path']) || empty($game_id)) {
                continue;
            }                   
            $url = "https://{$netent['subdomain']}.{$netent['domain']}/{$netent['path']}?gameId={$game_id}&wantsreels=true&operation={$this->operation}&jpid={$slot_jackpot_type}&jpc={$this->currency}&format={$this->format}";
            $api_url_arr[$slot_jackpot_type] = $url;
        }
        
        return $api_url_arr;
    }

    private function sendJackpotRequest(array $jackpot_url_arr)
    {
        if (empty($jackpot_url_arr)) {
            return false;
        }

        foreach ($jackpot_url_arr as $slot => $jackpots_arr) {

            if (empty($jackpots_arr)) {
                continue;
            }

            foreach ($jackpots_arr as $key => $url) {
                try {
                    $response = Http::retry(2, 55)->get($url);
                    if ($response->status() === 200) {
                        $parametrs_str = $response->body();
                        $this->fromQueryStringToArray($parametrs_str, $key, $slot); 
                    }
                } catch (\Throwable $th) {
                    
                    Log::info("Netent->sendJackpotRequest $url");
                }
                
            }
        }   
    }

    private function fromQueryStringToArray(string $string_with_values, string $jpid, string $casino_slot_ids)
    {
        $data_arr = explode('&', $string_with_values);
        $collection = collect($data_arr);
        $casino_id = explode("_", $casino_slot_ids)[0];
        $slot_id = explode("_", $casino_slot_ids)[1];
        $findme = "{$this->operation}.{$jpid}.{$this->currency}.amount=";
        $arr_with_jackpot = [];

        $collection->filter(function ($value, $key) use ($jpid, $findme, &$arr_with_jackpot) {
            $match = strpos($value, $findme);

            if ($match !== false) {
                $jackpot_amount_in_cents = substr($value, strlen($findme), strlen($value));
                $arr_with_jackpot['amount'] = $jackpot_amount_in_cents;
                return $value;
            } 
        });

        $this->updateJackpotsInDB($arr_with_jackpot, $casino_id, $slot_id); 
    }

}