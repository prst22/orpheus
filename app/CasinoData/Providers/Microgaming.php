<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Slot;
use App\Casino;
use App\Jackpot;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Microgaming extends ProviderBasedJackpotAbstract
{
    private $currency;

    function __construct(array $casino_ids)
    {
        $this->currency = 'EUR'; 
        $this->casino_ids = $casino_ids; 
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jackpot_url_arr = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jackpot_url_arr);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_types_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];
        
        if (!isset($slot_jackpot_types_arr[0])) {
            return $api_url_arr;
        }
        // Str::replaceFirst('@MG', '', $slot_jackpot_types_arr[0]); 
        $slot_jackpot_type = Str::endsWith($slot_jackpot_types_arr[0], '@MG') ?  $slot_jackpot_types_arr[0] : $slot_jackpot_types_arr[0] . '@MG';
            
        // $api_url_arr[$slot_jackpot_type] = 'https://www.microgaming.co.uk/external/api/progressive/GetCounters?currencyCode=EUR';
        $api_url_arr[$slot_jackpot_type] = 'https://www.unibet.com/feeder-rest-api/jackpot/jackpotGames.json?jurisdiction=MT&brand=unibet&deviceGroup=desktop&currency=EUR&application=polopoly&nrOfRows=120&startIndex=0&locale=en_GB';
        
        return $api_url_arr;
    }

    private function sendJackpotRequest(array &$jackpot_url_arr)
    {
        
        //$jackpot_url_arr array of one jackpot type url for each casino, because all jackpots of a specific type in certain casino are the same
        if (empty($jackpot_url_arr)) {
            return false;
        }
       
        $response = Http::retry(2, 55)->get(array_values(array_values($jackpot_url_arr)[0])[0]);
        
        if ($response->status() !== 200) {
            return false;
        }

        $jackpots_obj = $response->body();
        
        ['items' => $response_arr] = json_decode($jackpots_obj, true);
        
        if (empty($response_arr)) {
            return false;
        }
        
        // $response_arr = array_unique($response_arr, SORT_REGULAR);
        
        foreach ($jackpot_url_arr as $casino_id_slot_id => $jackpots_arr) {

            if (empty($jackpots_arr)) {
                continue;
            }
            
            foreach ($jackpots_arr as $key => $url) {
                
                $this->createJackpotsArray($response_arr, $key, $casino_id_slot_id); 
            }

        }   
    }

    private function createJackpotsArray(array $response_arr, string $jackpot_type, string $casino_id_slot_id)
    {
        [$casino_id, $slot_id] = explode("_", $casino_id_slot_id);
        $slot_jackpots_collection = collect($response_arr);
        
        $jackpot_data = $slot_jackpots_collection->firstWhere('jackpotId', '===', $jackpot_type);
        
        if (empty($jackpot_data)) {
            return false;
        }
    
        $arr_with_jackpot['amount'] = isset($jackpot_data['money']['amount']) ? strval(($jackpot_data['money']['amount'] * 100)) : null;
        // dd($jackpot_data['money']['amount'], $arr_with_jackpot, $jackpot_type);
        Casino::tracked()->find($casino_id)
            ->activeSlots()
            ->where([
                        'provider' => $this->getProviderName(), 
                        'slots.id' => $slot_id
                    ])
            ->first()
            ->jackpots()
            ->each(function($jackpot) use ($casino_id, $arr_with_jackpot) {
                $this->updateJackpotsInDB($arr_with_jackpot, $casino_id, $jackpot->slot_id); 
            });
    }
}