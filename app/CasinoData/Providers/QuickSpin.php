<?php

namespace App\CasinoData\Providers;

use App\CasinoData\ProviderBasedJackpotAbstract;
use Illuminate\Support\Facades\Http;
use App\Slot;
use App\Casino;
use App\Jackpot;
use Carbon\Carbon;

class QuickSpin extends ProviderBasedJackpotAbstract
{
    private $currency;

    function __construct(private array $casino_ids)
    {
        $this->currency = 'EUR';   
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
    }
    
    protected function makeApiRequest()
    {
        $slots = $this->getSlotsArr($this->casino_ids, $this->getProviderName());
        $jackpot_url_arr = $this->getCasinoData($this->getProviderName(), $slots);
        $this->sendJackpotRequest($jackpot_url_arr);
    }

    protected function buildProviderApiUrlArray(array $slot_jackpot_types_arr, object $casino, string $game_id = ''): array
    {
        $api_url_arr = [];
        
        foreach ($slot_jackpot_types_arr as $slot_jackpot_type) {
            $quickspin = $casino->quickspin_casino_data;  
            if ((!isset($quickspin['subdomain']) || empty($quickspin['subdomain'])) ||
                (!isset($quickspin['domain']) || empty($quickspin['domain']))) {
                continue;
            }
            $url = "https://{$quickspin['subdomain']}.{$quickspin['domain']}/game/getjackpot/";    
            $api_url_arr[$slot_jackpot_type] = $url;
        }

        return $api_url_arr;
    }

    private function sendJackpotRequest(array $jackpot_url_arr)
    {
        if (empty($jackpot_url_arr)) {
            return false;
        }
        
        foreach ($jackpot_url_arr as $casino_id_slot_id_game_id => $jp_type_url) {

            if (empty($jp_type_url)) {
                continue;
            }

            [$casino_id, $slot_id, $game_id] = explode("_", $casino_id_slot_id_game_id);
            $sessionId = $this->getSessionId($casino_id, $game_id);
            
            foreach ($jp_type_url as $key => $url) {
                $response = Http::asForm()->retry(2, 55)->withOptions(['verify' => false])->post($url, [
                    'sid' => $sessionId,
                    'g' => $game_id
                ]);

                if ($response->status() === 200) {
                    $jackpot_obj = $response->body();
                    $jackpots_arr = json_decode($jackpot_obj, true);

                    $this->createJackpotsArray($jackpots_arr, $key, $casino_id, $slot_id); 
                }
            }
            
        } 
    }

    private function createJackpotsArray(array $response_arr, string $jackpot_type, string $casino_id, string $slot_id)
    {
        if (!isset($response_arr['jackpots']) && !isset($response_arr['jackpots'][$jackpot_type])) {
            $jackpots_arr = [];
        } else {
            $jackpots_arr = $response_arr['jackpots'][$jackpot_type];
        }

        if (!empty($jackpots_arr) && isset($jackpots_arr['amount'])) {
            $jp_array = ['amount' => strval($jackpots_arr['amount'])];
            $this->updateJackpotsInDB($jp_array, $casino_id, $slot_id); 
        } 
    }

    private function getSessionId(string $casino_id = '', string $game_id = ''): ?string
    {
        if ($casino_id === '' || $game_id === '') {
            return null;
        }

        $casino = Casino::tracked()->find($casino_id);
        $quickspin = $casino->quickspin_casino_data;
          
        $response = Http::asForm()->retry(2, 55)->withOptions(['verify' => false])->post("https://{$quickspin['subdomain']}.{$quickspin['domain']}/game/demormlogin/", [
            'g' => $game_id,
            'partnerid' => $quickspin['partnerid'],
            'channel' => "web"
        ]);

        ['sid' => $sessionId] = $response->json();

        if (!$response->successful()) {
            return null;
        }
        if (!isset($sessionId)) {
            return null;
        }
        
        return $sessionId;
    }
}