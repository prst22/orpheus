<?php

namespace App\CasinoData;

use App\Casino;

abstract class CasinoBasedJackpotAbstract extends JackpotBase
{
    abstract protected function makeApiRequest();

    protected function getSlotsArr(): ?array
    {
        if (empty($this->casino_id)) {
            return null;
        }

        $all_casino_slots = [];

        $slots_col = Casino::tracked()->find($this->casino_id)->activeSlots()->where('provider', $this->provider)->get();
            
        if ($slots_col == null) {
            return $all_casino_slots;
        }

        $plucked = $slots_col->pluck('game_id', 'id');
        $slots = $plucked->all();
        
        if (!empty($slots)) {
            $all_casino_slots[$this->casino_id] = $slots;
        } 
        
        return $all_casino_slots;
    }
}