<?php

namespace App\CasinoData;

use Illuminate\Support\Str;

use App\Casino;
use App\Jackpot;

abstract class ProviderBasedJackpotAbstract extends JackpotBase
{
    abstract protected function makeApiRequest();
    
    abstract protected function buildProviderApiUrlArray(array $slot_jackpot_type_arr, object $provider_info, string $game_id = ''): array;

    protected function getCasinoData(string $provider = null, array $slots = null): ?array
    {
        if (empty($provider) || empty($slots)) {
            return null;
        }

        $api_url_arr = [];
        
        if ($provider === 'Netent') {

            foreach ($slots as $casino_id => $casino_slots) {
                
                $casino = Casino::tracked()->find($casino_id);

                foreach ($casino_slots as $slot_id => $game_id) {

                    $slot = $casino->activeSlots()->find($slot_id);
                    $slot_jackpot_plucked_arr = $slot->jackpotsInCasino($casino_id)->pluck('type');
                    $slot_jackpot_type_arr = $slot_jackpot_plucked_arr->all();
                    $arr_key = Str::slug("{$casino_id}-{$slot_id}", "_");
                    // method defined in App\CasinoData\Providers\Netent class
                    $api_url_arr[$arr_key] = $this->buildProviderApiUrlArray($slot_jackpot_type_arr, $casino, $game_id);
                    
                }

            }

        } elseif ($provider === 'Yggdrasil') {
            
            foreach ($slots as $casino_id => $casino_slots) {
                 
                $casino = Casino::tracked()->find($casino_id);

                foreach ($casino_slots as $slot_id => $game_id) {

                    $slot = $casino->activeSlots()->find($slot_id);
                    $slot_jackpot_plucked_arr = $slot->jackpotsInCasino($casino_id)->pluck('type');
                    $slot_jackpot_type_arr = $slot_jackpot_plucked_arr->all();
                    $arr_key = Str::slug("{$casino_id}-{$slot_id}", "_");
                    // method defined in App\CasinoData\Providers\Yggdrasil class
                    $api_url_arr[$arr_key] = $this->buildProviderApiUrlArray($slot_jackpot_type_arr, $casino, $game_id);
                
                }

            }

        } elseif ($provider === 'RedTiger') {
            
            foreach ($slots as $casino_id => $slot_ids) {
                 
                $casino = Casino::tracked()->find($casino_id);
                $all_slot_ids_in_casino = array_keys($slot_ids) ?? [];
                $all_jackpot_types_collection = Jackpot::where('casino_id', $casino_id)->whereIn('slot_id', $all_slot_ids_in_casino)->get()->pluck('type');
                $all_jackpot_types_arr = array_unique($all_jackpot_types_collection->all());
                $api_url_arr[$casino_id] = $this->buildProviderApiUrlArray($all_jackpot_types_arr, $casino);

            }
            
        } elseif ($provider === 'Microgaming') {
            
            foreach ($slots as $casino_id => $casino_slots) {
                
                $casino = Casino::tracked()->find($casino_id);
                
                if (!$casino) {
                    continue;
                }

                foreach ($casino_slots as $slot_id => $game_id) {
                    
                    $slot = $casino->activeSlots()->find($slot_id);
                    $slot_jackpot_plucked_arr = $slot->jackpotsInCasino($casino_id)->pluck('type');
                    $slot_jackpot_type_arr = $slot_jackpot_plucked_arr->all();
                    $arr_key = Str::slug("{$casino_id}-{$slot_id}", "_");
                    
                    $api_url_arr[$arr_key] = $this->buildProviderApiUrlArray($slot_jackpot_type_arr, $casino, $game_id ?? '');
                
                }
            }
            
        } elseif ($provider === 'PushGaming') {
            
            foreach ($slots as $casino_id => $slots) {
                 
                $casino = Casino::tracked()->find($casino_id);

                foreach ($slots as $slot_id => $game_id) {
                    $slot = $casino->activeSlots()->find($slot_id);
                    $slot_jackpot_plucked_arr = $slot->jackpotsInCasino($casino_id)->pluck('type');
                    $slot_jackpot_type_arr = $slot_jackpot_plucked_arr->all();
                    $arr_key = Str::slug("{$casino_id}-{$slot_id}", "_");
                    // method defined in App\CasinoData\Providers\PushGaming class
                    $jp_type_api_url = $this->buildProviderApiUrlArray($slot_jackpot_type_arr, $casino, $game_id);
                    if (empty($jp_type_api_url)) {
                        continue;
                    }
                    $api_url_arr[$arr_key] = $jp_type_api_url;
                }

            }

        } elseif ($provider === 'QuickSpin') {
            
            foreach ($slots as $casino_id => $slot) {
                
                $casino = Casino::tracked()->find($casino_id);
              
                foreach ($slot as $slot_id => $game_id) {
                    $slot_obj = $casino->activeSlots()->find($slot_id);
                    $slot_jackpot_plucked_arr = $slot_obj->jackpotsInCasino($casino_id)->pluck('type');
                    $slot_jackpot_type_arr = $slot_jackpot_plucked_arr->all();
                    $arr_key = Str::slug("{$casino_id}-{$slot_id}-{$game_id}", "_");
                    // method defined in App\CasinoData\Providers\QuickSpin class
                    $jp_type_api_url = $this->buildProviderApiUrlArray($slot_jackpot_type_arr, $casino, $game_id);
                    if (empty($jp_type_api_url)) {
                        continue;
                    }
                    $api_url_arr[$arr_key] = $jp_type_api_url;
                }

            }
            
        }

        return $api_url_arr;
    }
    //get all slots of all casinos as an array 
    protected function getSlotsArr(array $casino_id_arr = [], string $provider = null): ?array
    {
        if (empty($casino_id_arr)) {
            return null;
        }

        $all_casino_slots = [];

        foreach ($casino_id_arr as $casino_id) {
            $slots_col = Casino::tracked()->find($casino_id)->activeSlots()->where('provider', $provider)->get();
             
            if ($slots_col == null) {
                return $all_casino_slots;
            }

            $plucked = $slots_col->pluck('game_id', 'id');
            $slots = $plucked->all();
            
            if (!empty($slots)) {
                $all_casino_slots[$casino_id] = $slots;
            } 
        }
   
        return $all_casino_slots;
    }
    
    protected function getProviderName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}