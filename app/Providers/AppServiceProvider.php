<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Routing\UrlGenerator;

use App\Stream;
use App\Category;
use App\User;
use App\Http\Traits\Stream\CrateTwitchRequestUrl;
use App\Stream\FetchStream;
use App\Facades\FetchAllStreams;
use App\Facades\FetchCategoryStreams;
use App\Facades\FetchFavouriteStreams;
use Request;

class AppServiceProvider extends ServiceProvider
{
    use CrateTwitchRequestUrl;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (\App::environment('production')) {
            $url->forceScheme('https');
        }
        // custom pagination for search page collection SearchController->search()
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

            return new LengthAwarePaginator(
                $this->forPage($page, $perPage)->values(),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });

        Paginator::defaultView('pagination.custom');

        $twitch_url = config('services.twitch.fetch_streams_url');

        $this->app->singleton(FetchAllStreams::class, function($app) use ($twitch_url) {

            $streams = Stream::select('channel_id')->get();
            $requestUrlArr = $this->crateTwitchRequestUrl($twitch_url, $streams);
            
            return new FetchStream($requestUrlArr);

        });
        // create twich api request url based on category
        $this->app->singleton(FetchCategoryStreams::class, function($app) use ($twitch_url) {

            $slug = Request::route('slug');
            $category = Category::where('slug', $slug)->firstOrFail();
            $sorted_streams = Category::findOrFail($category->id)->streams()->select('channel_id')->get();
            $requestUrlArr = $this->crateTwitchRequestUrl($twitch_url, $sorted_streams);
            
            return new FetchStream($requestUrlArr);
            
        });
        // create twich api request url based on user favorite streams
        $this->app->singleton(FetchFavouriteStreams::class, function($app) use ($twitch_url) {

            $user = User::find(auth()->user()->id);
            $favourite_stream_ids = $user->favourite_streams()->pluck('stream_id');
            $channel_ids = Stream::select('channel_id')->whereIn('id', $favourite_stream_ids)->get();
            $requestUrlArr = $this->crateTwitchRequestUrl($twitch_url, $channel_ids);

            return new FetchStream($requestUrlArr);
            
        });
    }
}
