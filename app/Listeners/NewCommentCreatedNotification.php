<?php

namespace App\Listeners;

use App\Events\NewCommentCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewCommentNotification;
use App\Post;

class NewCommentCreatedNotification implements ShouldQueue
{

    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 100;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentCreated  $event
     * @return void
     */
    public function handle(NewCommentCreated $event)
    {
        $post_id = $event->comment->commentable_id;
        $post = Post::find($post_id);
        $comment_key = '#comment-' . $event->comment->getKey() ?? '';
        $route = route('posts.show', ['slug' => $post->slug]);
        $approved = $event->comment->approved;
        $comment_url = $approved ? url("{$route}{$comment_key}") : url($route);

        $messageContent = [ 
            'approved' => $approved ? '' : 'This comment needs to be approved',
            'comment_url' => $comment_url ?? '',
            'message_body' => $event->comment->comment ?? '',
        ];

        Mail::to(config('services.email.main'))->send(new NewCommentNotification($messageContent)); 
    }
}
