<?php

namespace App\Services;

class PageNumber 
{
    public function __construct (private \Illuminate\Pagination\LengthAwarePaginator $posts) {
        $this->posts = $posts;
    }

    public function get(): string
    {
        if ($this->posts === null) {
            return '';
        }

        $pageNumber = $this->posts->currentPage();
        $pageStr = $pageNumber !== null && $pageNumber !== 1 ? " - page {$pageNumber}" : '';
        
        return $pageStr;
        
    }
}