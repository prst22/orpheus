<?php

namespace App\Services;

class CountLiveStreams
{
    private $streams;

    public function __construct(array $streams = [])
    {
        $this->streams = $streams;
        $this->total = $this->total();
        $this->poker = $this->poker();
        $this->casino = $this->casino();
    }

    private function total(): int
    {
        return count($this->streams) ?? 0;
    }

    private function poker(): int
    {
        $streams_collection = \collect($this->streams);

        if ($streams_collection->isEmpty()) {
            return 0;
        }

        $poker_streams_collection = $streams_collection->whereIn('game', 'poker');
        
        if ($poker_streams_collection->isEmpty()) {
            return 0;
        }

        return $poker_streams_collection->count();
    }

    private function casino(): int
    {
        $streams_collection = \collect($this->streams);

        if ($streams_collection->isEmpty()) {
            return 0;
        }

        $casino_streams = $this->total - $this->poker;

        return $casino_streams ?? 0;
    }
    
}