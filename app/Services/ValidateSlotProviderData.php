<?php

namespace App\Services;

class ValidateSlotProviderData
{
    private ?array $netent_casino_data = NULL;
    private ?array $yggdrasil_casino_data = NULL;
    private ?array $redtiger_casino_data = NULL;
    private ?array $pushgaming_casino_data = NULL;
    private ?array $quickspin_casino_data = NULL;

    function __construct(array $providers_data) 
    {
        $this->providers_data = $providers_data;
    }

    public function Netent(): ?array 
    {
        if(!empty($this->providers_data['netentkey']) && !empty($this->providers_data['netentvalue'])){    
            ['netentkey' => $netentkey, 'netentvalue' => $netentvalue] = $this->providers_data;

            foreach ($netentkey as $key => $value) {
                $this->netent_casino_data[$value] = $netentvalue[$key];
            }
        }
        return $this->netent_casino_data;
    }

    public function Yggdrasil(): ?array 
    {
        if(!empty($this->providers_data['yggdrasilkey']) && !empty($this->providers_data['yggdrasilvalue'])){
            ['yggdrasilkey' => $yggdrasilkey, 'yggdrasilvalue' => $yggdrasilvalue] = $this->providers_data;
            
            foreach ($yggdrasilkey as $key => $value) {
                $this->yggdrasil_casino_data[$value] = $yggdrasilvalue[$key];
            }
        }
        return $this->yggdrasil_casino_data;
    }

    public function Redtiger(): ?array 
    {
        if(!empty($this->providers_data['redtigerkey']) && !empty($this->providers_data['redtigervalue'])){
            ['redtigerkey' => $redtigerkey, 'redtigervalue' => $redtigervalue] = $this->providers_data;
            
            foreach ($redtigerkey as $key => $value) {
                $this->redtiger_casino_data[$value] = $redtigervalue[$key];
            }
        }
        return $this->redtiger_casino_data;
    }

    public function Pushgaming(): ?array 
    {
        if(!empty($this->providers_data['pushgamingkey']) && !empty($this->providers_data['pushgamingvalue'])){
            ['pushgamingkey' => $pushgamingkey, 'pushgamingvalue' => $pushgamingvalue] = $this->providers_data;
            
            foreach ($pushgamingkey as $key => $value) {
                $this->pushgaming_casino_data[$value] = $pushgamingvalue[$key];
            }
        }
        return $this->pushgaming_casino_data;
    }

    public function Quickspin(): ?array 
    {
        if(!empty($this->providers_data['quickspinkey']) && !empty($this->providers_data['quickspinvalue'])){
            ['quickspinkey' => $quickspinkey, 'quickspinvalue' => $quickspinvalue] = $this->providers_data;
            
            foreach ($quickspinkey as $key => $value) {
                $this->quickspin_casino_data[$value] = $quickspinvalue[$key];
            }
        }
        return $this->quickspin_casino_data;
    }
}