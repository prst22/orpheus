<?php

namespace App\Services;

class SortedArticlesTitle
{
    public function __construct(private string $type, private string $ascendance)
    {
        $this->type = $type;
        $this->ascendance = $ascendance;
    }

    public function get(): string
    {
        if ($this->type === 'created_at' && $this->ascendance === 'desc') {
            return (\Route::currentRouteName() === 'posts.category' ||
             \Route::currentRouteName() === 'posts.category.sortby') ? __('category sorted by new') : __('New articles');
        }
        if ($this->type === 'created_at' && $this->ascendance === 'asc') {
            return (\Route::currentRouteName() === 'posts.category' ||
             \Route::currentRouteName() === 'posts.category.sortby') ? __('category sorted by old') : __('Old articles');
        }
        if ($this->type === 'views' && $this->ascendance === 'desc') {
            return (\Route::currentRouteName() === 'posts.category' ||
             \Route::currentRouteName() === 'posts.category.sortby') ? __('category sorted by popular') : __('Popular articles');
        }
        if ($this->type === 'views' && $this->ascendance === 'asc') {
            return (\Route::currentRouteName() === 'posts.category' ||
             \Route::currentRouteName() === 'posts.category.sortby') ? __('category sorted by least popular') : __('Least popular articles');
        }
        
        return '';
    }
}