<?php

namespace App\Services;

class JackpotAmount
{
    public static function get(\App\Jackpot $jackpot): ?\Illuminate\Support\Collection
    {
        if (!$jackpot) {
            return NULL;
        }

        $amount_collection = collect($jackpot->date_amount)->map(function ($value) {
            unset($value['date']);
            //cast jp amount from str to int
            if (isset($value['jack_data']['amount'])) {
                $value['jack_data']['amount'] = (int) $value['jack_data']['amount'];
            }

            return $value;
        });

        $amount_collection = $amount_collection->flatten()->filter()->values();

        return $amount_collection; 
    } 
}