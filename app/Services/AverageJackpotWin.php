<?php

namespace App\Services;

use App\Services\JackpotAmount;

class AverageJackpotWin
{
    public static function get(\App\Jackpot $jackpot): ?int
    {
        
        $jackpot_amount_collection = JackpotAmount::get($jackpot);

        if ($jackpot_amount_collection === NULL) {
            return $jackpot_amount_collection;
        }
        
        $filtered = $jackpot_amount_collection->filter(function ($value, $key) use ($jackpot_amount_collection) {

            if ($key < count($jackpot_amount_collection) - 1) {
                //check if current jp value is bigger than the next for at least 20% thus jackpot is won
                return $value > ((int) $value * 0.2) + $jackpot_amount_collection[$key + 1];
            } 
           
        });
        
        if ($filtered->isEmpty()) {
            return NULL;
        }

        return (int) $filtered->avg();
    }
}