<?php

namespace App\Services;

use Illuminate\Support\Str;

class Excerpt
{
    private $post_content;

    public function __construct(string $post_content = '')
    {
        $this->post_content = $post_content;
    }

    public function get(): string
    {
        if (empty($this->post_content)) {
            return $this->post_content;
        }

        //convert special chars to utf-8 ( £, € )
        $this->post_content = mb_convert_encoding($this->post_content, 'HTML-ENTITIES', 'UTF-8');
        $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        $doc->loadHTML($this->post_content);
        $xpath = new \DOMXPath($doc);
        
        foreach ($xpath->query('//h1|//h2|//h3|//h4|//h5|//h6|//figcaption|//table') as $node) {
            $node->parentNode->removeChild($node);
        }

        $html = $doc->saveHTML();
        
        libxml_clear_errors();
        $string_with_paragraph_tags = strip_tags(trim($html), ['p']);

        $excerpt = Str::of($string_with_paragraph_tags)->words(50, '...')
            ->replaceMatches('/\r|\n/', '')
            ->replaceMatches('/<\/p>/', ' ')
            ->replaceMatches('/<p>?( .+[\"\'])?>/', '')
            ->ucfirst();

        return html_entity_decode($excerpt);
    }
    
}