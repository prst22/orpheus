<?php

namespace App;
use App\Casino;
use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    protected $casts = [
        'active' => 'boolean'
    ];
    public $timestamps = false;
    protected $fillable = ['name', 'provider', 'game_id'];

    public function casinos()
    {
        return $this->belongsToMany('App\Casino')->withPivot('active');
    }

    public function jackpots()
    {
        return $this->hasMany('App\Jackpot');
    }

    public function jackpotsInCasino($casino_id)
    {
        return $this->jackpots()->where('casino_id', '=',  $casino_id);
    }
}
