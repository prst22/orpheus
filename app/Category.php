<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts(){
        return $this->belongsToMany('App\Post');
    }
    
    public function streams(){
        return $this->belongsToMany('App\Stream');
    }

    // return array
    public function get_all_categories($current_slug = ''){
        $categories = Category::all('type', 'name', 'slug')->sortBy('name');
        
        if($categories->isEmpty()){
            return false;
        }
        
        foreach ($categories as $category) {
            if($category->type != 'streams'){
                $category_link = sprintf( 
                    '<a href="%1$s" title="%2$s" class="categories__link %3$s">%4$s</a>',
                    sprintf( '%s', route( 'posts.category', $category->slug ) ),
                    sprintf( __( 'View all articles in %s'), ucwords($category->name) ),
                    (!empty($current_slug) && $current_slug === $category->slug ? 'active_category' : ''),
                    ucwords($category->name)
                );
                $all_category_links[] = $category_link;
            }else{
                $category_link = sprintf( 
                    '<a href="%1$s" title="%2$s" class="categories__link %3$s">%4$s %5$s</a>',
                    sprintf( '%s', route( 'streams.category', $category->slug ) ),
                    sprintf( __( 'View all streams in %s'), ucwords($category->name) ),
                    (!empty($current_slug) && $current_slug === $category->slug ? 'active_category' : ''),
                    ucwords($category->name),
                    '<svg class="icon"><use xlink:href="' . \URL::asset('assets') . '/symbol-defs.svg#monitor"></use></svg>'
                );
                $all_category_links[] = $category_link;
            }
            
        }
        return $all_category_links;
    }
}
