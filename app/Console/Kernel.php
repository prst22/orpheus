<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\CasinoData\Providers\Netent;
use App\CasinoData\Providers\Yggdrasil;
use App\CasinoData\Providers\RedTiger;
use App\CasinoData\Providers\Microgaming;
// use App\CasinoData\Providers\PushGaming;
use App\CasinoData\Providers\QuickSpin;
// use App\CasinoData\Casinos\BlueprintJackpotKing;

// use App\LotteryData\Euromillions;
use App\LotteryData\Megamillions;
// use App\LotteryData\Powerball;
use App\LotteryData\Lottomax;

use App\Casino;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $all_casino_ids = Casino::tracked()->get()->pluck('id');

        $schedule->call(new Netent($all_casino_ids->all()))->cron('0 0,3,6,9,12,15,18,21 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        $schedule->call(new Yggdrasil($all_casino_ids->all()))->cron('10 0,3,6,9,12,15,18,21 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        $schedule->call(new QuickSpin($all_casino_ids->all()))->cron('20 1,12,17,23 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        $schedule->call(new RedTiger($all_casino_ids->all()))->everyTwoHours()->when(fn() => $all_casino_ids->isNotEmpty());
        $schedule->call(new Microgaming($all_casino_ids->all()))->cron('0 3,8,13,18,23 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        // $schedule->call(new PushGaming($all_casino_ids->all()))->cron('30 0,3,6,9,12,15,18,21 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        // $schedule->call(new BlueprintJackpotKing())->cron('40 4,9,14,19,23 * * *')->when(fn() => $all_casino_ids->isNotEmpty());
        // Lottery 
        // $schedule->call(new Euromillions)->everySixHours();
        $schedule->call(new Megamillions)->everySixHours();
        // $schedule->call(new Powerball)->everySixHours();
        $schedule->call(new Lottomax)->everySixHours();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
