<?php

namespace App\Stream;

use App\Admin;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Carbon;
use App\Http\Traits\Stream\GetTwitchAppAccessToken;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\FetchStreamsException;

class FetchStream
{
    private $url_arr;

    use GetTwitchAppAccessToken;

    function __construct($url_arr)
    {
        $this->url_arr = $url_arr;
        $current_token_collection = Admin::find(1)->select('twitch_aa_token')->first();
        $current_token_arr = $current_token_collection['twitch_aa_token'];
       
        if (isset($current_token_arr['expires_in'])) {
            
            $expiration_date = Carbon::parse($current_token_arr['expires_in']);

            if (now()->gte($expiration_date)) {
                
                $this->get_token();
                $admin = Admin::find(1);
                $admin->twitch_aa_token = [
                    'access_token' => $this->token,
                    'expires_in' => now()->addSeconds($this->token_expires_in)
                ];

                $admin->save();
                
            } else {
                
                $this->token = $current_token_arr['access_token'];
          
            }
            
        } else {

            $this->get_token();
            $admin = Admin::find(1);
            $admin->twitch_aa_token = [
                'access_token' => $this->token,
                'expires_in' => now()->addSeconds($this->token_expires_in)
            ];

            $admin->save();
        }

        $client_id = config('services.twitch.client_id');
        
        $this->headers = [
            "Client-ID" => $client_id,
            "Authorization" => "Bearer {$this->token}"
        ];
        
        $this->games = [
            'poker' => '488190', 
            'slots' => '498566', 
            'roulette' => '490344', 
            'blackjack' => '1893867416', 
            'virtual casino' => '29452', 
            'casino' => '493887283',
        ];
        $this->poker_streams = array_slice($this->games, 0, 1);
        $this->casino_streams = array_slice($this->games, 1);
    }

    private function parse_data_to_array(string $category = '')
    {   
        $streamers_arr = [];
        $game_types = $category ? ( $category === 'poker_streams' ? $this->poker_streams : $this->casino_streams ) : $this->games;

        if (!empty($this->token)) {

            foreach ($this->url_arr as $url) {

                $twitch_response = Http::retry(2, 55)->withHeaders($this->headers)->get($url);
                $json_response = $twitch_response->json();  

                if ($twitch_response->failed()) {
                    throw new FetchStreamsException('Fetching streams failed');
                    return $streamers_arr;
                }
                
                if (!isset($json_response['data'])) {
                    throw new FetchStreamsException('Fetching streams failed');
                    return $streamers_arr;
                }

                if (count($json_response['data']) <= 0) { 
                    return $streamers_arr;
                }

                foreach ($json_response['data'] as $stream_arr) {

                    if (array_search($stream_arr['game_id'], $game_types)) {  
                            $streamer_name = ucfirst(strtolower($stream_arr['user_name']));
                            $raw_title = preg_replace('/[^\p{L}\s\d.,:\/]/u', '', mb_convert_encoding($stream_arr['title'], "UTF-8")); 
                            $stream_title = strlen($raw_title) > 38 ? mb_substr($raw_title, 0, 38, "UTF-8") . "..." : $raw_title;
                            $stream_title_formated = mb_convert_case($stream_title, MB_CASE_TITLE, "UTF-8");
                            $stream_language = $stream_arr['language'];
                            $stream_img = $stream_arr['thumbnail_url'];
                            $stream_img = str_replace("{width}", "562", $stream_img);
                            $stream_img = str_replace("{height}", "317", $stream_img);
                            $game_id = $stream_arr['game_id'];
                            $viewer_count = $stream_arr['viewer_count'];
                            $streamers_arr[$stream_arr['user_id']] = ['name' => $streamer_name, 
                                'title' => $stream_title_formated, 
                                'lan' => $stream_language, 
                                'img_src' => $stream_img, 
                                'game' => array_search($game_id, $game_types), 
                                'viewers' => $viewer_count
                            ];
                    }

                }  

            }
 
        }
        
        return $streamers_arr;
    }

    public function fetch_or_get_streams_cache_var($category = false, $favourite = false)
    {
        if ($favourite) {

            return $streamers_arr_for_query = $this->parse_data_to_array();
            
        }
        
        if ($category) {

            // if streams sorted by category dont use category cache var
            $saved_streamers_arr_for_query = Cache::remember("{$category}_streams", 90, function () use ($category) {
                return $this->parse_data_to_array($category);
            });

            return $saved_streamers_arr_for_query;

        } else {

            // save or use all streams from cache all_streams
            $saved_streamers_arr_for_query = Cache::remember('all_streams', 90, function () {
                return $this->parse_data_to_array();
            });

            return $saved_streamers_arr_for_query;

        } 
    }
}; 