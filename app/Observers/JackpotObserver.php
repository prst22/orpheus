<?php

namespace App\Observers;

use App\Jackpot;
use App\Services\AverageJackpotWin;

class JackpotObserver
{
    /**
     * Handle the Jackpot "updated" event.
     *
     * @param  \App\Jackpot  $jackpot
     * @return void
     */
    public function updated(Jackpot $jackpot)
    {
        if (!$jackpot) {
            return;
        }
        
        $avg_jackpot = AverageJackpotWin::get($jackpot); 

        if (!$avg_jackpot) {
            return;    
        }
        
        if ($jackpot->average === NULL) {
            $jackpot->average = $avg_jackpot;
        } else {
            $jackpot->average = ($jackpot->average + $avg_jackpot) / 2;
        }
        
        $jackpot->saveQuietly();
    }
}
