<?php

namespace App\Exceptions;

use Exception;

class FetchStreamsException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        \Log::debug('Fetching streams failed');
    }
}