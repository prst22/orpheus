<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Casino extends Model
{
    protected $casts = [
        'netent_casino_data' => 'array',
        'yggdrasil_casino_data' => 'array',
        'redtiger_casino_data' => 'array',
        'pushgaming_casino_data' => 'array',
        'quickspin_casino_data' => 'array',
        'image' => 'array',
        'info' => 'array',
    ];
    protected $fillable = ['name'];
    public $timestamps = false;

    public function scopeTracked($query) 
    {
        return $query->where('tracked', 1);
    }

    public function slots()
    {
        return $this->belongsToMany('App\Slot')->withPivot('active');
    }

    public function activeSlots()
    {
        return $this->belongsToMany('App\Slot')->wherePivot('active', 1);
    }

    public function getLogo($size)
    {
        if (!$this->image && !isset($this->image['branding'])) {
            return null;
        }

        if (!isset($this->image['branding']['logo'])) {
            return null;
        }

        if ($size === 'lg') {
            if(!isset($this->image['branding']['logo']['lg'])) {
                return null;
            }
    
            return $this->image['branding']['logo']['lg'];
        }

        if ($size === 'md') {
            if(!isset($this->image['branding']['logo']['md'])) {
                return null;
            }
    
            return $this->image['branding']['logo']['md'];
        }

        if ($size === 'sm') {
            if(!isset($this->image['branding']['logo']['sm'])) {
                return null;
            }
    
            return $this->image['branding']['logo']['sm'];
        }

        return null;
    }   

    public function getBanner($size)
    {
        if (!$this->image && !isset($this->image['branding'])) {
            return null;
        }

        if (!isset($this->image['branding']['banner'])) {
            return null;
        }

        if ($size === 'lg') {

            if (!isset($this->image['branding']['banner']['lg'])) {
                return null;
            }

            return $this->image['branding']['banner']['lg'];
        }
        
        if ($size === 'md') {

            if (!isset($this->image['branding']['banner']['md'])) {
                return null;
            }

            return $this->image['branding']['banner']['md'];
        }

        if ($size === 'sm') {

            if (!isset($this->image['branding']['banner']['sm'])) {
                return null;
            }

            return $this->image['branding']['banner']['sm'];
        }

        return null;
    }
}
