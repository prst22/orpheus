<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Laravelista\Comments\Commentable;

class Post extends Model
{
    // use Searchable, Commentable;
    use Commentable;

    protected $casts = [
        'thumbnail' => 'array',
        'published' => 'boolean',
        'sticky' => 'boolean',
    ];

    public function author()
    {
        return $this->belongsTo('App\Admin');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function get_categories($id, $is_arr = false)
    {
        $categories = Post::find($id)->categories()->orderBy('name')->get();

        if ($categories->isEmpty()) {
            return false;
        }
        
        if (!$is_arr) {
            foreach($categories as $category){
                $category_link = sprintf( 
                    '<a href="%1$s" title="%2$s" class="categories__link">%3$s</a>',
                    sprintf( '%s', route( 'posts.category', $category->slug ) ),
                    sprintf( __( 'View all articles in %s'), $category->name ),
                    $category->name
                );
                $category_names_arr[] = $category_link;
            }
            
            $categories_string = implode(', ', $category_names_arr);
    
            return $categories_string;
        } else {
            foreach ($categories as $category) {
                $category_array[] = $category->name;
            }
            return $category_array;
        }
    }

    public function get_approved_comments_count($id)
    {
        $comments_count = Post::find($id)->comments()->where('approved', true)->count();
   
        return $comments_count;
    }

    public function splitBody($value)
    {
        return explode('. ', $value);
    }

    // public function toSearchableArray()
    // {

    //     $array['title'] = $this->title;
    //     $array['body'] = $this->body;
    //     $array['created_at'] = $this->created_at;
        
    //     return $array;
    // }

}
