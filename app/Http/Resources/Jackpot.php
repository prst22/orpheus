<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use DB;
use Illuminate\Support\Str;

class Jackpot extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'slot_name' => $this->slot->name,
            'casino_name' => $this->casino->name,
            'slot_id' => $this->slot_id,
            'casino_id' => $this->casino_id,
            'jackpot_id' => $this->id,
            'jackpot_type' => $this->name,
            'active' => DB::table('casino_slot')->where([
                    ['slot_id', '=', $this->slot_id],
                    ['casino_id', '=', $this->casino_id]
                ])->first()->active,
            'date_amount' => $this->date_amount,
            'average' => $this->average,
            'provider' => $this->slot->provider,
        ];
    }
}
