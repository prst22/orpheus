<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJackpotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'casino-id' => 'required|string',
            'slot-id' => 'required|string',
            'jackpot-type' => 'required|string',
            'jackpot-name' => 'string|nullable|max:40',
        ];
    }
}
