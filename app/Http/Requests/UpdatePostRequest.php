<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'unique' => 'There is an article with same :attribute, please choose another',
            'dimensions' => 'Image should be 1080 x 418 minimum size'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $post_id = $this->route('post')->id;

        return [
            'thumbnail' => 'image|mimes:jpeg,jpg,png,gif,webp|dimensions:min_width=1080,min_height=418|max:2048',
            'title' => [Rule::unique('posts')->ignore($post_id), 'required'],
            'body' => 'required',
            'category' => 'required'
        ];
    }
}
