<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateStreamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return ['unique' => 'There is a stream with same :attribute, please choose another'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $stream_id = $this->route('stream')->id;

        return [
            'channel_name' => [Rule::unique('streams')->ignore($stream_id), 'required'],
            'channel_id' => [Rule::unique('streams')->ignore($stream_id), 'required'],
            'category' => 'required'
        ];
    }
}
