<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'unique' => 'There is a article with same :attribute, please choose another.',
            'dimensions' => 'Image should be 1020 x 418 minimum size'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => 'required|image|mimes:jpeg,jpg,png,gif,webp|dimensions:min_width=1020,min_height=418|max:2048',
            'category'  => 'required',
            'title'     => 'required|unique:posts',
            'body'      => 'required|string',
        ];
    }
}
