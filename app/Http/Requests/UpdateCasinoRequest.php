<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCasinoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $casino_id = $this->route('casino')->id;

        return [
            'name' => [Rule::unique('casinos')->ignore($casino_id), 'required', 'string'],
            'netentkey' => 'nullable|array',
            'netentvalue' => 'nullable|array',
            'yggdrasilkey' => 'nullable|array',
            'yggdrasilvalue' => 'nullable|array',
            'url' => 'nullable|url',
            'logo' => 'nullable|image|mimes:png,jpg,jpeg,gif|dimensions:min_height=300|max:2048',
            'banner' => 'nullable|image|mimes:jpeg,jpg,png,gif,webp|dimensions:min_width=1280,min_height=427|max:2048',
            'description' => 'string|nullable',
            'games' => 'nullable|array',
            'providers' => 'nullable|array',
        ];
    }
}