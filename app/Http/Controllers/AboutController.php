<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class AboutController extends Controller
{
    public function index()
    {
        $all_categories =  (new Category)->get_all_categories();

        return view('about_us', ['all_categories' => $all_categories]);
    }
}
