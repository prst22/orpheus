<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Casino;

class ShowCasinoController extends Controller
{
    public function index()
    {
        $casinos = Casino::select('name', 'description', 'url', 'rating', 'image', 'info', 'slug')
            ->orderBy('rating', 'desc')
            ->orderBy('name', 'asc')
            ->paginate(20);

        return view('casino.index', [
            'casinos' => $casinos 
        ]);
    }

    public function show(Casino $casino)
    {    
        return view('casino.show', [
            'casino' => $casino,
        ]);
    }
}
