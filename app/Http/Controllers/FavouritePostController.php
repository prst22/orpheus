<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FavouritePostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->per_page = 20;
    }

    public function index()
    {
        $user = auth()->user();
        $favorite_posts = $user->favourite_posts()->paginate($this->per_page);

        return view('favourite.posts')->with([
            'user' => $user,
            'posts' => $favorite_posts
        ]);
    }

    public function store($postid)
    {
        $user_id = auth()->user()->id;
        $user = User::findOrFail($user_id);

        $user->favourite_posts()->syncWithoutDetaching([$postid]);

        return redirect()->back()->with('success', 'Article was added to your favourites!');
    }
    
    public function destroy($postid)
    {
        $user_id = auth()->user()->id;
        $user = User::findOrFail($user_id);

        $user->favourite_posts()->detach($postid);

        return redirect()->back()->with('success', 'Article was removed from your favourite list!');
    }
}
