<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
// use App\LotteryData\Powerball;
// use App\LotteryData\Megamillions;
// use App\LotteryData\Euromillions;
// use App\LotteryData\Lottomax;

use App\LotteryJackpot;
use App\Services\PageNumber;

class LotteryJackpotTrackerController extends Controller
{
    public function __construct()
    {
        $this->per_page = 20;
    }

    public function index()
    {
        // $powerball = new Powerball;
        // $powerball();
        // $megamillions = new Megamillions;
        // $megamillions();
        // $euromillions = new Euromillions;
        // $euromillions();
        // $lottomax = new Lottomax;
        // $lottomax();
        
        $jackpots = LotteryJackpot::orderBy('draw_date', 'DESC')->whereNotNull('jackpot_data->winning_numbers')
            ->paginate($this->per_page);

        $categories = new Category;
        $all_categories = $categories->get_all_categories();
        $pageStr = (new PageNumber($jackpots))->get();

        return view('lottery_tracker', [
            'jackpots' => $jackpots,
            'all_categories' => $all_categories,
            'name' => 'all',
            'title' => 'Lottery tracker' . $pageStr
        ]);
    }

    public function sortByName($name)
    {
        if ($name == 'euromillions' || $name == 'powerball' || $name == 'megamillions' || $name == 'lottomax') {
            $jackpots = LotteryJackpot::orderBy('draw_date', 'DESC')->where('name', $name)->whereNotNull('jackpot_data->winning_numbers')
                ->paginate($this->per_page);   
        } else {
            abort(404);
        }
        $categories = new Category;
        $all_categories = $categories->get_all_categories();
        $pageStr = (new PageNumber($jackpots))->get();

        return view('lottery_tracker', [
            'jackpots' => $jackpots,
            'all_categories' => $all_categories,
            'name' => strtolower($name),
            'title' => ucfirst($name) . ' lottery tracker' . $pageStr
        ]);
    }
}
