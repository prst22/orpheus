<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Stream;
use App\Category;

use App\Services\PageNumber;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->per_page = 24;
    }

    public function search(Request $request)
    { 
        $categories = new Category;
        $all_categories = $categories->get_all_categories();
        $query = request('query');
        $pageStr = '';

        if (isset($query) && $query !== '') {
            $posts = Post::search($query)->get();	
            $streams = Stream::search($query)->get();
            $results = $posts->merge($streams)->paginate($this->per_page);
            $pageStr = (new PageNumber($results))->get();
        } else {
            $results = [];
        }
        
        return view('search_results', [
            'query' => $query,
            'results' => $results,
            'all_categories' => $all_categories,
            'title' => 'Search results' .  $pageStr
        ]);
    }
}
