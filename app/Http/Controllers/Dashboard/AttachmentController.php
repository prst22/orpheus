<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Dashboard\X;
use App\Attachment;
use App\Post;
use App\Category;
use App\Stream;
use App\Slot;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;

use App\Http\Traits\RemoveImage;

class AttachmentController extends Controller
{
    use RemoveImage;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 54;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attachments = Attachment::orderBy('created_at', 'desc')->paginate($this->per_page);
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_categories = Category::all()->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.attachment', [
                'attachments' => $attachments,
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'filemanager' => true,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($images_arr)
    {
        $attachment = new Attachment; 
        $attachment->image = $images_arr;
        $attachment->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attachment $attachment)
    {    
        $attachment_id = $attachment->id;

        $this->remove_image($attachment->image);

        Attachment::destroy($attachment->id);
        
        return redirect()->route('attachments.index')->with('success', "Attachment with id:{$attachment_id} removed!");
    }
}
