<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Http\Requests\StoreStreamRequest;
use App\Http\Requests\UpdateStreamRequest;

use App\Stream;
use App\Slot;
use App\Post;
use App\User;
use App\Category;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;

class StreamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 48;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count(); 
        $total_streams = Stream::all()->count();
        $streams = Stream::orderBy('created_at', 'desc')->paginate($this->per_page);
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.stream.index', [
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'streams' => $streams,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('type', 'streams')->get() ?? [];
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.stream.create', [
                'categories' => $categories,
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStreamRequest $request)
    {
        $categories = request('category');
        $stream = new Stream; 
        $stream->channel_id = request('channel_id');
        $stream->channel_name = Str::lower(request('channel_name'));
        $stream->description = request('description');
        $stream->slug = str_slug(request('channel_name'), '-');
        $stream->user_id = auth()->user()->id;
        $stream->save();
        $stream->categories()->sync($categories);
        
        return redirect()->route('streams.index', [$stream->slug]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  obj  $stream
     * @return \Illuminate\Http\Response
     */
    public function edit(Stream $stream)
    {
        $categories = Category::where('type', 'streams')->get() ?? [];
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        $stream_categories = $stream->categories()->orderBy('name')->get();
        $category_ids_arr = [];
        foreach($stream_categories as $category){
            $category_ids_arr[] = $category->id;
        }

        return view('dashboard.stream.edit', [
            'stream' => $stream,
            'categories' => $categories,
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'current_post_categories' => $category_ids_arr,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,
            'total_casinos' => $total_casinos,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  obj  $stream
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStreamRequest $request, Stream $stream)
    {
        $categories = request('category');
        $stream_to_edit = $stream; 
        $stream_to_edit->channel_name = Str::lower(request('channel_name'));
        $stream_to_edit->channel_id = request('channel_id');
        $stream_to_edit->description = request('description');
        $stream_to_edit->slug = str_slug(request('channel_name'), '-');
        $stream_to_edit->user_id = auth()->user()->id;
        $stream_to_edit->save();
        $stream_to_edit->categories()->sync($categories);

        return redirect()->route('streams.edit', [$stream_to_edit->id])->with('success', 'Stream updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->stream_id = $id;

        Category::chunk(250, function ($chunk_cat) {
            foreach ($chunk_cat as $cat_item) {
                $cat_item->streams()->detach($this->stream_id);
            }
        });

        User::chunk(500, function ($users){
            foreach ($users as $user) {
                $user->favourite_streams()->detach($this->stream_id);
            }
        });

        Stream::destroy($id);

        return redirect()->back(302, [], route('streams.index'))->with('success', 'Stream removed!');
    }
    
    public function __destruct() 
    {
        $this->stream_id = null;
    }
}
