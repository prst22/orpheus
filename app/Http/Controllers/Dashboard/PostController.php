<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

use App\Post;
use App\User;
use App\Category;
use App\Stream;
use App\Slot;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;
use Intervention\Image\Facades\Image;

use App\Http\Traits\Post\SavePostThumbnails;
use App\Http\Traits\RemoveImage;
use App\Services\Excerpt;

class PostController extends Controller
{
    use SavePostThumbnails;
    use RemoveImage;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 20;
    }
    
    public function index()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $posts = Post::orderBy('created_at', 'desc')->paginate($this->per_page);
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.post.index', [
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'posts' => $posts,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('type', 'posts')->get() ?? [];
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.post.create', [
                'categories' => $categories,
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $thumbnail_arr = $this->save_post_thumbnails($request->file('thumbnail'), $request->title);

        $excerpt = (new Excerpt(request('body')))->get();
       
        $filtered_body = strip_tags(request('body'));
        $string_content = html_entity_decode($filtered_body); 

        $categories = request('category');
        $post = new Post; 
        $post->title = request('title');
        $post->slug = str_slug(request('title'), '-');
        $post->body = request('body');
        $post->thumbnail = $thumbnail_arr;
        $post->published = $request->has('published');
        $post->sticky = $request->has('sticky');
        $post->user_id = auth()->user()->id;
        $post->excerpt = $excerpt ?? '';
        $post->word_count = str_word_count($string_content, 0);
        $post->views = rand(5, 25);
        $post->save();
        $post->categories()->sync($categories);
        
        return redirect()->route('posts.show', [$post->slug]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::where('type', 'posts')->get() ?? [];
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        $post_categories = $post->categories()->orderBy('name')->get();
        $category_ids_arr = [];

        foreach($post_categories as $category){
            $category_ids_arr[] = $category->id;
        }

        return view('dashboard.post.edit', [
            'post' => $post,
            'categories' => $categories,
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'current_post_categories' => $category_ids_arr,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,
            'total_casinos' => $total_casinos,
        ]);
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $post_to_edit = $post;

        if ($request->hasFile('thumbnail')) {
            //remove old thumbnails
            $this->remove_image($post_to_edit->thumbnail);
            //add new thumbnails
            $thumbnail_arr = $this->save_post_thumbnails($request->file('thumbnail'), $request->title);
            
            $post_to_edit->thumbnail = $thumbnail_arr;
        }

        $excerpt = (new Excerpt(request('body')))->get();
        
        $filtered_body = strip_tags(request('body'));
        $string_content = html_entity_decode($filtered_body); 
        
        $categories = request('category');
        $post_to_edit->title = request('title');
        $post_to_edit->slug = str_slug(request('title'), '-');
        $post_to_edit->body = request('body');
        $post_to_edit->user_id = auth()->user()->id;
        $post_to_edit->excerpt = $excerpt ?? '';
        $post_to_edit->word_count = str_word_count($string_content, 0);
        $post_to_edit->published = $request->has('published');
        $post_to_edit->sticky = $request->has('sticky');
        $post_to_edit->save();
        $post_to_edit->categories()->sync($categories);
        
        return redirect()->route('posts.edit', [$post->id])->with('success', 'Article updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->post_id = $post->id;
        
        Comment::where('commentable_id', $this->post_id)->delete();
        
        Category::chunk(250, function ($chunk_cat){
            foreach ($chunk_cat as $cat_item) {
                $cat_item->posts()->detach($this->post_id);
            }
        });
        
        User::chunk(500, function ($users){
            foreach ($users as $user) {
                $user->favourite_posts()->detach($this->post_id);
            }
        });

        $this->remove_image($post->thumbnail);

        Post::destroy($post->id);

        return redirect()->back(302, [], route('dashboard'))->with('success', "Post with id:{$this->post_id} removed!");
    }

    public function __destruct() 
    {
        $this->post_id = null;
    }
}
