<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

use App\Post;
use App\Category;
use App\Stream;
use App\Slot;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $categories = Category::orderBy('name', 'asc')->get();
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.category', [
                'categories' => $categories,
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $new_category = new Category;
        $new_category->type = strtolower(request('type'));
        $new_category->name = strtolower(request('name'));
        $new_category->slug = str_slug(request('name'), '-');
        $new_category->save();
        $name = ucfirst($new_category->name);

        return redirect()->route('categories.index')->with('success', "{$name} category created!");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category_to_edit = Category::find($category)->first();

        if(empty($category_to_edit)){
            abort(404); 
        }
        
        $this->cat_id = $category->id;

        if($category->type !== strtolower(request('type'))){
            if($category->type !== 'posts'){
                Post::chunk(250, function ($posts) {
                    foreach ($posts as $post) {
                        $post->categories()->detach($this->cat_id);
                    }
                });
            }elseif($category->type !== 'streams'){
                Stream::chunk(250, function ($streams) {
                    foreach ($streams as $stream) {
                        $stream->categories()->detach($this->cat_id);
                    }
                });
            } 
        }

        $category_to_edit->type = strtolower(request('type'));
        $category_to_edit->name = strtolower(request('name'));
        $category_to_edit->slug = str_slug(request('name'), '-');
        $category_to_edit->save();
        
        return redirect()->route('categories.index')->with('success', "Category with id:{$category->id} updated!" );;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->cat_id = $category->id;
        Post::chunk(250, function ($posts) {
            foreach ($posts as $post) {
                $post->categories()->detach($this->cat_id);
            }
        });

        Stream::chunk(250, function ($streams) {
            foreach ($streams as $stream) {
                $stream->categories()->detach($this->cat_id);
            }
        });

        Category::destroy($category->id);

        return redirect()->route('categories.index')->with('success', "{$category->name} category removed!");
    }

    public function __destruct() 
    {
        $this->cat_id = null;
    }
}
