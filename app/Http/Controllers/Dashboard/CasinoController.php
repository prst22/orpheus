<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use App\Http\Requests\UpdateCasinoRequest;
use App\Http\Requests\StoreCasinoRequest;

use App\Slot;
use App\Casino;
use App\Stream;
use App\Post;
use App\Jackpot;
use App\Category;
use Laravelista\Comments\Comment;
use App\Services\ValidateSlotProviderData;

use App\Http\Traits\Casino\SaveCasinoLogo;
use App\Http\Traits\Casino\SaveCasinoBanner;
use App\Http\Traits\RemoveImage;

class CasinoController extends Controller
{
    use SaveCasinoLogo;
    use SaveCasinoBanner;
    use RemoveImage;

    private ?array $logo_arr = null;
    private ?array $banner_arr = null;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 20;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();
        $casinos = Casino::orderBy('name', 'asc')->paginate($this->per_page);

        return view('dashboard.casino.index', [
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,     
            'total_casinos' => $total_casinos,   
            'casinos' => $casinos,   
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.casino.create', [
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,     
            'total_casinos' => $total_casinos,     
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCasinoRequest $request)
    {          
        $providers_data_arr = $request->only(['netentkey', 'netentvalue', 'yggdrasilkey', 'yggdrasilvalue', 'redtigerkey', 'redtigervalue', 'pushgamingkey', 'pushgamingvalue', 'quickspinkey', 'quickspinvalue']);
        $casino_info = [];
        $validator = new ValidateSlotProviderData($providers_data_arr);

        $netent_casino_data = $validator->Netent();
        $yggdrasil_casino_data = $validator->Yggdrasil();
        $redtiger_casino_data = $validator->Redtiger();
        $pushgaming_casino_data = $validator->Pushgaming();
        $quickspin_casino_data = $validator->Quickspin();
        
        if($request->has('games')) {
            $casino_info['games'] = request('games');
        }

        if($request->has('providers')) {
            $casino_info['providers'] = request('providers');
        }

        if($request->has('payment-methods')) {
            $casino_info['payment-methods'] = request('payment-methods');
        }

        $casino = new Casino;
        $casino->name = request('name');
        $casino->slug = str_slug(request('name'), '-');
        $casino->tracked = $request->has('tracked');
        $casino->netent_casino_data = $netent_casino_data;
        $casino->yggdrasil_casino_data = $yggdrasil_casino_data;
        $casino->redtiger_casino_data = $redtiger_casino_data;
        $casino->pushgaming_casino_data = $pushgaming_casino_data;
        $casino->quickspin_casino_data = $quickspin_casino_data;
        $casino->url = request('url');
        $casino->rating = request('rating');
        $casino->info = $casino_info;
        $casino->description = request('description');

        if($request->hasFile('logo')){
            
            $this->save_casino_logo($request->file('logo'), $casino->name);

            if(isset($casino->image['branding'])){
                $curr_branding = $casino->image;
                $curr_branding['branding']['logo'] = $this->logo_arr;
                $casino->image = $curr_branding;
            } else {
                $casino->image = ['branding' => ['logo' => $this->logo_arr]];
            }    
        }

        if($request->hasFile('banner')){

            $this->save_casino_banner($request->file('banner'), $casino->name);

            if(isset($casino->image['branding'])){
                $curr_branding = $casino->image;
                $curr_branding['branding']['banner'] = $this->banner_arr;
                
                $casino->image = $curr_branding;
            } else {
                $casino->image = ['branding' => ['banner' => $this->banner_arr]];
            } 
        } 
       
        $casino->save();

        return redirect()->route('casinos.index')->with('success', "{$casino->name} casino crated!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\casino  $casino
     * @return \Illuminate\Http\Response
     */
    public function edit(Casino $casino)
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();
       
        return view('dashboard.casino.edit', [
            'casino' => $casino,
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,     
            'total_casinos' => $total_casinos,     
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\casino  $casino
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCasinoRequest $request, Casino $casino)
    {
        $providers_data_arr = $request->only(['netentkey', 'netentvalue', 'yggdrasilkey', 'yggdrasilvalue', 'redtigerkey', 'redtigervalue', 'pushgamingkey', 'pushgamingvalue', 'quickspinkey', 'quickspinvalue']);   
        $casino_info = [];
        $validator = new ValidateSlotProviderData($providers_data_arr);

        $netent_casino_data = $validator->Netent();
        $yggdrasil_casino_data = $validator->Yggdrasil();
        $redtiger_casino_data = $validator->Redtiger();
        $pushgaming_casino_data = $validator->Pushgaming();
        $quickspin_casino_data = $validator->Quickspin();
 
        if($request->has('games')) {
            $casino_info['games'] = request('games');
        }

        if ($request->has('providers')) {
            $casino_info['providers'] = request('providers');
        }

        if($request->has('payment-methods')) {
            $casino_info['payment-methods'] = request('payment-methods');
        }

        $casino->name = request('name');
        $casino->slug = str_slug(request('name'), '-');
        $casino->tracked = $request->has('tracked');
        $casino->netent_casino_data = $netent_casino_data;
        $casino->yggdrasil_casino_data = $yggdrasil_casino_data;
        $casino->redtiger_casino_data = $redtiger_casino_data;
        $casino->pushgaming_casino_data = $pushgaming_casino_data;
        $casino->quickspin_casino_data = $quickspin_casino_data;
        $casino->url = request('url');
        $casino->rating = request('rating');
        $casino->info = $casino_info;
        $casino->description = request('description');

        if ($request->hasFile('logo')) {
            // check if there is an old logo and remove it
            if(!empty($casino->image)){
                if(!empty($casino->image['branding'])){
                    !empty($casino->image['branding']['logo']) ? $this->remove_image($casino->image['branding']['logo']) : null;
                }
            }
            
            $this->save_casino_logo($request->file('logo'), $casino->name);

            if(isset($casino->image['branding'])){
                $curr_branding = $casino->image;
                $curr_branding['branding']['logo'] = $this->logo_arr;
                $casino->image = $curr_branding;
            } else {
                $casino->image = ['branding' => ['logo' => $this->logo_arr]];
            }    
        }

        if($request->hasFile('banner')){
            //check if there is an old banner and remove it
            if(!empty($casino->image)){
                if(!empty($casino->image['branding'])){
                    !empty($casino->image['branding']['banner']) ? $this->remove_image($casino->image['branding']['banner']) : null;
                }
            }

            $this->save_casino_banner($request->file('banner'), $casino->name);

            if(isset($casino->image['branding'])){
                $curr_branding = $casino->image;
                $curr_branding['branding']['banner'] = $this->banner_arr;
                
                $casino->image = $curr_branding;
            } else {
                $casino->image = ['branding' => ['banner' => $this->banner_arr]];
            } 
        } 

        $casino->save();

        return redirect()->route('casinos.index')->with('success', "Casino {$casino->name} updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\casino  $casino
     * @return \Illuminate\Http\Response
     */
    public function destroy(casino $casino)
    {
        $this->casino_id = $casino->id;

        Slot::chunk(250, function ($chunk_slots) {
            foreach ($chunk_slots as $slot_item) {
                $slot_item->casinos()->detach($this->casino_id);
            }
        });
        
        Jackpot::where('casino_id', $casino->id)->delete();

        if (!empty($casino->image)) {
            if(!empty($casino->image['branding'])){
                !empty($casino->image['branding']['logo']) ? $this->remove_image($casino->image['branding']['logo']) : null;
                !empty($casino->image['branding']['banner']) ? $this->remove_image($casino->image['branding']['banner']) : null;
            }
        }

        Casino::destroy($casino->id);

        return redirect()->route('casinos.index')->with('success', "{$casino->name} casino removed!");
    }

    public function __destruct() 
    {
        $this->casino_id = null;
    }
}
