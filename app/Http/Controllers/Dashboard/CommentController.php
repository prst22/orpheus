<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use DB;
use Illuminate\Http\Request;

use App\User;
use App\Post;
use App\Category;
use App\Stream;
use App\Slot;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 20;
    }

    public function index()
    {  
        $comments = DB::table('comments')
            ->leftJoin('users', 'users.id', '=', 'comments.commenter_id')
            ->select(['comments.commenter_id', 'comments.comment', 'comments.approved', 'comments.commenter_type', 'comments.created_at', 'comments.id', 'comments.guest_name', 'users.name'])
            ->orderBy('comments.approved', 'asc')->orderBy('comments.created_at', 'desc')
            ->paginate($this->per_page);

        $categories = Category::orderBy('name', 'asc')->get();
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.comments', [
                'categories' => $categories,
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'comments' => $comments,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
            ]
        );
    }

    public function approve($id)
    {
        $comment = Comment::find($id);
        $comment->approved = true;
        $comment->save();
        return redirect()->back()->with('success', 'Comment was approved');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);
        
        if (Config::get('comments.soft_deletes') == true) {
			$comment->delete();
		}
		else {
			$comment->forceDelete();
        }
        
        return redirect()->back()->with('success', 'Comment was removed');
    }
}
