<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Dashboard\AttachmentController;

class CKEditorController extends Controller
{
    protected $images_arr_for_db;
    protected $attachment_controller;
    
    public function __construct(AttachmentController $attachment_controller)
    {
        $this->middleware('auth:admin');
        $this->images_arr_for_db = [];
        $this->attachment_controller = $attachment_controller;
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'upload' => 'image|mimes:jpeg,jpg,png,gif,webp'
        ]);

        if ($request->hasFile('upload')) {

            $sub_folder_n = date('n-Y');
            $post_image = $request->file('upload');

            $images_arr = $this->save_post_content_images($post_image, $sub_folder_n);

        } else {
            return json_encode(
                [
                    'error' => [
                        'message' => 'There is no file to upload'
                    ],
                ]
            );
        }

        return json_encode(
            [
                'urls' => $images_arr
            ]
        );
    }

    private function save_post_content_images($image, string $folder)
    {
        $file = str_slug(pathinfo($image->getClientOriginalName())['filename'], '-');
        $folder1 = trim($folder);
        $file_path = "post-images/{$folder1}/";

        $lg_image_width;
        $md_image_width; 
        $sm_image_width;

        $img_lg = Image::make($image)->encode('webp', 40);
        
        $img_lg->resize(1600, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        
        $lg_image_width = $img_lg->width();

        if ( $lg_image_width <= 1600 && $lg_image_width > 836 ) {

            $lg_image_path = $file_path . "{$file}-{$lg_image_width}w-lg.webp";

            Storage::disk('s3')->put($lg_image_path, $img_lg->stream());
            Storage::disk('s3')->setVisibility($lg_image_path, 'public');
            
            $images_arr[$lg_image_width] = Storage::disk('s3')->url($lg_image_path);

        }

        $img_md = $img_lg->resize(836, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $md_image_width = $img_md->width();

        if ( $md_image_width <= 836 && $md_image_width > 400 ) {

            $md_image_path = $file_path . "{$file}-{$md_image_width}w-md.webp";

            Storage::disk('s3')->put($md_image_path, $img_md->stream());
            Storage::disk('s3')->setVisibility($md_image_path, 'public');
            
            $images_arr[$md_image_width] = Storage::disk('s3')->url($md_image_path);

        }
            
        $img_sm = $img_md->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $sm_image_width = $img_sm->width();

        if ( $sm_image_width <= 400 ) {
 
            $sm_image_path = $file_path . "{$file}-{$sm_image_width}w-sm.webp";
            
            Storage::disk('s3')->put($sm_image_path, $img_sm->stream());
            Storage::disk('s3')->setVisibility($sm_image_path, 'public');

            $images_arr[$sm_image_width] = Storage::disk('s3')->url($sm_image_path);

        }
        
        $images_arr['default'] = isset($lg_image_path) ? Storage::disk('s3')->url($lg_image_path) : (isset($md_image_path) ? Storage::disk('s3')->url($md_image_path) : Storage::disk('s3')->url($sm_image_path));
        
        // prepare image array for storing in the database start

        $images = $images_arr;
        unset($images['default']);
        $images = array_values($images);
        $this->images_arr_for_db = $images;

        // prepare image array for storing in the database end
        
        // store image array in data base
        $this->add_images_to_db();
        
        $img_lg = null;

        return $images_arr;
    }

    public function add_images_to_db()
    {
        if ( !empty($this->images_arr_for_db) ) {
            $this->attachment_controller->store($this->images_arr_for_db);
        }
    } 
}
 