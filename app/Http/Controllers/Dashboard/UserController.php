<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Post;
use App\User;
use App\Category;
use App\Stream;
use App\Slot;
use App\Jackpot;
use App\Casino;
use Laravelista\Comments\Comment;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 100;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();
        $users = User::select('id', 'name', 'email', 'created_at', 'email_verified_at')->latest()->paginate($this->per_page);
        
        return view('dashboard.user.index', [
                'total_categories' => $total_categories,
                'total_posts' => $total_posts,
                'total_streams' => $total_streams,
                'unapproved_comments' => $unapproved_comments,
                'total_slots' => $total_slots,
                'total_jackpots' => $total_jackpots,
                'total_casinos' => $total_casinos,
                'users' => $users,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->favourite_posts()->detach($user->cat_id);
        $user->favourite_streams()->detach($user->cat_id);
        
        User::destroy($user->id);
        
        return redirect()->route('users.index')->with('success', "User {$user->name} removed!");
    }
}
