<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests\StoreSlotRequest;
use App\Http\Requests\UpdateSlotRequest;

use App\Slot;
use App\Casino;
use App\Stream;
use App\Post;
use App\Jackpot;
use App\Category;
use Laravelista\Comments\Comment;

class SlotController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 40;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $casino_slot_data = DB::table('slots')
            ->leftJoin('casino_slot', 'casino_slot.slot_id', '=', 'slots.id')
            ->leftJoin('casinos', 'casinos.id', '=', 'casino_slot.casino_id')
            ->select(['casinos.name as casino', 'casino_slot.casino_id', 'slots.id', 'slots.name', 'slots.provider', 'slots.game_id', 'casino_slot.active']);
        $slots = DB::table('jackpots')
            ->rightJoinSub($casino_slot_data, 'slots', function ($join) {
                $join->on('jackpots.slot_id', '=', 'slots.id')
                ->on('jackpots.casino_id', '=', 'slots.casino_id');
            })
            ->select(['slots.*', 'jackpots.type'])
            ->orderBy('slots.name', 'asc')
            ->paginate($this->per_page);
        $casinos = Casino::select(['id', 'name'])->get();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.slot.index', [
            'slots' => $slots,
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'casinos' => $casinos,   
            'total_jackpots' => $total_jackpots,   
            'total_casinos' => $total_casinos,   
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $casinos = Casino::select(['id', 'name'])->orderBy('name', 'asc')->get();  
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();

        return view('dashboard.slot.create', [
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'casinos' => $casinos,
            'total_jackpots' => $total_jackpots,
            'total_casinos' => $total_casinos,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSlotRequest $request)
    {
        $slot = Slot::firstOrCreate(['name' => request('name'), 'provider' => request('provider'), 'game_id' => request('gameid')]); 
        $slot->casinos()->syncWithoutDetaching([
            request('casino') => ['active' => $request->has('active')]
        ]);

        return redirect()->route('slots.create')->with('success', 'Slot created!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlotRequest $request, $id)
    {
        $slot_to_edit = Slot::find($id); 
        $current_casino_id = request('current-casino');
        $new_casino_id = request('new-casino');
        $current_name = request('current-name');
        $current_provider = request('current-provider');
        $current_game_id = request('current-gameid');
        $casinos = $slot_to_edit->casinos();
        $check_if_casino_attached_to_slot = $casinos->where('casinos.id', '=', $current_casino_id)->exists();
        
        if($check_if_casino_attached_to_slot){

            $all_slots = Slot::where('name', request('name'))->where('provider', request('provider'))->where('game_id', request('gameid'))->get();

            if(count($all_slots) > 0){
                foreach($all_slots as $slot){
                    if($slot->casinos()->where('casinos.id', $new_casino_id)->exists()){
                        return redirect()->back(302, [], route('slots.index'))->with('error', 'Slot already exists');
                    }
                }
            }
            
        } 
        
        $slot_to_edit->name = request('name');
        $slot_to_edit->provider = request('provider');
        $slot_to_edit->game_id = request('gameid');

        $slot_to_edit->save();
        // update casino_slot pivot table casino id and remove old jackpot from a slot if exists
        if($current_casino_id !== $new_casino_id){
            $slot_to_edit->casinos()->updateExistingPivot($current_casino_id, [ 'casino_id' => $new_casino_id]);

            if($slot_to_edit->jackpotsInCasino($current_casino_id)->exists()){
                $slot_to_edit->jackpotsInCasino($current_casino_id)->delete();
            }     
        }

        Casino::find($new_casino_id)->slots()->updateExistingPivot($id, ['active' => false]);

        return redirect()->back(302, [], route('slots.index'))->with('success', "Slot with id:{$id} updated!" );
    }

    public function setActive(Request $request, Slot $slot){
        $casino_id = request('casinoid');    
        $slot->casinos()->updateExistingPivot($casino_id, ['active' => $request->has('active')]);

        return redirect()->back(302, [], route('slots.index'))->with('success', "Active updated for slot with id:{$slot->id}"); 
    }

    /**
     * Detach the jackpot from casino.
     *
     * @param  int  $id
     * @param  int  $casino_id
     * @return \Illuminate\Http\Response
     */
    public function detach($id, $casino_id)
    {
        Casino::find($casino_id)->slots()->detach($id);
        Jackpot::where('slot_id', $id)->where('casino_id', $casino_id)->delete();

        return redirect()->back(302, [], route('slots.index'))->with('success', 'Slot detached!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->slot_id = $id;

        Slot::destroy($id);
        
        Casino::chunk(250, function ($chunk_casinos){
            foreach ($chunk_casinos as $casino_item) {
                $casino_item->slots()->detach($this->slot_id);
            }
        });

        Jackpot::where('slot_id', $id)->delete();

        return redirect()->back(302, [], route('slots.index'))->with('success', 'Slot removed!');
    }

    public function __destruct() 
    {
        $this->slot_id = null;
    }
}
