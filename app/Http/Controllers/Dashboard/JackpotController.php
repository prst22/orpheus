<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests\StoreJackpotRequest;
use App\Http\Requests\UpdateJackpotRequest;

use App\Slot;
use App\Casino;
use App\Stream;
use App\Post;
use App\Jackpot;
use App\Category;
use Laravelista\Comments\Comment;

class JackpotController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->per_page = 40;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jackpots = DB::table('jackpots')
            ->leftJoin('slots', 'slots.id', '=', 'slot_id')
            ->leftJoin('casinos', 'casinos.id', '=', 'casino_id')
            ->select(['jackpots.id', 'casinos.name as casino_name', 'jackpots.casino_id', 'jackpots.name as jackpot_name', 'slot_id', 'slots.name as slot_name', 'jackpots.type'])
            ->orderBy('slots.name', 'asc')
            ->paginate($this->per_page);
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $total_jackpots = Jackpot::all()->count();
        $casinos = Casino::select(['id', 'name'])->get();
        $slots = Slot::select(['id', 'name'])->get();
        $total_casinos = Casino::all()->count();

        return view('dashboard.jackpot.index', [
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'total_jackpots' => $total_jackpots,   
            'jackpots' => $jackpots,   
            'casinos' => $casinos,   
            'slots' => $slots,   
            'total_casinos' => $total_casinos,    
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $total_categories = Category::all()->count();
        $total_posts = Post::all()->count();
        $total_streams = Stream::all()->count();
        $unapproved_comments = Comment::where('approved', false)->count();
        $total_slots = Slot::all()->count();
        $casinos = Casino::select(['id', 'name'])->orderBy('name', 'asc')->get();  
        $slots = Slot::select(['id', 'name'])->orderBy('name', 'asc')->get();
        $total_jackpots = Jackpot::all()->count();
        $total_casinos = Casino::all()->count();
 
        return view('dashboard.jackpot.create', [
            'total_categories' => $total_categories,
            'total_posts' => $total_posts,
            'total_streams' => $total_streams,
            'unapproved_comments' => $unapproved_comments,
            'total_slots' => $total_slots,
            'casinos' => $casinos,
            'slots' => $slots,
            'total_jackpots' => $total_jackpots,
            'total_casinos' => $total_casinos,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJackpotRequest $request)
    {
        $cas = Casino::find(request('casino-id'));  
        $slot_attached_to_casino = $cas->slots()->where('slot_id', request('slot-id'))->exists();

        if($slot_attached_to_casino){
            $jackpot = Jackpot::firstOrCreate([
                    'casino_id' => request('casino-id'), 
                    'slot_id' => request('slot-id'), 
                    'type' => request('jackpot-type'),
                    'name' => request('jackpot-name'),
                ]); 
        }else{
            return redirect()->route('jackpots.create')->with('error', "This slot is not attached to {$cas->name} casino"); 
        }

        return redirect()->back(302, [], route('jackpots.index'))->with('success', 'Jackpot created!'); 
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jackpot  $jackpot
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJackpotRequest $request, Jackpot $jackpot)
    {
        $cas = Casino::find(request('casino-id'));  
        $slot_attached_to_casino = $cas->slots()->where('slot_id', request('slot-id'))->exists();

        if($slot_attached_to_casino){    
            $jackpot_to_edit = $jackpot; 
            $jackpot_to_edit->casino_id = request('casino-id');
            $jackpot_to_edit->slot_id = request('slot-id');
            $jackpot_to_edit->type = request('jackpot-type');
            $jackpot_to_edit->name = request('jackpot-name');
            $jackpot_to_edit->save();
        }else{
            return redirect()->back(302, [], route('jackpots.index'))->with('error', "This slot is not attached to {$cas->name} casino"); 
        }   

        return redirect()->back(302, [], route('jackpots.index'))->with('success', "Jackpot with id:{$jackpot_to_edit->id} updated!");    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jackpot::destroy($id);

        return redirect()->back(302, [], route('jackpots.index'))->with('success', 'Jackpot removed!');
    }
}
