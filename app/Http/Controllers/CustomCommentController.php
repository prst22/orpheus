<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravelista\Comments\CommentController;
use Laravelista\Comments\Comment;
use Illuminate\Support\Facades\Auth;
use Spatie\Honeypot\ProtectAgainstSpam;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Events\NewCommentCreated;

class CustomCommentController extends CommentController
{
    public function __construct()
    {
        $this->middleware('web');

        if (config('comments.guest_commenting') == true) {
            $this->middleware('auth')->except('store');
            $this->middleware(ProtectAgainstSpam::class)->only('store');
        } else {
            $this->middleware('auth');
        }
    }

    /**
     * Creates a new comment for given model.
     */
    public function store(Request $request)
    {
        // If guest commenting is turned off, authorize this action.
        // If guest commenting is turned off, authorize this action.
        if (Config::get('comments.guest_commenting') == false) {
            Gate::authorize('create-comment', Comment::class);
        }

        // Define guest rules if user is not logged in.
        if (!Auth::guard('web')->check() && !Auth::guard('admin')->user()) {
            $guest_rules = [
                'guest_name' => 'required|string|max:255',
                'guest_email' => 'required|string|email|max:255',
                'g-recaptcha-response' => 'required|string',
            ];
        }
        $messages = [
            'g-recaptcha-response.required' => 'Recapcha field is required',
        ];
        // Merge guest rules, if any, with normal validation rules.
        Validator::make($request->all(), array_merge($guest_rules ?? [], [
            'commentable_type' => 'required|string',
            'commentable_id' => 'required|string|min:1',
            'message' => 'required|string|max:2000'
        ]), $messages)->validate();

        $model = $request->commentable_type::findOrFail($request->commentable_id);

        $commentClass = Config::get('comments.model');
        $comment = new $commentClass;

        if (!Auth::guard('web')->check() && !Auth::guard('admin')->check()) {
            $comment->guest_name = $request->guest_name;
            $comment->guest_email = $request->guest_email;
        } else {
            if(Auth::guard('web')->check()){
                $comment->commenter()->associate(Auth::user());
            }
            if(Auth::guard('admin')->check()){
                $comment->commenter()->associate(Auth::guard('admin')->user());
            }
        }

        $comment->commentable()->associate($model);
        $comment->comment = $request->message;

        if(Auth::guard('web')->check() || Auth::guard('admin')->check()){

            $comment->approved = true;
            $comment->save();
            
            if($comment->commenter_type === 'App\User'){
                NewCommentCreated::dispatch($comment);
            }
            
            return redirect()->to(url()->previous() . '#comment-' . $comment->getKey())->with('success', __('New comment has been added'));
        }

        $comment->approved = !Config::get('comments.approval_required');
        $comment->save();
   
        NewCommentCreated::dispatch($comment);

        return $comment->approved ? redirect()->to(url()->previous() . '#comment-' . $comment->getKey())->with('success', __('New comment will be added after approval')) : redirect()->to(url()->previous())->with('success', __('New comment will be added after approval'));
        
    }

    /**
     * Updates the message of the comment.
     */
    public function update(Request $request, Comment $comment)
    {
        Gate::authorize('edit-comment', $comment);

        Validator::make($request->all(), [
            'message' => 'required|string|max:2000'
        ])->validate();

        $comment->update([
            'comment' => $request->message
        ]);
         
        return redirect()->to(url()->previous() . '#comment-' . $comment->getKey());
    }

    /**
     * Deletes a comment.
     */
    public function destroy(Comment $comment)
    {
        Gate::authorize('delete-comment', $comment);

        if (Config::get('comments.soft_deletes') == true) {
			$comment->delete();
		}
		else {
			$comment->forceDelete();
		}

        return redirect()->back()->with('success', 'Your comment has been deleted');
    }
}
