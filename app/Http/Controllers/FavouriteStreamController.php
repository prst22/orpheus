<?php

namespace App\Http\Controllers;

use App\User;
use App\Stream;
use Illuminate\Http\Request;
use App\Facades\FetchFavouriteStreams;

class FavouriteStreamController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(auth()->user()->id);
        $favourite_streams = FetchFavouriteStreams::fetch_or_get_streams_cache_var(NULL, true);
        $keys = array_keys($favourite_streams);
        $live_channels_db = Stream::whereIn('channel_id', $keys)->orderBy('channel_name', 'asc')->get();

        return view('favourite.streams')->with([
            'user' => $user,
            'streams' => $live_channels_db,
            'live_streams' => $favourite_streams,
            'no_favourite_streams' => empty($favourite_streams) ? true : false
        ]);
    }

    public function store($streamid)
    {
        $user_id = auth()->user()->id;
        $user = User::findOrFail($user_id);
        $user->favourite_streams()->syncWithoutDetaching([$streamid]);

        return redirect()->back()->with('success', 'Stream was added to your favourites!');
    }

    public function destroy($streamid)
    {
        $user_id = auth()->user()->id;
        $user = User::findOrFail($user_id);
        $user->favourite_streams()->detach($streamid);

        return redirect()->back()->with('success', 'Stream was removed from your favourite list!');
    }
}
