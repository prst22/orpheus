<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class TermsAndConditionsController extends Controller
{
    public function index()
    {
        $all_categories =  (new Category)->get_all_categories();

        return view('terms_and_conditions', ['all_categories' => $all_categories]);
    }
}
