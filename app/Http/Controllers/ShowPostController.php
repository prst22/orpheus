<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\Casino;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\Post\SortingParameters;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use App\Services\SortedArticlesTitle;
use App\Services\PageNumber;

class ShowPostController extends Controller
{
    use SortingParameters;

    public function __construct()
    {
        $this->per_page = 16;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('published', true)
            ->orderBy('sticky', 'DESC')
            ->orderBy('created_at', 'desc')
            ->paginate($this->per_page);
        $categories = new Category;
        $all_categories = $categories->get_all_categories();
        $pageStr = (new PageNumber($posts))->get();

        

        return view('index', [
            'posts' => $posts,
            'all_categories' => $all_categories,
            'title' => 'Articles, videos, streams, jackpot tracker' . $pageStr
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        $is_fav;
        $casinos = Casino::whereNotNull('rating')->inRandomOrder()->select('rating', 'name', 'url', 'image')->get();

        if (empty($post)) {
            abort(404); 
        }

        abort_if(!$post->published && !Auth::guard('admin')->check(), 404);

        if (!Auth::guard('admin')->check() && $this->isBrowser()) {
            $post_timestamp = $post->timestamps;
            $post->timestamps = false;
            $post->increment('views');
            $post->save();
            $post->timestamps = $post_timestamp;
        }

        if (Auth::guard('web')->check()) {
            $fav_posts = auth()->user()->favourite_posts()->pluck('post_id');
            $is_fav = $fav_posts->search($post->id) !== false ? true : false;
        }

        $categories = Post::where('slug', $slug)->first()->categories()->orderBy('name')->get();

        return view('post.show', [
            'post' => $post,
            'casinos' => $casinos,
            'categories' => $categories,
            'is_fav' => $is_fav ?? false,
        ]);
    }

    public function sortby(string $type, string $ascendance)
    {
        $sorting = $this->get_sorting_parameters($type, $ascendance);

        $posts = Post::where('published', true)->orderBy($sorting['type'], $sorting['ascendance'])->paginate($this->per_page);
        $categories = new Category;
        $all_categories = $categories->get_all_categories();
        $pageStr = (new PageNumber($posts))->get(); 
        $title = (new SortedArticlesTitle($sorting['type'], $sorting['ascendance']))->get() . $pageStr;

        return view('index', [
            'posts' => $posts,
            'all_categories' => $all_categories,
            'type' => $type,
            'ascendance' => $ascendance,
            'title' => $title
        ]);
    }

    public function category(string $slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $all_cat = new Category;
        $all_categories = $all_cat->get_all_categories($slug);
        $sorted_posts = Category::findOrFail($category->id)->posts()->where('published', true)->orderBy('created_at', 'desc')->paginate($this->per_page);
        $pageStr = (new PageNumber($sorted_posts))->get();

        return view('index', [
            'posts' => $sorted_posts,
            'all_categories' => $all_categories,
            'slug' => $slug,
            'title' => $category->name . ' ' . __('category') . ' ' . __('sorted by new') . $pageStr,
            'category_name' => $category->name
        ]);
    }

    public function category_sortby(string $slug, string $type, string $ascendance)
    {
        $sorting = $this->get_sorting_parameters($type, $ascendance);

        $category = Category::where('slug', $slug)->firstOrFail();
        $all_cat = new Category;
        $all_categories = $all_cat->get_all_categories($slug);
        $sorted_posts = Category::findOrFail($category->id)->posts()->where('published', true)->orderBy($sorting['type'], $sorting['ascendance'])->paginate($this->per_page);
        $sorting_type_str = (new SortedArticlesTitle($sorting['type'], $sorting['ascendance']))->get();
        $pageStr = (new PageNumber($sorted_posts))->get();
        $title = $category->name .' '. $sorting_type_str . $pageStr;
        
        return view('index', [
            'posts' => $sorted_posts,
            'category_name' => $category->name,
            'all_categories' => $all_categories,
            'slug' => $slug,
            'type' => $type,
            'ascendance' => $ascendance,
            'title' => $title,
        ]);
    }

    private function isBrowser() 
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            return preg_match( '/^(Mozilla|Opera|PSP|Bunjalloo|wii)/i', $_SERVER['HTTP_USER_AGENT'] ) && !preg_match( '/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'] );
        }

        return false;
    }
}
