<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravelista\Comments\Comment;
use DB;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->per_page = 20;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();

        $comments = DB::table('comments')
            ->join('posts', 'posts.id', '=', 'comments.commentable_id')->where('comments.commenter_id', $user->id)
            ->select(['comments.commentable_id', 'comments.commenter_id', 'comments.comment', 'comments.approved', 'comments.created_at', 'comments.id', 'posts.slug'])
            ->orderBy('comments.created_at', 'desc')
            ->paginate($this->per_page);

        return view('profile')->with([
            'user' => $user,
            'comments' => $comments
        ]);
    }
}
