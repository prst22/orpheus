<?php

namespace App\Http\Controllers;

use App\Casino;
use App\Slot;
use App\Jackpot;
// use App\CasinoData\Providers\Netent;
// use App\CasinoData\Providers\Yggdrasil;
// use App\CasinoData\Providers\RedTiger;
// use App\CasinoData\Providers\Microgaming;
// use App\CasinoData\Providers\PushGaming;
// use App\CasinoData\Casinos\BlueprintJackpotKing;
// use App\CasinoData\Providers\QuickSpin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class JackpotTrackerController extends Controller
{
    public function index()
    {
        // $all_casino_ids = Casino::tracked()->get()->pluck('id');
        // $n = new Netent($all_casino_ids->all());
        // $n();
        // $y = new Yggdrasil($all_casino_ids->all());
        // $y();
        // $redTiger = new RedTiger($all_casino_ids->all());
        // $redTiger();
        // $microgaming = new Microgaming($all_casino_ids->all());
        // $microgaming();
        // $blueprint = new BlueprintJackpotKing();
        // $blueprint();
        // $push_gaming = new PushGaming($all_casino_ids->all());
        // $push_gaming();
        // $quickspin = new QuickSpin($all_casino_ids->all());
        // $quickspin();

        $initial_casino_query = Casino::tracked()->orderBy('name', 'ASC');
        $casinos = $initial_casino_query->select('id', 'name', 'image', 'slug', 'url')->get();
        $slots = Slot::select('id', 'name', 'provider')->orderBy('name', 'ASC')->get();
        $casino_id = $initial_casino_query->first()->id;
        $slot_id = $initial_casino_query->first()->activeSlots()->orderBy('name', 'ASC')->first()->id;
        $provider_images = [
            'netent'                    => URL::asset('assets') . '/casino/providers/netent.png',
            'yggdrasil'                 => URL::asset('assets') . '/casino/providers/yggdrasil.png',
            'redtiger'                  => URL::asset('assets') . '/casino/providers/redtiger.png',
            'microgaming'               => URL::asset('assets') . '/casino/providers/microgaming.png',
            'blueprint'                 => URL::asset('assets') . '/casino/providers/blueprint-jk.png',
            'pushgaming'                => URL::asset('assets') . '/casino/providers/pushgaming.png',
            'quickspin'                 => URL::asset('assets') . '/casino/providers/quickspin.png',
        ];

        return view('jackpot_tracker', [
            'casinos' => $casinos,
            'slots' => $slots,
            'casino_id' => $casino_id,
            'slot_id' => $slot_id,
            'provider_images' => $provider_images,
        ]);
    }
}
