<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\Casino as CasinoResource;
use App\Casino;
use App\Http\Requests\CasinoApiRequest;

class CasinoApiController extends Controller
{
    public function __invoke(CasinoApiRequest $request)
    {   
        $casino_id_query_str = request()->query('casino_id');
        $casino_id = !empty($casino_id_query_str) ? $casino_id_query_str : '';

        try {
            return CasinoResource::collection(Casino::tracked()->find($casino_id)->activeSlots()->get());
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Not found',
                'message' => 'Resource not found'
              ], 404);
        }  
    }
}
