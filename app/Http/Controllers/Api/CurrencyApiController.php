<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Cookie;

class CurrencyApiController extends Controller
{
    private array $supported_currencies;
    private array $currencies_symbols;

    public function __construct() {
        $this->supported_currencies = ['EUR', 'USD', 'CAD', 'GBP', 'AUD'];
		$this->currencies_symbols = [
            'EUR' => '€', 
            'USD' => '$', 
            'CAD' => 'CA$', 
            'GBP' => '£', 
            'AUD' => 'A$'
        ];
    }
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$currency = strtoupper($request->query('code'));

		$currency_api_key = config('services.currency_api.key');
		$currency_api_url = "https://freecurrencyapi.net/api/v2/latest?apikey={$currency_api_key}&base_currency=EUR";
		

		if (Cookie::get('XSRF-TOKEN') === null || empty(Cookie::get('XSRF-TOKEN') )) {
			return response()->json([
				'error' 	=> 'Unauthorized',
				'message' 	=> 'Your are not authorized'
			  ], 401);
		}

		if (empty($currency)) {
			return [
                'code' 	    => 'EUR',
                'symbol'    => '€',
                'value' 	=> 1,
            ];
		}
		
		if (!in_array($currency, $this->supported_currencies)) {
			return response()->json([
				'error' 	=> 'Not found',
				'message' 	=> 'This currency is not supported'
			  ], 404);
		}

		try {

			$currency_arr;

			if (Cache::has('currency_cache')) {
				$currency_arr = Cache::get('currency_cache');
			} else {
				$response = Http::withOptions([
    				'verify' => false,
				])->acceptJson()->get($currency_api_url);
				
				if ($response->failed()) {

					return response()->json([
						'error' 	=> 'Bad Gateway',
						'message' 	=> 'Something went wrong'
					], 502);

				}

				$currency_arr = $response->json('data');

				Cache::put('currency_cache', $currency_arr, 21600);
			}

			return $this->FilteredCurrencyResponce($currency_arr, $currency);

		} catch (\Throwable $th) {
			return response()->json([
				'error'     => 'Error',
				'message'   => $th->getMessage()
			  ], 500);
		}
	}

	private function FilteredCurrencyResponce(array $currencies, string $needle): array
	{
		//return currency value relative to 1 EUR
		if (!empty($currencies) && isset($currencies[$needle])) {
			return [
				'code'      => $needle,
                'symbol'    => $this->currencies_symbols[$needle],
				'value'     => round($currencies[$needle], 2)
			];
		} 

		return [
			'code' 	    => 'EUR',
            'symbol'    => '€',
			'value' 	=> 1,
		];
	}
}
