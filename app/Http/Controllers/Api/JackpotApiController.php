<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\Jackpot as JackpotResource;
use App\Jackpot;
use App\Http\Requests\JackpotApiRequest;
use Illuminate\Support\Facades\Cache;
use Cookie;

class JackpotApiController extends Controller
{
    public function __invoke(JackpotApiRequest $request)
    {
        if (Cookie::get('XSRF-TOKEN') === null) {
            return response()->json([
                'error' => 'Unauthorized',
                'message' => 'Your session has expired. Please reload the page'
              ], 401);
        }
        
        $casino_id_query_str = request()->query('casino_id');
        $casino_ids_arr = !empty($casino_id_query_str) ? explode( ",", $casino_id_query_str) : [];
        $slot_id_query_str = request()->query('slot_id');
        $slot_id = !empty($slot_id_query_str) ? explode( ",", $slot_id_query_str) : []; 

        //one slot jackpot in all casinos
        if (count($casino_ids_arr) > 1) {
            $one_slot_jp_in_all_casionos_data = Cache::remember("one_slot_jp_in_all_casionos_data_{$slot_id[0]}", 420, function () use ($casino_ids_arr, $slot_id) {
                return (JackpotResource::collection(Jackpot::whereIn('casino_id', $casino_ids_arr)->whereIn('slot_id', $slot_id)->get()))
                ->response()
                ->header('Access-Control-Allow-Origin', url('/'))
                ->header('Vary', 'Origin');  
            });

            return $one_slot_jp_in_all_casionos_data;
        } 
        
        return (JackpotResource::collection(Jackpot::whereIn('casino_id', $casino_ids_arr)->whereIn('slot_id', $slot_id)->get()))
            ->response()
            ->header('Access-Control-Allow-Origin', url('/'))
            ->header('Vary', 'Origin');  
    }
}
