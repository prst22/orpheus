<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\Attachment as AttachmentResource;
use App\Attachment;
use Illuminate\Support\Facades\Auth;

class AttachmentApiController extends Controller
{
    public function __construct()
    {
        $this->per_page = 36;
    }

    public function __invoke(Request $request)
    { 
        return AttachmentResource::collection(Attachment::orderBy('created_at', 'desc')->paginate($this->per_page));
    }
}
