<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogleProvider()
    { 
        $prev_route_name = app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName(); 
        $prev_route_name == 'posts.show' ? session()->put('google_redirect', url()->previous()) : null;
        
        return Socialite::driver('google')->redirect();
    }

    public function redirectToFacebookProvider()
    {
        $prev_route_name = app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName(); 
        $prev_route_name == 'posts.show' ? session()->put('facebook_redirect', url()->previous()) : null;
        
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleGoogleProviderCallback()
    {
        
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect()->route('login');
        }
        
        if(explode("@", $user->email)[1] !== 'gmail.com'){ 
            return redirect()->route('login');
        }
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();

        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->save();
            auth()->login($newUser, true);
        }
        if(!session()->has('google_redirect')){
            return redirect()->intended(route('home'));
        }
        $redirect = session()->get('google_redirect');
        session()->forget('google_redirect');
        
        return redirect($redirect);
    }

    public function handleFacebookProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            return redirect()->route('login');
        }
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->facebook_id     = $user->id;
            $newUser->save();
            auth()->login($newUser, true);
        }
        if(!session()->has('facebook_redirect')){
            return redirect()->intended(route('home'));
        }
        $redirect = session()->get('facebook_redirect');
        session()->forget('facebook_redirect');

        return redirect($redirect);
    }
}
