<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }
    
    protected function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email|max:255',
            'password' => 'required|string|min:8'
        ]);

        $user_loggin_is_successful = Auth::guard('admin')->attempt($this->credentials($request), $request->filled('remember'));
        // Attempt to log the user in
        if ($user_loggin_is_successful) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('dashboard'));
        } 
        
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    
    protected function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('home');
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

}
