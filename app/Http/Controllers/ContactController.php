<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactFormRequest;
use \ReCaptcha\ReCaptcha;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function submitForm(ContactFormRequest $formData)
    {
        $secretKey = config('services.google.recapcha_secret');
        $recaptcha = new ReCaptcha($secretKey);
        $resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])->verify($formData['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        
        if ($resp->isSuccess()) {

            $messageContent = [ 
                'name' => strip_tags(request('name')),
                'email' => strip_tags(request('email')),
                'message-body' => strip_tags(request('message-body')),
            ];

            try {
                Mail::to(config('services.email.main'))->send(new ContactForm($messageContent)); 
            } catch(Exception $e) {
                return redirect()->back()->with('error', 'Something went wrong'. $e->getMessage());
            }

            return redirect()->back()->with('success', 'You message have been send!');
            
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }
}
