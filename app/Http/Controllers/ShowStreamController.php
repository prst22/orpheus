<?php

namespace App\Http\Controllers;

use App\Stream;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Facades\FetchAllStreams;
use App\Facades\FetchCategoryStreams;
use App\Services\CountLiveStreams;
use App\Exceptions\FetchStreamsException;

class ShowStreamController extends Controller
{
    public function index()
    {
        $live_channels_db = [];
        $live_streams = [];
        $total_poker_streams = 0;
        $total_casino_streams = 0;
        $total_streams = 0;
        $message = '';

        try {
            $live_streams = FetchAllStreams::fetch_or_get_streams_cache_var();
            $keys = array_keys($live_streams);
            $live_channels_db = Stream::whereIn('channel_id', $keys)->orderBy('channel_name', 'asc')->get();
            $total_poker_streams = (new CountLiveStreams($live_streams))->poker;
            $total_casino_streams = (new CountLiveStreams($live_streams))->casino;
            $total_streams = (new CountLiveStreams($live_streams))->total;
        } catch (FetchStreamsException $exception) {
            $message = $exception->getMessage();
        } catch (\Throwable $th) {
            $message = 'There are no live streams right now';
        }

        return view('stream.all_streams', [
            'streams' => $live_channels_db,
            'live_streams' => $live_streams,
            'total_poker_streams' => $total_poker_streams,
            'total_casino_streams' => $total_casino_streams,
            'total_streams' => $total_streams,
            'message' => $message
        ]);
    }

    public function show($slug)
    {
        $stream = Stream::where('slug', $slug)->firstOrFail();
        $is_fav;

        if (Auth::guard('web')->check()) {
            $fav_streams = auth()->user()->favourite_streams()->pluck('stream_id');
            $is_fav = $fav_streams->search($stream->id) !== false ? true : false;
        }

        return view('stream.show', [
            'stream' => $stream,
            'description' => $stream->description,
            'is_fav' => $is_fav ?? false
        ]);
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $live_streams = FetchCategoryStreams::fetch_or_get_streams_cache_var(str_slug($slug, '_'));
        $keys = array_keys($live_streams);
        $live_channels_db = Stream::whereIn('channel_id', $keys)->orderBy('channel_name', 'asc')->get();
        $total_streams = (new CountLiveStreams($live_streams))->total;

        return view('stream.category_streams', [
            'streams' => $live_channels_db,
            'category_name' => $category->name,
            'live_streams' => $live_streams,
            'total_streams' => $total_streams
        ]);
    }
}
