<?php 

namespace App\Http\Traits\Post;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait SavePostThumbnails
{
    private function save_post_thumbnails($thumbnail_src, string $post_title)
    {
        $uploaded_source_file_basename = str_slug($post_title, '-') . '.webp';
       
        $loop_post_image_sm = "thumbnails/post/l/small/{$uploaded_source_file_basename}";
        $loop_post_image_md = "thumbnails/post/l/medium/{$uploaded_source_file_basename}";
        $loop_post_image_lg = "thumbnails/post/l/large/{$uploaded_source_file_basename}";

        $single_post_image_sm = "thumbnails/post/s/small/{$uploaded_source_file_basename}";
        $single_post_image_md = "thumbnails/post/s/medium/{$uploaded_source_file_basename}";
        $single_post_image_lg = "thumbnails/post/s/large/{$uploaded_source_file_basename}";
        
        $picture_for_single = Image::make($thumbnail_src)->encode('webp', 60);
     
        $picture_for_loop = Image::make($thumbnail_src)->encode('webp', 60);
        
        $single_post_lg = $picture_for_single->fit(1080, 360, function ($constraint) {
            $constraint->upsize();
        });

        Storage::disk('s3')->put($single_post_image_lg, $single_post_lg->stream());
        Storage::disk('s3')->setVisibility($single_post_image_lg, 'public');

        $single_post_md = $picture_for_single->fit(540, 180, function ($constraint) {
            $constraint->upsize();
        });

        Storage::disk('s3')->put($single_post_image_md, $single_post_md->stream());
        Storage::disk('s3')->setVisibility($single_post_image_md, 'public');

        $single_post_sm = $picture_for_single->fit(270, 90, function ($constraint) {
            $constraint->upsize();
        });

        Storage::disk('s3')->put($single_post_image_sm, $single_post_sm->stream());
        Storage::disk('s3')->setVisibility($single_post_image_sm, 'public');

        // loop post thumbnails
        $large_thumbnail_src = $picture_for_loop->fit(836, 418, function ($constraint) {
            $constraint->upsize();
        }); 

        Storage::disk('s3')->put($loop_post_image_lg, $large_thumbnail_src->stream());
        Storage::disk('s3')->setVisibility($loop_post_image_lg, 'public');

        $medium_thumbnail_src = $picture_for_loop->fit(680, 340, function ($constraint) {
            $constraint->upsize();
        });

        Storage::disk('s3')->put($loop_post_image_md, $medium_thumbnail_src->stream());
        Storage::disk('s3')->setVisibility($loop_post_image_md, 'public');

        $small_thumbnail_src = $picture_for_loop->fit(278, 139, function ($constraint) {
            $constraint->upsize();
        });

        Storage::disk('s3')->put($loop_post_image_sm, $small_thumbnail_src->stream());
        Storage::disk('s3')->setVisibility($loop_post_image_sm, 'public');

        $picture_for_single = null;
        $picture_for_loop = null;

        return [
                'l' => [
                    'sm' => ['url' => Storage::disk('s3')->url($loop_post_image_sm)],
                    'md' => ['url' => Storage::disk('s3')->url($loop_post_image_md)],
                    'lg' => ['url' => Storage::disk('s3')->url($loop_post_image_lg)],
                ],
                's' => [
                    'sm' => ['url' => Storage::disk('s3')->url($single_post_image_sm)],
                    'md' => ['url' => Storage::disk('s3')->url($single_post_image_md)],
                    'lg' => ['url' => Storage::disk('s3')->url($single_post_image_lg)],    
                ]

            ];
    }
}