<?php 

namespace App\Http\Traits\Post;

trait SortingParameters
{
    private function get_sorting_parameters($type, $ascendance)
    {
        $sorting_type = ($type === 'date' || $type === 'popularity') ? $type : 'date';

        if($sorting_type === 'popularity'){
            $ascendance_type = ($ascendance === 'mostviewed' || $ascendance === 'leastviewed') ? $ascendance : 'mostviewed';
            $sort_by = 'views';
            $sort_by_ascendance = ($ascendance_type === 'mostviewed') ? 'desc' : 'asc';
        }elseif($sorting_type === 'date'){
            $ascendance_type = $ascendance;
            $sort_by = 'created_at';
            $sort_by_ascendance = 'asc';
        }else{
            $sort_by = 'created_at';
            $sort_by_ascendance = 'desc';
        }
        
        return ['type' => $sort_by, 'ascendance' => $sort_by_ascendance];
    }
}