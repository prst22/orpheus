<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
trait RemoveImage
{
    private function remove_image(array $images)
    {
        
        if (isset($images['l']) || isset($images['s'])) {

            foreach ($images as $imageset) {
                foreach ($imageset as $image) {
                    
                    $url = parse_url($image['url']);
                    $path = Str::replaceFirst('/storage', '', $url['path']);
        
                    if (!empty($path)) {
                        if (Storage::disk('public')->exists($path)) {
                            Storage::disk('public')->delete($path);
                        }

                        if (Storage::disk('s3')->exists($path)) {
                            Storage::disk('s3')->delete($path);
                        }
                    }
                }
                $image_url_arr = isset($image['url']) ? $image['url'] : $image;
            }
            
        } else {

            foreach ($images as $image) {
                
                $url = parse_url($image);
                $path = Str::replaceFirst('/storage', '', $url['path']);
                
                if (!empty($path)) {
                    if (Storage::disk('public')->exists($path)) {
                        Storage::disk('public')->delete($path);
                    }

                    if (Storage::disk('s3')->exists($path)) {
                        Storage::disk('s3')->delete($path);
                    }
                }
            }
            
        }
    }
}