<?php

namespace App\Http\Traits\Casino;

use Illuminate\Http\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait SaveCasinoBanner
{
    private function save_casino_banner($image_blob, string $casino_name)
    {
        //create lg size banner
        $intervention_ins_banner_lg = Image::make($image_blob)->encode('webp', 60);
        
        $file_name_lg = Str::of("{$casino_name}-banner-1360w-lg")->slug('-')->append(".webp");
        $file_name_md = Str::of("{$casino_name}-banner-836w-md")->slug('-')->append(".webp");
        $file_name_sm = Str::of("{$casino_name}-banner-500w-sm")->slug('-')->append(".webp");

        $intervention_ins_banner_lg->fit(1360, 454);
        $intervention_ins_banner_lg->save(Storage::disk('public')->path("casino-images/branding/banners/{$file_name_lg}"));

        //create md size banner
        $intervention_ins_banner_lg->fit(836, 279);
        $intervention_ins_banner_lg->save(Storage::disk('public')->path("casino-images/branding/banners/{$file_name_md}"));

         //create sm size banner
        $intervention_ins_banner_lg->fit(400, 133);
        $intervention_ins_banner_lg->save(Storage::disk('public')->path("casino-images/branding/banners/{$file_name_sm}"));

        $this->banner_arr = [
            'lg' => Storage::disk('public')->url("casino-images/branding/banners/{$file_name_lg}"),
            'md' => Storage::disk('public')->url("casino-images/branding/banners/{$file_name_md}"),
            'sm' => Storage::disk('public')->url("casino-images/branding/banners/{$file_name_sm}")
        ];
        
        $intervention_ins_banner_lg = null;
        // Storage::disk('public')->delete($saved_casino_banner_lg);
    }
}