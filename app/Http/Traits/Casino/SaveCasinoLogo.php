<?php

namespace App\Http\Traits\Casino;

use Illuminate\Http\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait SaveCasinoLogo
{
    private function save_casino_logo($image_blob, string $casino_name) 
    {
        $intervention_ins_logo = Image::make($image_blob)->encode('webp', 75);
        $intervention_ins_logo->resize(null, 300, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
       
        $lg_logo_width = $intervention_ins_logo->width();
        $file_name_lg = Str::of("{$casino_name}-logo")->append("-{$lg_logo_width}w")->slug('-')->append('.webp');

        //save to local storage 
        Storage::disk('public')->put("casino-images/branding/logos/{$file_name_lg}", $intervention_ins_logo->stream());
                
        //create md size logo
        $intervention_ins_logo->resize(null, 100, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
       
        $md_logo_width = $intervention_ins_logo->width();
        $file_name_md = Str::of("{$casino_name}-logo")->append("-{$md_logo_width}w")->slug('-')->append('.webp');

        Storage::disk('public')->put("casino-images/branding/logos/{$file_name_md}", $intervention_ins_logo->stream());

        //create md size logo
        $intervention_ins_logo->resize(null, 50, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
       
        $sm_logo_width = $intervention_ins_logo->width();
        $file_name_sm = Str::of("{$casino_name}-logo")->append("-{$sm_logo_width}w")->slug('-')->append('.webp');

        //save to s3 storage and set visibility to public
        Storage::disk('public')->put("casino-images/branding/logos/{$file_name_sm}", $intervention_ins_logo->stream());

        $this->logo_arr = [
            'lg' => Storage::disk('public')->url("casino-images/branding/logos/{$file_name_lg}"),
            'md' => Storage::disk('public')->url("casino-images/branding/logos/{$file_name_md}"),
            'sm' => Storage::disk('public')->url("casino-images/branding/logos/{$file_name_sm}")
        ];
        
        $intervention_ins_logo = null;
    } 
}