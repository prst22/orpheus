<?php

namespace App\Http\Traits\Stream;

trait GetTwitchAppAccessToken
{
    private function get_token()
    {
        $gettoken_url = 'https://id.twitch.tv/oauth2/token?client_id=wg5z0849nslv0ux5vvudojl8kb2bq6&client_secret=j5e7po989wteesdhmlbih8e1ldp6ul&grant_type=client_credentials';
        $twitch_curl = curl_init();
        curl_setopt($twitch_curl, CURLOPT_URL, $gettoken_url);
        curl_setopt($twitch_curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($twitch_curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($twitch_curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($twitch_curl, CURLOPT_POST, 1);
        $response = json_decode(curl_exec($twitch_curl), true);
        curl_close($twitch_curl);
        $this->token = $response['access_token'];
        $this->token_expires_in = $response['expires_in'];
    }
}