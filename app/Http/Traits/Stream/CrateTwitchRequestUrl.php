<?php

namespace App\Http\Traits\Stream;

trait CrateTwitchRequestUrl
{
    protected function crateTwitchRequestUrl($url, $channels)
    {
        $url_arr = [];
        $amount_of_streams = 100;

        $groups = array_chunk($channels->toArray(), $amount_of_streams);
        
        foreach ($groups as $key => $group) {

            $chunked_url = $url;

            foreach($group as $index => $channel){
                

                if(isset($channel['channel_id']) && !empty($channel['channel_id'])){
                    
                    $chunked_url .= 'user_id='. $channel['channel_id'] . '&';
                }
                
            }
            
            $url_arr[] = rtrim($chunked_url, '&');
            
        }
      
        return $url_arr;
    }
}