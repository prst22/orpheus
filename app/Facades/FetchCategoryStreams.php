<?php

namespace App\Facades;

class FetchCategoryStreams
{
    protected static function resolveFacade($name)
    {
        return app()->make($name);
    }

    public static function __callStatic($method, $args)
    {
        return(self::resolveFacade('App\Facades\FetchCategoryStreams'))->$method(...$args);
    }
}