<?php

namespace App\Facades;

class FetchAllStreams
{
    protected static function resolveFacade($name)
    {
        return app()->make($name);
    }

    public static function __callStatic($method, $args)
    {
        return(self::resolveFacade('App\Facades\FetchAllStreams'))->$method(...$args);
    }
}