<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $messageContent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $messageContent)
    {
        $this->messageContent = $messageContent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_from = config('mail.from.address');
        return $this->from($mail_from)->subject('Contact Form Gambling Central')->markdown('emails.contact_form');
    }
}
