<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LotteryJackpot extends Model
{
    use HasFactory;

    protected $casts = [
        'jackpot_data' => 'array'
    ];

    protected $fillable = ['name', 'type', 'draw_date', 'jackpot_data'];

    public $timestamps = false;

    public function getFormatedAmount(string $type = 'prize_pool')
    {   
        $cur = $this->jackpot_data['extra']['currency'] === 'USD' ? '$' : '£';

        if ($type === 'cash_value' && isset($this->jackpot_data['cash_value'])) {
            return sprintf('%s%s', $cur, preg_replace('/\B(?=(\d{3})+(?!\d))/', ',', $this->jackpot_data['cash_value']));
        }
        if ($type === 'prize_pool' && isset($this->jackpot_data['prize_pool'])) {
            return sprintf('%s%s', $cur, preg_replace('/\B(?=(\d{3})+(?!\d))/', ',', $this->jackpot_data['prize_pool']));
        }
    }
    
    public function getFormatedDate(string $separator = '/')
    {
        if (isset($this->draw_date)) {
            return Carbon::parse($this->draw_date)->format("d{$separator}m{$separator}Y");
        } 
    }
}
