<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Stream extends Model
{
    // use Searchable;

    public function author()
    {
        return $this->belongsTo('App\Admin');
    }
    
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function liked_by()
    {
        return $this->belongsToMany('App\User');
    }

    public function get_categories($id)
    {
        $categories = Stream::find($id)->categories()->orderBy('name')->get();
        
        if($categories->isEmpty()){
            return false;
        }

        foreach($categories as $category){
            $category_link = sprintf( 
                '<a href="%1$s" title="%2$s" class="categories__link">%3$s</a>',
                sprintf( '%s', route( 'streams.category', $category->slug ) ),
                sprintf( __( 'View all articles in %s'), $category->name ),
                $category->name
            );
            $category_names_arr[] = $category_link;
        }
        
        $categories_string = implode(', ', $category_names_arr);

        return $categories_string;
    }

    // public function toSearchableArray()
    // {

    //     $array['channel_name'] = $this->channel_name;
    //     $array['description'] = $this->description;
    //     $array['created_at'] = $this->created_at;

    //     return $array;
    // }
}
