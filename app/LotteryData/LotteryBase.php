<?php 

namespace App\LotteryData;

abstract class LotteryBase
{
    abstract protected function makeApiRequest();
    
    protected function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}