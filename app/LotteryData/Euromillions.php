<?php 

namespace App\LotteryData;

use Illuminate\Support\Facades\Http;
use App\LotteryData\LotteryBase;
use App\LotteryJackpot;
use \Carbon\Carbon;
use Illuminate\Support\Str;

class Euromillions extends LotteryBase
{
    function __construct()
    {
        $this->url = 'https://www.national-lottery.co.uk/results/euromillions/draw-history';
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
        $this->setDbData();  
    }

    protected function makeApiRequest()
    {
        $this->makeJackpotRequest();
    }

    private function makeJackpotRequest()
    {
        try {
            $response = Http::retry(2, 55)->get($this->url);
        } catch (\Throwable $th) {
            return null;
        }
       
        if ($response->status() !== 200) {
            return null;
        }

        $this->euromillions_page = $response->body();
    }

    private function getEuromillionsJackpotData()
    {
        $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        $doc->loadHTML($this->euromillions_page);
        $xpath = new \DOMXPath($doc);
        $nodes = $xpath->query("//div[@class='table_cell_padding']/span[@class='table_cell_block']");

        if (count($nodes) <= 0) {
           return null;
        }

        $date = Carbon::parse(trim($nodes[0]->nodeValue))->format('Y-m-d');
        $jackpot_amount_str = trim($nodes[1]->nodeValue);
        $jackpot_amount_str_2 = Str::replace(',', '', $jackpot_amount_str);
        $jackpot_amount = (int) Str::replace('£', '', $jackpot_amount_str_2);

        $ball_numbers_str = Str::replace(' ', '', trim($nodes[2]->nodeValue));
        $winning_numbers_arr = explode('-', $ball_numbers_str);

        $lucky_starts_str = Str::replace(' ', '',trim($nodes[3]->nodeValue));
        $lucky_starts_arr = explode('-', $lucky_starts_str);

        return [
            'draw_date' => $date,
            'jackpot_data' => [
                'winning_numbers' => $winning_numbers_arr,
                'prize_pool' => $jackpot_amount,
                'extra' => [
                    'lucky_stars' => $lucky_starts_arr
                ]
            ] 
        ];
    }

    private function setDbData()
    {
        $jackpot = $this->getEuromillionsJackpotData();
        
        if (!isset($jackpot)) {
            return;
        }
       
        $last_jackpot_in_db = LotteryJackpot::where('name', $this->getClassName())->latest('draw_date')->first();

        if (!isset($last_jackpot_in_db)) {
            $new_jp = new LotteryJackpot();
            $new_jp->name = $this->getClassName();
            $new_jp->type = 'national';
            $new_jp->draw_date = $jackpot['draw_date'];
            $new_jp->jackpot_data = [
                'winning_numbers'   => $jackpot['jackpot_data']['winning_numbers'],
                'prize_pool'        => $jackpot['jackpot_data']['prize_pool'], 
                'cash_value'        => null,
                'extra'             => [
                    'lucky_stars'   => $jackpot['jackpot_data']['extra']['lucky_stars'],
                    'currency'      => 'GBP'
                ]
            ];
            
            $new_jp->save(); 
        } else {

            if ($last_jackpot_in_db->draw_date === $jackpot['draw_date']) {
                return;
            } else {
                $new_jp = new LotteryJackpot();
                $new_jp->name = $this->getClassName();
                $new_jp->type = 'national';
                $new_jp->draw_date = $jackpot['draw_date'];
                $new_jp->jackpot_data = [
                    'winning_numbers'   => $jackpot['jackpot_data']['winning_numbers'],
                    'prize_pool'        => $jackpot['jackpot_data']['prize_pool'], 
                    'cash_value'        => null,
                    'extra'             => [
                        'lucky_stars'   => $jackpot['jackpot_data']['extra']['lucky_stars'],
                        'currency'      => 'GBP'
                    ]
                ];
                
                $new_jp->save();    
            }
        }
    }
}