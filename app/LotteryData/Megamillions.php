<?php 

namespace App\LotteryData;

use Illuminate\Support\Facades\Http;
use App\LotteryData\LotteryBase;
use App\LotteryJackpot;
use \Carbon\Carbon;

use Illuminate\Support\Str;

class Megamillions extends LotteryBase
{
    function __construct()
    {
        $this->numbers_url = 'https://data.ny.gov/resource/5xaw-6ayf.json';
        $this->megamillions_numbers_arr = null;
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
        $this->setDbData();  
    }

    protected function makeApiRequest()
    {
        try {
            $response = Http::retry(2, 55)->get($this->numbers_url);
        } catch (\Throwable $th) {
            return null;
        }
        
        if ($response->status() !== 200) {
            return null;
        }

        $this->megamillions_numbers_arr = $response->json();
    }

    private function getMegamillionsJackpotData()
    { 
        if (!isset($this->megamillions_numbers_arr)) {
            return null;
        }

        $date = Carbon::parse($this->megamillions_numbers_arr[0]['draw_date'])->format('Y-m-d');
        $multiplier = Str::replaceFirst('0', '', $this->megamillions_numbers_arr[0]['multiplier']);
        $winning_numbers_arr = explode(' ', $this->megamillions_numbers_arr[0]['winning_numbers']);
        
        $prize_pool = $this->getJpAmount($date);

        if ($prize_pool === null) {
            return null;
        }
        
        return [
            'name'      => $this->getClassName(),
            'type'      => 'national',
            'draw_date' => $date,
            'jackpot_data' => [
                'winning_numbers'   => $winning_numbers_arr,
                'prize_pool'        => (int) $prize_pool, 
                'extra'     => [
                    'multiplier'    => $multiplier,
                    'm_ball'        => $this->megamillions_numbers_arr[0]['mega_ball'],
                    'currency'      => 'USD'
                ]
            ],
        ];
    }

    private function getJpAmount($date) 
    {
        if (!isset($date)) {
            return null;
        }

        $url = "https://nylottery.ny.gov/drupal-api/api/v2/winning_numbers?_format=json&nid=16&page=0&start_date={$date}&winning_numbers=";
        
        try {
            $response = Http::retry(2, 55)->get($url);
        } catch (\Throwable $th) {
            return null;
        }
        
        if ($response->status() !== 200) {
            return null;
        }

        $stats = $response->json();

        if (!isset($stats['rows'][0])) {
            return null;
        }
        
        if ($stats['rows'][0]['date'] !== $date) {
            return null;
        }

        return $stats['rows'][0]['jackpot'];
    }

    private function setDbData()
    {
        $data = $this->getMegamillionsJackpotData();

        if (!isset($data)) {
            return;
        }

        $last_jackpot_in_db = LotteryJackpot::where('name', $this->getClassName())->latest('draw_date')->first();
        
        if (!isset($last_jackpot_in_db)) {
            $new_jp = new LotteryJackpot();
            $new_jp->name = $data['name'];
            $new_jp->type = $data['type'];
            $new_jp->draw_date = $data['draw_date'];
            $new_jp->jackpot_data = [
                'winning_numbers'   => $data['jackpot_data']['winning_numbers'],
                'prize_pool'        => $data['jackpot_data']['prize_pool'], 
                'extra'             => [
                    'multiplier'    => $data['jackpot_data']['extra']['multiplier'],
                    'm_ball'        => $data['jackpot_data']['extra']['m_ball'],
                    'currency'      => 'USD'
                ]
            ];
            
            $new_jp->save();
        } else {
            $last_jackpot_draw_date_in_db = $last_jackpot_in_db->draw_date;

            if ($last_jackpot_draw_date_in_db === $data['draw_date']) {
                return;
            } else {
                $new_jp = new LotteryJackpot();
                $new_jp->name = $data['name'];
                $new_jp->type = $data['type'];
                $new_jp->draw_date = $data['draw_date'];
                $new_jp->jackpot_data = [
                    'winning_numbers'   => $data['jackpot_data']['winning_numbers'],
                    'prize_pool'        => $data['jackpot_data']['prize_pool'], 
                    'extra'             => [
                        'multiplier'    => $data['jackpot_data']['extra']['multiplier'],
                        'm_ball'        => $data['jackpot_data']['extra']['m_ball'],
                        'currency'      => 'USD'
                    ]
                ];
        
                $new_jp->save();    
            }
        }
    }
}