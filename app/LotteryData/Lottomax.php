<?php 

namespace App\LotteryData;

use Illuminate\Support\Facades\Http;
use App\LotteryData\LotteryBase;
use App\LotteryJackpot;
use \Carbon\Carbon;
use Illuminate\Support\Str;

class Lottomax extends LotteryBase
{
    function __construct()
    {
        $this->url = 'https://www.wclc.com/lotto-max-prize-details.htm';
        $this->lottomax_page = null;
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
        $this->setDbData();  
    }

    protected function makeApiRequest()
    {
        $this->makeJackpotRequest();
    }

    private function makeJackpotRequest()
    {
        try {
            $response = Http::retry(2, 55)->get($this->url);
        } catch (\Throwable $th) {
            return null;
        }
       
        if ($response->status() !== 200) {
            return null;
        }
        $this->lottomax_page = $response->body();
    }

    private function getLottoMaxJackpotData()
    {
        $doc = new \DOMDocument('1.0', 'UTF-8');
        libxml_use_internal_errors(true);
        $doc->loadHTML($this->lottomax_page);
        $xpath = new \DOMXPath($doc);
        $date_node = $xpath->query("//div[@class='pastWinNumDate floatLeft']/h4");
        $winning_numbers_nodes = $xpath->query("//ul[@class='pastWinNumbers']/li");
        $jackpot_amount_node = $xpath->query("//div[@class='pastWinNumJackpot floatRight']/h3");
        $max_millions_nodes = $xpath->query("//div[@class='prizeBreadkownLottoMaximillionsNum']");
        $extra_number_node = $xpath->query("//div[@class='pastWinNumExtra']");
        $max_millions_numbers = null;
        
        if (count($date_node) <= 0) {
            return null;
        }
       
        // winning numbers
        foreach ($winning_numbers_nodes as $value) {
            if (trim($value->nodeValue) !== '') {
                $winning_numbers[] = $value->nodeValue;
            } 
        }
       
        try {
            // date string
            $date_str = trim($date_node[0]->textContent);
            $date = Carbon::parse($date_str)->format('Y-m-d');
        } catch (\Throwable $th) {
            return null;
        }
        
        $winning_numbers_arr = $winning_numbers;
        $winning_numbers_arr[7] = substr(end($winning_numbers), 5, 2);
   
        // check if winning numbers exist
        if (count($winning_numbers_arr) <= 0) {
            return null; 
        }
        
        $jackpot_str = trim(substr(trim($jackpot_amount_node[0]->nodeValue), 9));
        $jackpot_str = Str::replace('$', '', $jackpot_str);
        $jackpot_str = Str::replace(',', '', $jackpot_str);
        $jackpot_amount = (int) $jackpot_str;
        
        $is_max_millions = count($max_millions_nodes) > 0 ? true : false;
        
        //check if there is max millions numbers
        if ($is_max_millions) {

            foreach ($max_millions_nodes as $number) {
                $max_millions_numbers[] = trim($number->textContent);
            }

        }

        return [
            'draw_date' => $date,
            'jackpot_data' => [
                'winning_numbers' => $winning_numbers_arr,
                'prize_pool' => $jackpot_amount,
                'extra' => [
                    'max_millions_numbers' => $max_millions_numbers,
                    'extra_number' => trim($extra_number_node[0]->nodeValue)
                ]
            ] 
        ];
    }

    private function setDbData()
    {
        $jackpot = $this->getLottoMaxJackpotData();
        
        if (!isset($jackpot)) {
            return;
        }
       
        $last_jackpot_in_db = LotteryJackpot::where('name', $this->getClassName())->latest('draw_date')->first();
 
        if (!isset($last_jackpot_in_db)) {
            $new_jp = new LotteryJackpot();
            $new_jp->name = $this->getClassName();
            $new_jp->type = 'national';
            $new_jp->draw_date = $jackpot['draw_date'];
            $new_jp->jackpot_data = [
                'winning_numbers'   => $jackpot['jackpot_data']['winning_numbers'],
                'prize_pool'        => $jackpot['jackpot_data']['prize_pool'], 
                'cash_value'        => null,
                'extra'             => [
                        'max_millions_numbers' => $jackpot['jackpot_data']['extra']['max_millions_numbers'],
                        'extra_number'  => $jackpot['jackpot_data']['extra']['extra_number'],
                        'currency'      => 'USD',
                    ]
            ];
            
            $new_jp->save(); 
        } else {

            if ($last_jackpot_in_db->draw_date === $jackpot['draw_date']) {
                return;
            } else {
                $new_jp = new LotteryJackpot();
                $new_jp->name = $this->getClassName();
                $new_jp->type = 'national';
                $new_jp->draw_date = $jackpot['draw_date'];
                $new_jp->jackpot_data = [
                    'winning_numbers'   => $jackpot['jackpot_data']['winning_numbers'],
                    'prize_pool'        => $jackpot['jackpot_data']['prize_pool'], 
                    'cash_value'        => null,
                    'extra'             => [
                        'max_millions_numbers' => $jackpot['jackpot_data']['extra']['max_millions_numbers'],
                        'extra_number'  => $jackpot['jackpot_data']['extra']['extra_number'],
                        'currency'      => 'USD',
                    ]
                ];
                
                $new_jp->save();    
            }
        }
    }
}