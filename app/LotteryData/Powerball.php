<?php 

namespace App\LotteryData;

use Illuminate\Support\Facades\Http;
use App\LotteryData\LotteryBase;
use App\LotteryJackpot;

use Illuminate\Support\Str;
use Carbon\Carbon;

class Powerball extends LotteryBase
{
    function __construct()
    {
        $this->amount_url = 'https://powerball.com/api/v1/estimates/powerball?_format=json';
        $this->numbers_url = 'https://www.powerball.com/api/v1/numbers/powerball/recent?_format=json';
        $this->powerball_amount_json = null;
        $this->powerball_numbers_json = null;
        $this->field_next_draw_date = null;
    }

    public function __invoke()
    {
        $this->makeApiRequest();  
        $this->setDbData();  
    }

    protected function makeApiRequest()
    {
        $this->makeJackpotAmountApiRequest();
        $this->makeJackpotNumbersApiRequest();
    }

    private function makeJackpotNumbersApiRequest()
    {
        try {
            $response = Http::retry(2, 55)->get($this->numbers_url);
        } catch (\Throwable $th) {
            return null;
        }
       
        if ($response->status() !== 200) {
            return null;
        }

        $this->powerball_numbers_json = $response->json();
    }

    private function makeJackpotAmountApiRequest()
    {
        try {
            $response = Http::retry(2, 55)->get($this->amount_url);
        } catch (\Throwable $th) {
            return null;
        }
       
        if ($response->status() !== 200) {
            return null;
        }

        $this->powerball_amount_json = $response->json();
    }

    private function setPowerballJackpotAmount()
    {
        if (!isset($this->powerball_amount_json)) {
            return;
        }
        
        if (!isset($this->powerball_amount_json[0]['field_prize_amount'], $this->powerball_amount_json[0]['field_prize_amount_cash'])) {
            return;
        }
        
        $field_next_draw_date = $this->filterFieldNextDrawDate($this->powerball_amount_json[0]['field_next_draw_date']);
        $this->field_next_draw_date = $field_next_draw_date;

        $powerball = LotteryJackpot::where([
            ['name', '=', $this->getClassName()],
            ['draw_date', '=', $field_next_draw_date]
        ])->first();

        if (isset($powerball)) {   
            return;
        }

        $patterns = [];
        $patterns[0] = '/\$/';
        $patterns[1] = '/Million/';
        $replacements = ['', ''];

        $field_prize_amount_cash = preg_replace($patterns, $replacements, $this->powerball_amount_json[0]['field_prize_amount_cash']);
        $field_prize_amount = preg_replace($patterns, $replacements, $this->powerball_amount_json[0]['field_prize_amount']);

        $cash_value = (int) (floatval(trim($field_prize_amount_cash)) * 1000000);
        $prize_pool = (int) (floatval(trim($field_prize_amount)) * 1000000);

        $powerball = new LotteryJackpot();
        $powerball->name = $this->getClassName();
        $powerball->type = 'national';
        $powerball->draw_date = $field_next_draw_date;
        $powerball->jackpot_data = [
            'prize_pool'        => $prize_pool, 
            'cash_value'        => $cash_value,
            'extra'             => [
                'currency'      => 'USD'
            ]
        ];
        
        $powerball->save(); 
    }

    private function getPowerballJackpotNumbers()
    {
        if (isset($this->powerball_numbers_json[0])) {
            $numbers_arr = explode(',', $this->powerball_numbers_json[0]['field_winning_numbers']);

            return [
                'winning_numbers' => $numbers_arr,
                'field_multiplier' => $this->powerball_numbers_json[0]['field_multiplier'],
                'draw_date' => $this->powerball_numbers_json[0]['field_draw_date']
            ];
        } else {
            return null;
        }
    }

    private function filterFieldNextDrawDate(string $date): string
    {
        $end_pos = strpos($date, 'T');
        return Str::substr($date, 0, $end_pos);
    }

    private function setDbData()
    {
        //updating winning numbers for previous powerbal record in db
        $this->setPowerballJackpotAmount();
        $numbers = $this->getPowerballJackpotNumbers();

        if (!isset($numbers)) {
            return;
        }

        $second_last_jackpot_in_db = LotteryJackpot::where('name', $this->getClassName())->orderBy('draw_date', 'DESC')->skip(1)->first();
        
        if ($second_last_jackpot_in_db && !isset($second_last_jackpot_in_db->jackpot_data['winning_numbers'])) {
            $jp_data = array_merge($second_last_jackpot_in_db->jackpot_data, [
                    'winning_numbers'   => $numbers['winning_numbers'],
                    'extra'             => [
                            'multiplier'    => $numbers['field_multiplier'],
                            'currency'      => 'USD'
                        ]
                ]);

            $second_last_jackpot_in_db->name = $this->getClassName();
            $second_last_jackpot_in_db->type = 'national';
            $second_last_jackpot_in_db->draw_date = $numbers['draw_date'];
            $second_last_jackpot_in_db->jackpot_data = $jp_data;
            
            $second_last_jackpot_in_db->save();    
        } 
    }
}