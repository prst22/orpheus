<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jackpot extends Model
{  
    protected $casts = [
        'date_amount' => 'array'
    ];

    protected $fillable = ['casino_id', 'slot_id', 'type', 'date_amount', 'name'];

    public $timestamps = false;

    public function casino()
    {
        return $this->belongsTo('App\Casino');
    }

    public function slot()
    {
        return $this->belongsTo('App\Slot');
    }
}
