<?php

namespace Database\Factories;

use App\LotteryJackpot;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class LotteryJackpotFactory extends Factory
{
    protected $model = LotteryJackpot::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $arr = ['$', '£'];
        return [
            'name' => 'Euromillions',
            'type' => 'national',
            'draw_date' => $this->faker->date(),
            'jackpot_data' => [
                'winning_numbers' => [40,41,58,64,65,17],
                'prize_pool' => '1300000', 
                'cash_value' => '600000',
                'extra' => [
                    'multiplier' => rand(1, 10),
                    'm_ball' => rand(1, 99),
                    'currency' => Arr::random($arr)
                ]
            ],
        ];
    }
}
