<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Casino;

class AddSlugToCasinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('casinos', function (Blueprint $table) {
            $table->string('slug');
        });

        Casino::upsert([
            ['name' => 'Mr green', 'slug' => 'mr-green'],
            ['name' => 'Unibet', 'slug' => 'unibet'],
            ['name' => 'Casumo', 'slug' => 'casumo'],
            ['name' => '888casino', 'slug' => '888casino'],
            ['name' => 'bet365', 'slug' => 'bet365'],
            ['name' => 'Bitstarz', 'slug' => 'bitstarz'],
            ['name' => 'Marathonbet', 'slug' => 'marathonbet'],
            ['name' => 'Redstar', 'slug' => 'redstar'],
            ['name' => 'Pinnacle', 'slug' => 'pinnacle'],
            ['name' => 'Videoslots', 'slug' => 'videoslots'],
            ['name' => 'Party Poker', 'slug' => 'party-poker'],
            ['name' => 'Casinoeuro', 'slug' => 'casinoeuro'],
        ], ['name'], ['slug']);
        
        Schema::table('casinos', function (Blueprint $table) {
            $table->unique('slug');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casinos', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
