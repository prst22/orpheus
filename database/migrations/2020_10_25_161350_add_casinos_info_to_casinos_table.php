<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCasinosInfoToCasinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('casinos', function (Blueprint $table) {
            $table->string('url')->nullable();
            $table->float('rating', 3, 1)->nullable();
            $table->text('image')->nullable();
            $table->text('info')->nullable();
            $table->boolean('tracked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casinos', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('rating');
            $table->dropColumn('image');
            $table->dropColumn('info');
            $table->dropColumn('tracked');
        });
    }
}
