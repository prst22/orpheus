<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotteryJackpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lottery_jackpots', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('type', 50)->nullable()->default(NULL);
            $table->date('draw_date');
            $table->json('jackpot_data')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lottery_jackpots');
    }
}
