<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasinoSlotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casino_slot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('casino_id')->unsigned()->index();
            $table->bigInteger('slot_id')->unsigned()->index(); 
            $table->boolean('active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casino_slot');
    }
}
