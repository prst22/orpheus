<?php

use App\Http\Controllers\ShowPostController;
use App\Http\Controllers\ShowStreamController;
use App\Http\Controllers\ShowCasinoController;
use App\Http\Controllers\FavouritePostController;
use App\Http\Controllers\FavouriteStreamController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\JackpotTrackerController;
use App\Http\Controllers\LotteryJackpotTrackerController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\TermsAndConditionsController;

use App\Http\Controllers\Dashboard\PostController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\StreamController;
use App\Http\Controllers\Dashboard\CasinoController;
use App\Http\Controllers\Dashboard\SlotController;
use App\Http\Controllers\Dashboard\JackpotController;
use App\Http\Controllers\Dashboard\CKEditorController;
use App\Http\Controllers\Dashboard\CommentController;
use App\Http\Controllers\Dashboard\AttachmentController;
use App\Http\Controllers\Dashboard\UserController;

use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);

Route::get('/', [ShowPostController::class, 'index'])->name('home');

// SHOW SINGLE POST
Route::get('post/{slug}', [ShowPostController::class, 'show'])->name('posts.show');

Route::get('streams', [ShowStreamController::class, 'index'])->name('all-streams');
// SHOW SINGLE STREAM
Route::get('stream/{slug}', [ShowStreamController::class, 'show'])->name('streams.show');

// SORTING ROUTS
Route::get('/sortby/{type}/{ascendance}', [ShowPostController::class, 'sortby'])->where(['type' => '[a-z]+', 'ascendance' => '[a-z]+'])->name('posts.sortby');
Route::get('posts/category/{slug}', [ShowPostController::class, 'category'])->name('posts.category');
Route::get('posts/category/{slug}/sortby/{type}/{ascendance}', [ShowPostController::class, 'category_sortby'])->where(['type' => '[a-z]+', 'ascendance' => '[a-z]+'])->name('posts.category.sortby');
Route::get('streams/category/{slug}', [ShowStreamController::class, 'category'])->name('streams.category');

Route::middleware(['verified'])->prefix('favourite')->group(function() {
    Route::get('posts', [FavouritePostController::class, 'index'])->name('favourite.posts.index');
    Route::get('streams', [FavouriteStreamController::class, 'index'])->name('favourite.streams.index');
    Route::post('/save/post/{postid}', [FavouritePostController::class, 'store'])->name('favourite.posts.store');
    Route::post('/save/stream/{postid}', [FavouriteStreamController::class, 'store'])->name('favourite.streams.store');
    Route::post('/destroy/post/{postid}', [FavouritePostController::class, 'destroy'])->name('favourite.posts.destroy');
    Route::post('/destroy/stream/{postid}', [FavouriteStreamController::class, 'destroy'])->name('favourite.streams.destroy');
});

Route::prefix('admin')->group(function() {
    Route::get('/',[AdminLoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('login', [AdminLoginController::class, 'login'])->name('admin.login.submit');
    Route::post('logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
});

Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

Route::get('/jackpots-tracker', [JackpotTrackerController::class, 'index'])->name('jackpots-tracker');
Route::get('/lottery-tracker', [LotteryJackpotTrackerController::class, 'index'])->name('lottery-tracker');
Route::get('/lottery-tracker/{name}', [LotteryJackpotTrackerController::class, 'sortByName'])->name('lottery-name');

Route::get('/casinos', [ShowCasinoController::class, 'index'])->name('all-casinos');
Route::get('/casinos/{casino:slug}', [ShowCasinoController::class, 'show'])->name('casinos.show');

Route::get('search', [SearchController::class, 'search'])->name('search');

Route::prefix('contact')->group(function(){
    Route::get('/', [ContactController::class, 'index'])->name('contact');
    Route::post('submit', [ContactController::class, 'submitForm'])->name('submit-form');
});

// DASHBOARD START
Route::prefix('dashboard')->group(function(){
    Route::get('/', [PostController::class, 'index'])->name('dashboard');
    
    Route::resource('posts', PostController::class)->except([
        'index', 'show'
    ]);
    
    // Categories
    Route::resource('categories', CategoryController::class)->except([
        'edit', 'show', 'create'
    ]);

    // Streams    
    Route::resource('streams', StreamController::class)->except([
        'show'
    ]);
    
    // Casinos
    Route::resource('casinos', CasinoController::class)->except([
        'show'
    ]);

    // Slots
    Route::resource('slots', SlotController::class)->except([
        'edit', 'show'
    ]);
    Route::post('/slots/set-active/{slot}', [SlotController::class, 'setActive'])->name('slots.set-active');
    Route::post('/slots/{id}/{casinoid}', [SlotController::class, 'detach'])->name('slots.detach');

    //Jackpots
    Route::resource('jackpots', JackpotController::class)->except([
        'edit', 'show'
    ]);

    Route::post('upload', [CKEditorController::class, 'upload']);

    // approve comments
    Route::get('comments', [CommentController::class, 'index'])->name('dashboard.comments.index');
    Route::post('comments/{id}', [CommentController::class, 'approve'])->name('dashboard.comments.approve');
    Route::post('comments/destroy/{id}', [CommentController::class, 'destroy'])->name('dashboard.comments.destroy');

    // attachments
    Route::resource('attachments', AttachmentController::class)->only([
        'index', 'destroy'
    ]); 

    // users
    Route::resource('users', UserController::class)->only([
        'index', 'destroy'
    ]);
});
// DASHBOARD END

// LOGIN WITH GOOGLE FACEBOOK START
Route::prefix('login')->group(function(){
    Route::get('googleredirect', [LoginController::class, 'redirectToGoogleProvider'])->name('google.login');
    Route::get('googlecallback', [LoginController::class, 'handleGoogleProviderCallback']);

    Route::get('facebookredirect', [LoginController::class, 'redirectToFacebookProvider'])->name('facebook.login');
    Route::get('facebookcallback', [LoginController::class, 'handleFacebookProviderCallback']);
});
// LOGIN WITH GOOGLE FACEBOOK END

// TERMS

Route::get('/terms-and-conditions', [TermsAndConditionsController::class, 'index'])->name('terms');
Route::get('/about', [AboutController::class, 'index'])->name('about');