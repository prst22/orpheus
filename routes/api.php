<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserApi;
use App\Http\Controllers\Api\AttachmentApiController;
use App\Http\Controllers\Api\JackpotApiController;
use App\Http\Controllers\Api\CasinoApiController;
use App\Http\Controllers\Api\CurrencyApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', UserApi::class);

//Jackpots
Route::get('jackpots', JackpotApiController::class); 

//Casino slots
Route::get('casino-slots', CasinoApiController::class); 

//Attachments
Route::get('getattachments', AttachmentApiController::class); 

//Currency
Route::get('currency', CurrencyApiController::class); 
